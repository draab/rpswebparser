package at.foorada.logging;

import at.foorada.versioning.data.Version;
import org.json.simple.JSONObject;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 *
 *
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 13.03.2017.
 */
public class JSONFormatter extends Formatter {

    /**
     * format for database timestamp (string sorting possible)<br>
     * format:yyyy-MM-dd HH:mm:ss
     */
    private static final SimpleDateFormat TIMESTAMP = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private final Version appVersion;

    public JSONFormatter(Version appVersion) {

        this.appVersion = appVersion;
    }

    @Override
    public String format(LogRecord record) {

        JSONObject log = new JSONObject();

        log.put("appName", appVersion.getAppName());

        log.put("appVersion", appVersion.getAppVersion());

        log.put("date", TIMESTAMP.format(new Date(record.getMillis())));

        String logger = record.getLoggerName();
        if(logger != null) {
            log.put("logger", logger);
        }

        log.put("level", record.getLevel().toString());

        String clazz = record.getSourceClassName();
        if(clazz != null) {
            log.put("class", clazz);
        }

        String method = record.getSourceMethodName();
        if(method != null) {
            log.put("method", method);
        }

        if (record.getMessage() != null) {
            log.put("message", formatMessage(record));
        }

        Throwable t = record.getThrown();
        if(t != null) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            pw.println();
            record.getThrown().printStackTrace(pw);
            pw.close();
            log.put("exception", sw.toString());
        }

        return log.toJSONString();
    }
}
