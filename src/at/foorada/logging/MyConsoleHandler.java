/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class MyConsoleHandler extends Handler {

    public static final Level CONSOLE_ERROR_LIMIT = Level.WARNING;
    public static final String LOG_FORMAT_STRING = "%4$s: %5$s  -- in --  %2$s  -- at --  [%1$tc]%n %6$s";

    private void configure(Level level) {

        setLevel(level);
        setFilter(null);
        setFormatter(new SimpleFormatter());
    }

    public MyConsoleHandler() {
        this(Level.ALL);
    }

    public MyConsoleHandler(Level level) {
        configure(level);
    }

    @Override
    public void publish(LogRecord record) {
        if (!isLoggable(record)) {
            return;
        }

        Date dat = new Date();
        dat.setTime(record.getMillis());

        //building source string with given information
        String source;
        if (record.getSourceClassName() != null) {
            source = record.getSourceClassName();
            if (record.getSourceMethodName() != null) {
                source += " " + record.getSourceMethodName();
            }
        } else {
            source = record.getLoggerName();
        }

        //print strack trace with new line before
        String throwable = "";
        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();
            try (PrintWriter pw = new PrintWriter(sw)) {
                pw.println();
                record.getThrown().printStackTrace(pw);
            }
            throwable = sw.toString();
        }

        String message = getFormatter().formatMessage(record);

        //format string to log
        String log = String.format(LOG_FORMAT_STRING,
                dat,
                source,
                record.getLoggerName(),
                record.getLevel().getLocalizedName(),
                message,
                throwable);

        //do log on console depending on logLevel
        if(record.getLevel().intValue() >= CONSOLE_ERROR_LIMIT.intValue()) {
            System.err.print(log);
        } else {
            System.out.print(log);
        }
    }

    @Override
    public void flush() {
        System.out.flush();
        System.err.flush();
    }

    @Override
    public void close() throws SecurityException {
        flush();
    }
}
