package at.foorada.logging;

import at.foorada.versioning.data.Version;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 *
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 13.03.2017.
 */
public class MyNetHandler extends Handler {

    private static final String DEFAULT_URL = "http://log.draab.at/record.php";

    private final URL url;
    private final Version appVersion;
    private List<LogRecord> logList = new ArrayList<>();

    public MyNetHandler(Version appVersion, String urlString, Level ownLevel) throws IOException {
        this(appVersion, urlString);
        setLevel(ownLevel);
    }

    public MyNetHandler(Version appVersion, Level ownLevel) throws IOException {
        this(appVersion, DEFAULT_URL);
        setLevel(ownLevel);
    }

    public MyNetHandler(Version appVersion) throws IOException {
        this(appVersion, DEFAULT_URL);
    }

    public MyNetHandler(Version appVersion, String urlString) throws IOException {

        setFormatter(new JSONFormatter(appVersion));
        this.appVersion = appVersion;
        url = new URL(urlString);
    }

    @Override
    public void publish(LogRecord record) {
        if (isLoggable(record)) {
            logList.add(record);
        }
    }

    public void forcePublish(LogRecord record) {

        StringBuilder bldr = new StringBuilder(500);
        bldr.append("log=[");
        bldr.append(getFormatter().format(record));
        bldr.append("]");

        try {
            sendMessage(bldr.toString());

        } catch (IOException e) {
            System.err.println("failed to send log to server");
        }
    }

    @Override
    public void flush() {
        if(logList.isEmpty())
            return;

        StringBuilder bldr = new StringBuilder(500);
        bldr.append("log=[");

        for(int i=0; i<logList.size(); i++) {
            if(i != 0)
                bldr.append(",");
            bldr.append(getFormatter().format(logList.get(i)));
        }
        logList.clear();
        bldr.append("]");

        try {
            sendMessage(bldr.toString());

        } catch (IOException e) {
             System.err.println("failed to send log to server");
        }
    }

    private void sendMessage(String message) throws IOException {

        byte[] postData = message.getBytes( StandardCharsets.UTF_8 );

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty( "charset", "utf-8");
        connection.setUseCaches( false );
        connection.setRequestProperty( "Content-Length", Integer.toString( postData.length ));

        DataOutputStream wr = new DataOutputStream( connection.getOutputStream());

        wr.write(postData);
        wr.close();

//                connection.getDoInput()

        Reader in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));

        for (int c; (c = in.read()) >= 0;)
            System.out.print((char)c);

    }

    @Override
    public void close() throws SecurityException {
        flush();
    }
}
