package at.foorada.rk.rps;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.WindowEvent;

/**
 * event handler for firing a window_close_request on pressing escape
 * @author danie
 */
public class EscKeyClosingEventHandler implements EventHandler<KeyEvent> {

    private String message;

    public EscKeyClosingEventHandler(String consoleMessage) {
        message = consoleMessage;
    }

    public EscKeyClosingEventHandler() {
        this("exit on escape");
    }

    @Override
    public void handle(KeyEvent event) {
        if(event.getCode() == KeyCode.ESCAPE && event.getSource() instanceof Scene) {
            System.out.println(message);

            Scene s = (Scene)event.getSource();

            // fireEvent is necessary to firing OnCloseRequest event
            s.getWindow().fireEvent(
                    new WindowEvent(
                            s.getWindow(),
                            WindowEvent.WINDOW_CLOSE_REQUEST
                    )
            );
        }
    }
}
