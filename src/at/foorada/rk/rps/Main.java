package at.foorada.rk.rps;

import at.foorada.logging.MyConsoleHandler;
import at.foorada.logging.MyNetHandler;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessModuleEnum;
import at.foorada.rk.rps.gui.MainWindowController;
import at.foorada.versioning.data.Version;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.*;
import java.util.prefs.Preferences;

public class Main extends Application {

    /**
     * hold on format:
     * ['r'|'b'|'d'].d.d(.d)
     * starting with an char r, b or d followed by a '.' than a number
     * another '.' with number and optional the same again
     */
    public static final Version APP_VERSION = new Version("RPSWebParser", "d0.1.1");

    public static final Logger APP_LOGGER = Logger.getLogger("at.foorada");
    public Image appIcon;

    private static final String DEFAULT_FILE = "data."+DataAccessModuleEnum.JSON.getPlainExtension();

    private MainWindowController mainWindowController = null;
    private Stage mainWindow = null;
    private Preferences preferences;



    public static void main(String[] args) throws IOException {
        initAppLogger();

        try {
            launch(args);

        } catch (Exception e) {
            APP_LOGGER.log(Level.SEVERE, "unhandled exception", e);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        APP_LOGGER.log(Level.FINEST, "RPSWebParser application started");
        mainWindow = primaryStage;
        try {
            preferences = Preferences.userNodeForPackage(this.getClass());
        } catch (SecurityException e) {
            APP_LOGGER.log(Level.WARNING, "unable to get preferences object", e);
        }

        //adding the icon for the main window
        appIcon = new Image("/resource/icon/rkRpsParser.png");
        primaryStage.getIcons().add(appIcon);

        mainWindowController = MainWindowController.loadWindow(primaryStage);
        arrangeMainWindow();

        primaryStage.setOnCloseRequest(event -> {
            storeMainWindowBoundaries();

            mainWindowController.close();

            APP_LOGGER.log(Level.FINEST, "controlled program-shutdown");
        });

        // save data with ctrl + s
        primaryStage.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
            if(event.getCode().equals(KeyCode.F11)) {
                System.out.format("x %f \n", mainWindow.getX());
                System.out.format("y %f \n", mainWindow.getY());
                System.out.format("width %f \n", mainWindow.getHeight());
                System.out.format("height %f \n", mainWindow.getWidth());
                fullScreenWindowToggle();
            }
            if(event.isControlDown() && event.getCode().equals(KeyCode.S)) {
                mainWindowController.saveData();
            }
        });

        primaryStage.show();

        checkGivenParameter();
    }

    private void checkGivenParameter() {

        Application.Parameters params = getParameters();
        if(params != null && params.getRaw().size() > 0) {
            Map<String, String> namedParameters = params.getNamed();
            List<String> unnamedParameters = params.getUnnamed();


            //if db is set check if the given file is a valid dataAccess module
            if(namedParameters.containsKey("db")) {
                File file = new File(namedParameters.get("db"));
                if(file.exists()) {
                    mainWindowController.loadNewDataAccess(file);
                }
            }
        } else {
            //open default file if possible
            File defaultFile = new File(DEFAULT_FILE);
            if(defaultFile.exists()) {
                mainWindowController.loadNewDataAccess(defaultFile);

            } else {
                APP_LOGGER.log(Level.INFO, "no default {0}", DEFAULT_FILE);
            }
        }
    }

    //region window boundaries and resize methods

    private void arrangeMainWindow() {
        if(preferences != null && mainWindow != null) {
            try {
                mainWindow.setX(preferences.getDouble("mainWindowXPos", 0));
                mainWindow.setY(preferences.getDouble("mainWindowYPos", 0));
                mainWindow.setWidth(preferences.getDouble("mainWindowWidth", 800));
                mainWindow.setHeight(preferences.getDouble("mainWindowHeight", 600));
            } catch (IllegalStateException e) {
                APP_LOGGER.log(Level.WARNING, "unable to arrange MainWindow", e);
            }
        }
    }

    private void fullScreenWindowToggle() {

        mainWindow.setFullScreen(mainWindow.isFullScreen());


//        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
//        double screenWidth = primScreenBounds.getWidth();
//        double screenHeight = primScreenBounds.getHeight();
//
//	    mainWindow.setX(0);
//	    mainWindow.setY(0);
//	    mainWindow.setWidth(screenWidth);
//	    mainWindow.setHeight(screenHeight);
    }

    private void storeMainWindowBoundaries() {
        if(preferences != null && mainWindow != null) {
            try {
                preferences.putDouble("mainWindowXPos", mainWindow.getX());
                preferences.putDouble("mainWindowYPos", mainWindow.getY());
                preferences.putDouble("mainWindowWidth", mainWindow.getWidth());
                preferences.putDouble("mainWindowHeight", mainWindow.getHeight());
            } catch (IllegalStateException | IllegalArgumentException e) {
                APP_LOGGER.log(Level.WARNING, "unable store window boundaries in preferences", e);
            }
        }
    }

    //endregion

    //region logger region

    private static void initAppLogger() {

        // suppress the logging output to the console
        Logger rootLogger = Logger.getLogger("");
        Handler[] handlers = rootLogger.getHandlers();
        if (handlers[0] instanceof ConsoleHandler) {
            rootLogger.removeHandler(handlers[0]);
        }

        switch(APP_VERSION.getAppVersion().charAt(0)) {
            case 'r':   //release version
                setReleaseLogHandler();
                break;
            case 'b':   //beta version
                setBetaLogHandler();
                break;
            case 'd':   //development version
                setDevelopmentLogHandler();
                break;
            default:
                setDevelopmentLogHandler();
        }
    }

    /**
     * sets handler for beta versions (simple file handler for INFO and above)
     */
    private static void setBetaLogHandler() {
        APP_LOGGER.setLevel(Level.INFO);
        APP_LOGGER.addHandler(new MyConsoleHandler());
        try {
            APP_LOGGER.addHandler(new FileHandler("log.xml"));
            addNetHandler();
        } catch (IOException e) {
            APP_LOGGER.log(Level.WARNING, null, e);
        }
    }

    /**
     * sets handler for release version (simple file handler for SEVERE)
     * TODO implement MyNetHandler for delivery SEVERE logs to the server
     */
    private static void setReleaseLogHandler() {
        APP_LOGGER.setLevel(Level.SEVERE);
        APP_LOGGER.addHandler(new MyConsoleHandler());
        try {
            APP_LOGGER.addHandler(new FileHandler("errorLog.xml"));
            addNetHandler();
        } catch (IOException e) {
            APP_LOGGER.log(Level.WARNING, null, e);
        }
    }

    /**
     * sets own console handler and a file handler for development
     */
    private static void setDevelopmentLogHandler() {
        APP_LOGGER.setLevel(Level.ALL);
        APP_LOGGER.addHandler(new MyConsoleHandler());
        try {
            APP_LOGGER.addHandler(new FileHandler("logger.xml"));
//            addNetHandler();
        } catch (IOException e) {
            APP_LOGGER.log(Level.WARNING, null, e);
        }
    }

    private static void addNetHandler() throws IOException {
        MyNetHandler handler = new MyNetHandler(APP_VERSION, Level.SEVERE);
        if(!APP_VERSION.getAppVersion().startsWith("d"))
            handler.forcePublish(new LogRecord(Level.FINEST, "app usage"));
        APP_LOGGER.addHandler(handler);

    }

    //endregion
}