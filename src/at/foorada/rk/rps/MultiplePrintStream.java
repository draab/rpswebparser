package at.foorada.rk.rps;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

/**
 * printStream with a set of other print streams;
 * printStreams can be added and removed;
 * each method call is done on each printStream in the set
 * @author danie
 */
public class MultiplePrintStream extends PrintStream {

    private final Set<PrintStream> streamSet;

    public MultiplePrintStream(PrintStream out) {
        super(out);
        streamSet = new HashSet<>();
        streamSet.add(out);
    }

    public void addOutputStream(PrintStream out) {
        streamSet.add(out);
    }

    public void removeOutputStream(PrintStream out) {
        streamSet.remove(out);
    }

    @Override
    public void flush() {
        streamSet.forEach(PrintStream::flush);
    }

    @Override
    public void close() {
        streamSet.forEach(PrintStream::close);
    }

    @Override
    public boolean checkError() {
	if (streamSet.stream().anyMatch((ps) -> (ps.checkError()))) {
	    return true;
	}
        return false;
    }

//    @Override
//    protected void setError() {
//        streamSet.forEach(PrintStream::setError);
//    }

//    @Override
//    protected void clearError() {
//        streamSet.forEach(PrintStream::clearError);
//    }

    @Override
    public void write(int b) {
	streamSet.forEach((ps) -> {ps.write(b);});
    }

    @Override
    public void write(byte[] buf, int off, int len) {
	streamSet.forEach((ps) -> {ps.write(buf, off, len);});
    }

    @Override
    public void print(boolean b) {
	streamSet.forEach((ps) -> {ps.print(b);});
    }

    @Override
    public void print(char c) {
	streamSet.forEach((ps) -> {ps.print(c);});
    }

    @Override
    public void print(int i) {
	streamSet.forEach((ps) -> {ps.print(i);});
    }

    @Override
    public void print(long l) {
	streamSet.forEach((ps) -> {ps.print(l);});
    }

    @Override
    public void print(float f) {
	streamSet.forEach((ps) -> {ps.print(f);});
    }

    @Override
    public void print(double d) {
	streamSet.forEach((ps) -> {ps.print(d);});
    }

    @Override
    public void print(char[] s) {
	streamSet.forEach((ps) -> {ps.print(s);});
    }

    @Override
    public void print(String s) {
	streamSet.forEach((ps) -> {ps.print(s);});
    }

    @Override
    public void print(Object obj) {
	streamSet.forEach((ps) -> {ps.print(obj);});
    }

    @Override
    public void println() {
        streamSet.forEach(PrintStream::println);
    }

    @Override
    public void println(boolean x) {
	streamSet.forEach((ps) -> {ps.println(x);});
    }

    @Override
    public void println(char x) {
	streamSet.forEach((ps) -> {ps.println(x);});
    }

    @Override
    public void println(int x) {
	streamSet.forEach((ps) -> {ps.println(x);});
    }

    @Override
    public void println(long x) {
	streamSet.forEach((ps) -> {ps.println(x);});
    }

    @Override
    public void println(float x) {
	streamSet.forEach((ps) -> {ps.println(x);});
    }

    @Override
    public void println(double x) {
	streamSet.forEach((ps) -> {ps.println(x);});
    }

    @Override
    public void println(char[] x) {
	streamSet.forEach((ps) -> {ps.println(x);});
    }

    @Override
    public void println(String x) {
	streamSet.forEach((ps) -> {ps.println(x);});
    }

    @Override
    public void println(Object x) {
	streamSet.forEach((ps) -> {ps.println(x);});
    }

    @Override
    public PrintStream append(char c) {
	streamSet.forEach((ps) -> {ps.append(c);});
        return this;
    }

    @Override
    public PrintStream append(CharSequence csq) {
	streamSet.forEach((ps) -> {ps.append(csq);});
        return this;
    }

    @Override
    public PrintStream append(CharSequence csq, int start, int end) {
	streamSet.forEach((ps) -> {ps.append(csq, start, end);});
        return this;
    }
}
