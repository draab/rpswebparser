package at.foorada.rk.rps;

import at.foorada.rk.rps.data.RPSMember;
import javafx.event.EventHandler;
import javafx.event.EventTarget;
import javafx.scene.control.ComboBox;
import javafx.scene.input.KeyEvent;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Event Handler for KeyEvent on a ComboBox<br>
 * on typing a character the ComboBox will jump to the item with
 * starting character (if exists) <br>
 * if more than one item with same leading character exists, a iteration thru
 * the items is possible on typing the character multiple times
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class MyComboBoxKeyEventHandler implements EventHandler<KeyEvent> {
    
    private static final Logger LOGGER = Logger.getLogger(MyComboBoxKeyEventHandler.class.getName());

    @Override
    public void handle(KeyEvent event) {
        EventTarget target = event.getTarget();
        if(target instanceof ComboBox) {
	    ComboBox comboBox = (ComboBox) target;
            RPSMember s = jumpTo(event.getText(), (RPSMember) comboBox.getValue(), comboBox.getItems());
            if (s != null) {
                comboBox.setValue(s);
		event.consume();
            }
        } else {
	    LOGGER.log(Level.SEVERE, "event handler only for setOnKeyReleased on ComboBox!");
        }
    }

    private RPSMember jumpTo(String keyPressed, RPSMember currentlySelected, List<RPSMember> items) {
        String key = keyPressed.toUpperCase();
	
	// Only act on letters so that navigating with cursor keys does not
	// try to jump somewhere.
        if (key.matches("^[A-Z]$")) {
	    
            RPSMember firstFound = null;
	    boolean currentFound = false;
	    
	    //iterate throw all comboBox items
            for (RPSMember s : items) {
		//check if name of item start with typed letter
                if (s.name.toUpperCase().startsWith(key)) {
		    
		    // if nothing is selected, take the first found item.
		    // and
		    // if current selected item was already found in last iteration
		    // return the next found item
		    if(currentFound || currentlySelected == null)
			return s;
		    
		    //mark first found, in case that the last one is the selected one
		    if(firstFound == null) {
			firstFound = s;
		    }
		    
		    //mark if current selected is found
		    if(s.equals(currentlySelected)) {
			currentFound = true;
		    }
		    
		    //in case of orderd list, after nothing more to find the first found is the next
                } else if(firstFound != null) {
		    return firstFound;
		}
            }
        }
        return null;
    }
}
