package at.foorada.rk.rps.data;

import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessInterface;
import at.foorada.rk.rps.data.storage.main.dataAccess.NoDataAccess;
import javafx.beans.property.ObjectPropertyBase;
import javafx.beans.property.ReadOnlyObjectPropertyBase;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

/**
 *
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 17.03.2017.
 */
public class DataAccessModule {

    private final DataAccessProperty dataAccess = new DataAccessProperty();


    public DataAccessModule() {
        this(new NoDataAccess());
    }

    public DataAccessModule(DataAccessInterface dataAccess) {
        this.dataAccess.set(dataAccess);
    }



    public DataAccessProperty getDataAccessProperty() {
        return dataAccess;
    }

    public DataAccessReadOnlyProperty getDataAccessReadOnlyProperty() {
        return new DataAccessReadOnlyProperty(dataAccess);
    }


    public class DataAccessProperty extends ObjectPropertyBase<DataAccessInterface> {
        DataAccessInterface oldDataAccess;

        @Override
        protected void invalidated() {
            DataAccessInterface newValue = get();
            if(newValue == oldDataAccess)
                return;
            if(newValue == null)
                set(oldDataAccess);

            oldDataAccess = newValue;
        }

        @Override
        public Object getBean() {
            return DataAccessModule.this;
        }

        @Override
        public String getName() {
            return "dataAccess";
        }
    }

    public class DataAccessReadOnlyProperty extends ReadOnlyObjectPropertyBase<DataAccessInterface> {

        private final DataAccessProperty property;

        DataAccessReadOnlyProperty(DataAccessProperty dataAccess) {
            this.property = dataAccess;
            this.property.addListener(new ChangeListener<DataAccessInterface>() {
                @Override
                public void changed(ObservableValue<? extends DataAccessInterface> observable, DataAccessInterface oldValue, DataAccessInterface newValue) {
                    fireValueChangedEvent();
                }
            });
        }

        @Override
        public Object getBean() {
            return DataAccessModule.this;
        }

        @Override
        public String getName() {
            return "dataAccess";
        }

        @Override
        public DataAccessInterface get() {
            return property.get();
        }
    }
}
