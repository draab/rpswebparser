/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author danie
 */
public class QuickLoadConfiguration {
    public String mail = "";
    public int offset = 0;
    public int days = 14;
    
    public Set<RPSDepartment> departments = new HashSet<>();
}
