package at.foorada.rk.rps.data;

import java.util.*;

/**
 * class represents an department from the rk dienstplan website
 * @author danie
 */
public class RPSDepartment implements Comparable<RPSDepartment> {

    // static object for unknwon department
    public static final RPSDepartment UNKNOWN = new RPSDepartment(-1, "Unknown");
    // static all department object for filter
    public static final RPSDepartment ALL = new RPSDepartment(-2, "Alle Abteilungen");

    //department number is the id from the website for description
    private final int departmentNumber;
    private final String description;
    private Calendar addedTime;

    public RPSDepartment(int departmentNumber, String text) {
        this.departmentNumber = departmentNumber;
        description = text;
        addedTime = Calendar.getInstance();
    }

    public void setAddedTime(Calendar added) {
        this.addedTime = added;
    }

    public int getId() {
        return departmentNumber;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return description;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof RPSDepartment && this.departmentNumber == ((RPSDepartment) obj).departmentNumber;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(this.departmentNumber);
    }

    @Override
    public int compareTo(RPSDepartment o) {
        if(o instanceof RPSDepartment) {
            return this.description.compareTo(((RPSDepartment) o).description);
        }
        return 0;
    }

    public Calendar getAddedTime() {
	return addedTime;
    }
}