package at.foorada.rk.rps.data;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

/**
 * class represents a shift entry from the rk dienstplan website
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class RPSEntry {

    public static final String NL_String = System.getProperty("line.separator");
    /**
     * format for database timestamp (string sorting possible)<br>
     * format:yyyy-MM-dd HH:mm:ss
     */
    public static final SimpleDateFormat TIMESTAMP = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /**
     * format for simply date (string sorting possible)<br>
     * format:yyyy-MM-dd
     */
    public static final SimpleDateFormat TIMESTAMP_DATE_ONLY = new SimpleDateFormat("yyyy-MM-dd");

    private Type type;

    private boolean deleted = false;
    private String comment;
    private Calendar start;
    private Calendar end;
    private String anker;
    private List<RPSMember> member;
    private RPSDepartment department;
    private Calendar addedTime;

    public RPSEntry() {
        this.member = new ArrayList<>();
        addedTime = Calendar.getInstance();
    }

    public Calendar getAddedTime() {
        return addedTime;
    }

    public long getShiftDurationInMillis() {
        return end.getTimeInMillis() - start.getTimeInMillis();
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setStart(Calendar start) {
        this.start = start;
    }

    public Calendar getStart() {
        return start;
    }

    public Calendar getEnd() {
        return end;
    }

    /**
     * setStart() has to be called before this method.
     * Otherwise an RPSEntryException will be thrown.<br>
     * if the end date is before the start date, the end date will be incremented
     * by one day (nightshift). If the end date is after incrementation 
     * still before the start date a RPSEntryException will be thrown.
     * @param end
     * @throws RPSEntryException thrown in case of not set start
     */
    public void setEnd(Calendar end) throws RPSEntryException {
        if(start == null)
            throw new RPSEntryException("start date must be set bevor end date");

	//check if duty is nightshift
        if(start.compareTo(end) > 0) {
            end.add(Calendar.DAY_OF_MONTH, 1);
	    
	    //if end date is still before start date, something went wrong
	    if(start.compareTo(end) > 0) {
		throw new RPSEntryException("start date is after end date");
	    }
        }
        this.end = end;
    }

    public void setAnker(String anker) {
        this.anker = anker;
    }

    public String getAnker() {
        return this.anker;
    }

    public void addMember(RPSMember member) {
        this.member.add(member);
    }

    public void addMember(String prefix, String memberName) {
        this.member.add(new RPSMember(prefix, memberName));
    }

    public void addMember(String member) {
        addMember(new RPSMember(member));
    }

    public boolean hasMember(RPSMember member) {
        return this.member.contains(member);
    }

    public List<RPSMember> getMemberList() {
        return this.member;
    }

    public boolean hasAnker() {
        return type.hasAnker();
    }

    public boolean isFutureEntry() {
	return end.compareTo(start) > 0;
    }

    public RPSDepartment getDepartment() {
        return department;
    }

    public void setDepartment(RPSDepartment department) {
        this.department = department;
    }

    /**
     * function for checking if an entry is occupied through
     * a given list of entries.
     * while calculating a distance is add to end and subtract from start.
     * It is a offset for the rest to the next shift.
     * The given exception entries will be excluded from the checking list.
     * @param entry RPSEntry to check
     * @param occupyingList list with entries which will occupy the given entry
     * @param safetyDistanceMin offset for start and end of the given event in minutes
     * @param exception events if given, will be excluded from the occupyingList
     * @return 
     */
    public static boolean isRPSEntryOccupied(RPSEntry entry, List<RPSEntry> occupyingList, int safetyDistanceMin, RPSEntry... exception) {
        List<RPSEntry> exceptionList = Arrays.asList(exception);

        Calendar entryStartExtended = (Calendar) entry.getStart().clone();
        entryStartExtended.add(Calendar.MINUTE, -safetyDistanceMin);
        Calendar entryEndExtended = (Calendar) entry.getEnd().clone();
        entryEndExtended.add(Calendar.MINUTE, +safetyDistanceMin);

        for(RPSEntry possibleOccEntry : occupyingList) {
            if(possibleOccEntry.getStart().compareTo(entryStartExtended) > 0 && possibleOccEntry.getStart().compareTo(entryEndExtended) < 0 ||
                    possibleOccEntry.getEnd().compareTo(entryStartExtended) > 0 && possibleOccEntry.getEnd().compareTo(entryEndExtended) < 0) {

                //check if possible entry is not at same time
                Calendar possibleOccEntryStart = (Calendar) possibleOccEntry.getStart().clone();
                possibleOccEntryStart.add(Calendar.MINUTE, +2*60);
                Calendar possibleOccEntryEnd = (Calendar) possibleOccEntry.getEnd().clone();
                possibleOccEntryEnd.add(Calendar.MINUTE, -2*60);

                if(!exceptionList.contains(possibleOccEntry)) {
                    return true;
                } else if((possibleOccEntryStart.compareTo(entry.getStart()) > 0 && possibleOccEntryStart.compareTo(entry.getEnd()) < 0 ||
                        possibleOccEntryEnd.compareTo(entry.getStart()) > 0 && possibleOccEntryEnd.compareTo(entry.getEnd()) < 0)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * check if given string is a placeholder or an real member name.
     * @param member name to check if it is a placeholder.
     * @return 
     */
    public static boolean isValidMember(String member) {
        switch(member) {
            case "Arzt":
            case "Transportfuehrer":
            case "Transportfuehrer-HG":
            case "Auszubildender":
            case "Sanitaeter":
            case "Einsatzlenker":
            case "Einsatzlenker-HG":
            case "HÄND-Einsatzlenker":
            case "Dienstführender":
            case "Disponent-Azubi":
            case "Disponent":
            case "Disponent-DH":
            case "Handy":
            case "Offizier":
            case "Schulungsteilnehmer":
            case "Innendienst":
            case "Journal":
            case "KFZ-DFZ":
            case "SRM-Arzt":
            case "SRM-Einsatzlenker":
            case "SRM-Sanitaeter":
            case "SRM-Auszubildender":
                return false;
            default:
                return true;
        }
    }

    @Override
    public String toString() {
        StringBuilder bldr = new StringBuilder();

        bldr.append("Type: ");
        bldr.append(type);
        bldr.append(NL_String);

        bldr.append("Start: ");
        bldr.append(start.getTime());
        bldr.append(NL_String);

        bldr.append("End: ");
        bldr.append(end.getTime());
        bldr.append(NL_String);

        if(hasAnker()) {
            bldr.append("Anker: ");
            bldr.append(anker);
            bldr.append(NL_String);
        }

        bldr.append("Members: ");
        boolean first =true;
        for(RPSMember m: member) {
            if(first)
                first = false;
            else
                bldr.append(", ");
            bldr.append(m.prefix + m.name);
        }
        bldr.append(NL_String);

        bldr.append("Comment: ");
        bldr.append(comment);
        bldr.append(NL_String);

        return bldr.toString();
    }

    public boolean isDeleted() {
	return deleted;
    }

    public void setDeleted(boolean deleted) {
	this.deleted = deleted;
    }

    public void setAddedTime(Calendar added) {
	this.addedTime = added;
    }

    @Override
    public int hashCode() {
	int hash = 0;
	if(addedTime != null)
	    hash += addedTime.hashCode();
	if(anker != null)
	    hash += anker.hashCode();
	if(comment != null)
	    hash += comment.hashCode();
	hash += Boolean.hashCode(deleted);
	if(department != null)
	    hash += department.hashCode();
	if(end != null)
	    hash += end.hashCode();
//	hash += Integer.hashCode(id);
	if(member != null)
	    for(RPSMember m:member)
		hash += m.hashCode();
	if(start != null)
	    hash += start.hashCode();
	if(type != null)
	    hash += type.hashCode();
	return hash;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj) {
	    return true;
	}
	if (obj == null) {
	    return false;
	}
	if (getClass() != obj.getClass()) {
	    return false;
	}
	final RPSEntry other = (RPSEntry) obj;
	if (this.deleted != other.deleted) {
	    return false;
	}
	if (!Objects.equals(this.comment, other.comment)) {
	    return false;
	}
	if (!Objects.equals(this.anker, other.anker)) {
	    return false;
	}
	if (this.type != other.type) {
	    return false;
	}
	if (!Objects.equals(this.start, other.start)) {
	    return false;
	}
	if (!Objects.equals(this.end, other.end)) {
	    return false;
	}
	if (!Objects.equals(this.member, other.member)) {
	    return false;
	}
	if (!Objects.equals(this.department, other.department)) {
	    return false;
	}
	if (!Objects.equals(this.addedTime, other.addedTime)) {
	    return false;
	}
	return true;
    }
    
    /**
     * enum for shift types
     */
    public enum Type implements Comparable<Type> {
        SEW ("SEW", ""),
        SEW_BACKGROUND ("SEW-Hintergrund", ""),
        ATW ("ATW", ""),
        NEF ("NEF", "N"),
        NAW ("NAW", "N"),
        DISPOSITION_LEITUNG("Disposition Leitung", "L"),
        DISPOSITION("Disposition", "L"),
        EL("Einsatzleitung", "EL"),
        SCHULUNG("Schulung", "S"),
        SCHULUNG_KFZ("Schulung KFZ", "S"),
        DIENSTFUEHRUNG("Dienstführung", "D"),
        DIENSTVERANTWORTUNG("Dienstverantwortung", "D"),
        OVD("OVD", "O"),
        HAEND("HÄND mobil", "H"),
        HAEND_ORDINATION("HÄND-Ordination", ""),
        AMBULANZ("Ambulanz", "A"),
        AMBULANZ_FUSSTRUPP("Ambulanz-Fußtrupp", "A"),
        ID("Innendienst", "I"),
        POOL("Pool", "P"),
        UNKNOWN("Unknown", "?"),
        ALL_TYPES("Alle Typen", "ALL");    //only for deleting

        private String description;
        private String shurtcutDesc;

        Type(String desc, String shortcut) {
            this.description = desc;
            this.shurtcutDesc = shortcut;
        }

        @Override
        public String toString() {
            return description;
        }

        public String getDescription() {
            return description;
        }

        public String getShurtcut() {
            return shurtcutDesc;
        }

	/**
	 * from given string a enum object will be searched,
	 * or otherwise the UNKNOWN object if nothing is found.<br>
	 * The real description from the enum object will be searched.
	 * Not like "getFromPlanString".
	 * @param type string for finding Type enum
	 * @return 
	 */
        public static Type getFromString(String type) {
            for(Type d : Type.values())
                if(d.description.compareTo(type) == 0)
                    return d;
            return UNKNOWN;
        }

	/**
	 * from given string a enum object will be searched,
	 * or otherwise the UNKNOWN object if nothing is found.<br>
	 * The description from the website will be searched.
	 * Not like "getFromString".
	 * @param type string for finding Type enum
	 * @return 
	 */
        public static Type getFromPlanString(String type) {
            switch(type) {
                case "ATW":
                    return ATW;
                case "SEW":
                    return SEW;
                case "SEW-Hintergrund":
                    return SEW_BACKGROUND;
                case "NEF":
                    return NEF;
                case "NAW":
                    return NAW;
                case "Disposition":
                    return DISPOSITION;
                case "Einsatzleitung":
                    return EL;
                case "Disposition-Leitung":
                    return DISPOSITION_LEITUNG;
                case "Schulung":
                    return SCHULUNG;
                case "Schulung-KFZ":
                    return SCHULUNG_KFZ;
                case "Dienstführung":
                    return DIENSTFUEHRUNG;
                case "Dienstverantwortung":
                    return DIENSTVERANTWORTUNG;
                case "OVD":
                    return OVD;
                case "HÄND mobil":
                    return HAEND;
                case "HÄND-Ordination":
                    return HAEND_ORDINATION;
                case "Ambulanz":
                    return AMBULANZ;
                case "Ambulanz-Fusstrupp":
                    return AMBULANZ_FUSSTRUPP;
                case "ID":
                    return ID;
                default:
                    return UNKNOWN;
            }
        }

	/**
	 * check if this type is a possible one for shift-swapping
	 * @return 
	 */
        public boolean isTypeForSwapping() {
            switch(this) {
                case SEW:
                case SEW_BACKGROUND:
                case ATW:
                case DISPOSITION:
                case DISPOSITION_LEITUNG:
                case HAEND:
                case POOL:
                    return true;
                default:
                return false;
            }
        }

	/**
	 * check if this type has an anker (most of the time it is an SEW)
	 * @return 
	 */
        public boolean hasAnker() {
            switch(this) {
                case SEW:
                case SEW_BACKGROUND:
                case ATW:
                case NEF:
                case NAW:
                case HAEND:
                case AMBULANZ:
                case SCHULUNG_KFZ:
                    return true;
                case DISPOSITION:
                case DISPOSITION_LEITUNG:
                case AMBULANZ_FUSSTRUPP:
                case SCHULUNG:
                case DIENSTFUEHRUNG:
                case DIENSTVERANTWORTUNG:
                case OVD:
                case HAEND_ORDINATION:
                case ID:
                case POOL:

                case UNKNOWN:
                default:
                    return false;
            }
        }
    }

    /**
     * exception class for RPS (entry) issues
     */
    public static class RPSEntryException extends Exception {
        public RPSEntryException(String text) {
            super(text);
        }
    }
}
