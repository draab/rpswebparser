package at.foorada.rk.rps.data;

/**
 * class represents a member of a shift in the rk dienstplan website
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class RPSMember implements Comparable<RPSMember> {
    /**
     * known prefixes of names
     * TODO move to configuration and make it editable
     */
    public static final String[] PREFIX_ARRAY = {"PA ","SEF ", "GK ", "ZK ", "NFS ", "OF ", "ZD ", "KTW "};
    public static final String NAME_REGEX = "[^a-zA-Z0-9öäüß -|:()\\[\\]]";
    public String prefix;
    public String name;

    public RPSMember(String fullName) {
        fullName = fullName.replaceAll(NAME_REGEX, "");
        fullName = fullName.trim();
        int idx = RPSMember.getPrefixEndIndex(fullName);
        prefix = fullName.substring(0, idx);
        name = removeBrackets(fullName.substring(idx));
    }

    public RPSMember(String prefix, String name) {
        this.prefix = prefix;
        this.name = removeBrackets(name);
    }
    
    public String getFullName() {
        return this.prefix + this.name;
    }

    /**
     * remove brackets and their content
     * @param name
     * @return 
     */
    public static String removeBrackets(String name) {

        int start = name.indexOf('(');
        int end = name.indexOf(')');

        if(end >=0 && start >= 0) {
            name = name.substring(0, start) +
                    name.substring(end+1);
        }

        return name.trim();
    }

    /**
     * returns the index of the prefix end of the given string
     * @param memberName
     * @return 
     */
    public static int getPrefixEndIndex(String memberName) {
        int prefixEndIdx = 0;

        for(int i=0; i < PREFIX_ARRAY.length; i++) {
            if(memberName.startsWith(PREFIX_ARRAY[i], prefixEndIdx)) {
                int length = PREFIX_ARRAY[i].length();
                prefixEndIdx += length;

                i = -1;
            }
        }

        return prefixEndIdx;
    }

    @Override
    public int compareTo(RPSMember o) {
        return this.name.compareTo(((RPSMember) o).name);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof RPSMember && this.name.equals(((RPSMember) obj).name);
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    public String getName() {
        return name;
    }
}
