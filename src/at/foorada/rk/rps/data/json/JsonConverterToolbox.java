/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.json;

import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.data.RPSMember;
import at.foorada.rk.rps.data.QuickLoadConfiguration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This class is a toolbox to convert objects to json (org.json.simple) and
 * reverse
 *
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public final class JsonConverterToolbox {

    private static final Logger LOGGER = Logger.getLogger(JsonConverterToolbox.class.getName());

    private JsonConverterToolbox() {
    }

    //<editor-fold defaultstate="collapsed" desc="RPS Member functions">
    /**
     * converts member list to JSONArray object
     *
     * @param collection
     * @return JSONArray object containing member list
     */
    public static JSONArray memberListToJson(Collection<RPSMember> collection) {
	JSONArray arr = new JSONArray();

	collection.forEach((s) -> {
	    arr.add(s.getFullName());
	});

	return arr;
    }

    /**
     * converts a member JSONArray to an ArrayList (reversed to the function
     * memberListToJson)
     *
     * @param memberArray
     * @return
     */
    public static List<RPSMember> toMemberList(JSONArray memberArray) {
	List<RPSMember> list = new ArrayList<>();

	memberArray.forEach((s) -> {
	    list.add(new RPSMember((String) s));
	});

	return list;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="RPS Department functions">
    /**
     * converts a department object to a JSONObject (reversed to function
     * toDepartmentObj)
     *
     * @param department
     * @return
     */
    public static JSONObject departmentToJson(RPSDepartment department) {

	JSONObject obj = new JSONObject();

	obj.put("id", department.getId());
	obj.put("description", department.getDescription());
	return obj;
    }

    /**
     * converts a JSONObject to an department object (reversed to function
     * departmentToJson)
     *
     * @param json
     * @return
     */
    public static RPSDepartment toDepartmentObj(JSONObject json) {
	return new RPSDepartment(
		((Long) json.get("id")).intValue(),
		(String) (json).get("description"));
    }

    /**
     * converts an department list to a JSONArray object (reversed to
     * toDepartmentList)
     *
     * @param collection
     * @return JSONArray object containing given department
     */
    public static JSONArray departmentListToJson(Collection<RPSDepartment> collection) {
	JSONArray departmentArr = new JSONArray();

	collection.forEach((d) -> {
	    departmentArr.add(departmentToJson(d));
	});

	return departmentArr;
    }

    /**
     * converts a JSONArray to a department ArrayList (reversed to
     * departmentListToJson)
     *
     * @param jsonDepartmentList
     * @return
     */
    public static List<RPSDepartment> toDepartmentList(JSONArray jsonDepartmentList) {
	List<RPSDepartment> dprt = new ArrayList();

	jsonDepartmentList.forEach((Object o) -> {
	    if (o instanceof JSONObject) {
		dprt.add(toDepartmentObj((JSONObject) o));
	    }
	});
	return dprt;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="RPS Entry functions">
    /**
     * converts a entry to an JSONObject
     *
     * @param e
     * @return
     */
    public static JSONObject entryToJson(RPSEntry e) {

	JSONObject obj = new JSONObject();

	obj.put("start", e.getStart().getTimeInMillis());
	obj.put("end", e.getEnd().getTimeInMillis());
	JSONObject jsonDep = new JSONObject();
	jsonDep.put("id", e.getDepartment().getId());
	jsonDep.put("description", e.getDepartment().getDescription());
	obj.put("department", jsonDep);
	obj.put("anker", e.getAnker());
	obj.put("type", e.getType().getDescription());
	obj.put("comment", e.getComment());
	obj.put("memberList", memberListToJson(e.getMemberList()));
	obj.put("addedTime", e.getAddedTime().getTimeInMillis());

	return obj;
    }

    /**
     * converts a JSONObject to an RPSEntry (reversed to entryToJson)
     *
     * @param json
     * @return
     * @throws at.foorada.data.RPSEntry.RPSEntryException
     */
    public static RPSEntry toEntryObj(JSONObject json) throws RPSEntry.RPSEntryException {

	RPSEntry e = new RPSEntry();

	Calendar start = Calendar.getInstance();
	start.setTimeInMillis((Long) json.get("start"));
	e.setStart(start);

	Calendar end = Calendar.getInstance();
	end.setTimeInMillis((Long) json.get("end"));
	e.setEnd(end);

	Calendar addedTime = Calendar.getInstance();
	addedTime.setTimeInMillis((Long) json.get("addedTime"));
	e.setAddedTime(addedTime);

	JSONObject jsonDep = (JSONObject) json.get("department");
	e.setDepartment(
		new RPSDepartment(
			((Long) jsonDep.get("id")).intValue(),
			(String) jsonDep.get("description")));

	e.setAnker((String) json.get("anker"));
	e.setType(RPSEntry.Type.getFromString((String) json.get("type")));
	e.setComment((String) json.get("comment"));

	JSONArray jsonMem = (JSONArray) json.get("memberList");
	jsonMem.forEach((m) -> {
	    e.addMember((String) m);
	});

	return e;
    }

    /**
     * converts a entry list to a JSONArray (reversed to toEntryList)
     *
     * @param collection
     * @return JSONArray
     */
    public static JSONArray entryListToJson(Collection<RPSEntry> collection) {
	JSONArray entryArr = new JSONArray();

	collection.forEach((e) -> {
	    entryArr.add(entryToJson(e));
	});

	return entryArr;
    }

    /**
     * converts a JSONArray to an ArrayList (reversed to entryListToJson)
     *
     * @param jsonEntryList
     * @return
     */
    public static List<RPSEntry> toEntryList(JSONArray jsonEntryList) {
	List<RPSEntry> list = new ArrayList<>();

	jsonEntryList.forEach((o) -> {
	    if (o instanceof JSONObject) {

		try {
		    list.add(toEntryObj((JSONObject) o));
		} catch (RPSEntry.RPSEntryException ex) {
		    LOGGER.log(Level.SEVERE, "exception occured while reading entry from json", ex);
		}
	    }
	});

	return list;
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Quick load configuration function">
    
    /**
     * the method puts the quick load configuration value to a JSONObject (reversed to toQuickLoadConfig)
     * @param config
     * @return JSONObject
     */
    public static JSONObject quickLoadConfigToJson(QuickLoadConfiguration config) {
        final JSONObject qlJson = new JSONObject();
        
        //add information from quick load configuration object to json object
        qlJson.put("mail", config.mail);
        qlJson.put("offset", config.offset);
        qlJson.put("days", config.days);
        qlJson.put("departments", departmentListToJson(config.departments));
        
        return qlJson;
    }
    
    
    /**
     * converts the JSONObject to a quick load configuration object (reversed to quickLoadConfigToJson)
     * @param jsonConfig
     * @return 
     */
    public static QuickLoadConfiguration toQuickLoadConfig(JSONObject jsonConfig) {
        
            QuickLoadConfiguration qlConfig = new QuickLoadConfiguration();
            qlConfig.mail = (String) jsonConfig.get("mail");
            qlConfig.offset = ((Long) jsonConfig.get("offset")).intValue();
            qlConfig.days = ((Long) jsonConfig.get("days")).intValue();
	    qlConfig.departments.addAll(toDepartmentList((JSONArray)jsonConfig.get("departments")));
	    
	    return qlConfig;
    }
    
    
    //</editor-fold>
}
