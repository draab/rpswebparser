package at.foorada.rk.rps.data.storage.db;

import java.io.IOException;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class DataBase {
    
    private static final Logger LOGGER = Logger.getLogger(DataBase.class.getName());
    public static final String LINE_SEPERATOR = System.getProperty("line.separator");

    Connection c = null;

    public String listTables() throws IOException {
        return listTables(true);
    }

    public String listTables(boolean output) throws IOException {

        StringBuilder bldr = new StringBuilder();

        try {
            Statement stmt = c.createStatement();
            String sql = "SELECT name FROM sqlite_master WHERE type='table'";
            ResultSet res = stmt.executeQuery(sql);

            boolean first = true;
            while (res.next()) {
                if (first)
                    first = false;
                else
                    bldr.append(LINE_SEPERATOR);
                bldr.append(res.getString("name"));
            }

            if(first)
                bldr.append("no tables in database");

            stmt.close();
        }catch(SQLException sqle) {
	    throw new IOException(sqle);
        }

        if(output)
	    LOGGER.log(Level.INFO, bldr.toString());

        return bldr.toString();
    }

    public void showTable(String tableName) throws IOException {
        try {
            Statement stmt = c.createStatement();
            String sql = "SELECT * FROM "+tableName;
            ResultSet res = stmt.executeQuery(sql);
            ResultSetMetaData rsmd = res.getMetaData();
            int columnCount = rsmd.getColumnCount();
            for(int i=1; i <= columnCount; i++) {
                System.out.format("%25s", rsmd.getColumnName(i));
            }
            System.out.println();
            while(res.next()) {
                for(int i=1; i <= columnCount; i++) {
                    String param = res.getString(i);
                    if(param == null)
                        param = "";
                    int subidx = 22;
                    if(subidx >= param.length())
                        subidx = param.length();
                    System.out.format("%25s", param.substring(0,subidx));
                }
                System.out.println();
            }
        }catch(SQLException sqle) {
	    throw new IOException(sqle);
        }
    }

    public abstract void close() throws IOException;

    public Connection getConnection() {
        return c;
    }
    
    public abstract String getHostNameInfo();
}
