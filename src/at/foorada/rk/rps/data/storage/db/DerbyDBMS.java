package at.foorada.rk.rps.data.storage.db;

import java.io.File;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DerbyDBMS extends DataBase {
    private static final Logger LOGGER = Logger.getLogger(DerbyDBMS.class.getName());
    
    private static final String CLASS_NAME = "org.apache.derby.jdbc.EmbeddedDriver";
    private static final String PROTOCOL = "jdbc:derby:";
    
    private String filename;

    public DerbyDBMS(String filename) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        throw new UnsupportedOperationException();
//        try {
//            Class.forName(CLASS_NAME).newInstance();
//            Properties props = new Properties(); // connection properties
//            // providing a user name and password is optional in the embedded
//            // and derbyclient frameworks
//            props.put("user", "user1");
//            props.put("password", "user1");
//
////            c = DriverManager.getConnection(PROTOCOL + filename +";create=true", props);
//            c = DriverManager.getConnection(PROTOCOL + filename +";create=true");
//            System.out.println("Derby DB opened");
//        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
//            e.printStackTrace();
//            throw e;
//        }
    }

    @Override
    public void close() {
	
	if( c!= null) {
	    try {
		c.close();
		DriverManager.getConnection(PROTOCOL +";shutdown=true");
		LOGGER.finest("Derby DB closed");
	    } catch(SQLException se) {

		if (( (se.getErrorCode() == 50000)
			&& ("XJ015".equals(se.getSQLState()) ))) {
		    // we got the expected exception
		    
		    LOGGER.log(Level.INFO, "Derby shut down normally");
		    // Note that for single database shutdown, the expected
		    // SQL state is "08006", and the error code is 45000.
		} else {
		    // if the error code or SQLState is different, we have
		    // an unexpected exception (shutdown failed)
		    LOGGER.log(Level.WARNING, "Derby did not shut down normally");
		}
	    }
	}
    }

    @Override
    public String getHostNameInfo() {
	File file = new File(filename);
	return file.getName();
    }
}
