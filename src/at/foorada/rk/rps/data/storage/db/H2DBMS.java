package at.foorada.rk.rps.data.storage.db;

import java.io.File;
import java.io.IOException;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class H2DBMS extends DataBase {
    
    private static final Logger LOGGER = Logger.getLogger(H2DBMS.class.getName());

    private static final String CLASS_NAME = "org.h2.Driver";
    private static final String PROTOCOL = "jdbc:h2:";
    
    private String filename;

    public H2DBMS(String filename) throws SQLException, ClassNotFoundException {

        throw new UnsupportedOperationException();

//        try {
//            Class.forName(CLASS_NAME);
//            c = DriverManager.getConnection(PROTOCOL + filename);
//            System.out.println("Database opened successfully: "+filename);
//        } catch(ClassNotFoundException cnfe) {
//            System.err.println("JDBC class not found: "+ CLASS_NAME);
//            throw cnfe;
//        } catch(SQLException sqle) {
//            System.err.println("Can't connect to database: "+ PROTOCOL);
//            throw sqle;
//        }
    }

    
    @Override
    public void close() throws IOException {
	
	if(c != null) {
	    try {
		c.close();
		LOGGER.log(Level.INFO, "Database closed");
	    } catch(SQLException e) {
		throw new IOException(e);
	    }
	}
    }

    @Override
    public String getHostNameInfo() {
	File file = new File(filename);
	return file.getName();
    }
    
    
}
