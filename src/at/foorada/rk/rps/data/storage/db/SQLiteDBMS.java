package at.foorada.rk.rps.data.storage.db;

import java.io.File;
import java.io.IOException;

import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SQLiteDBMS extends DataBase {

    private static final Logger LOGGER = Logger.getLogger(SQLiteDBMS.class.getName());
    
    public static final String LINE_SEPERATOR = System.getProperty("line.separator");
    private static final String CLASS_NAME = "org.sqlite.JDBC";
    private static final String PROTOCOL = "jdbc:sqlite:";
    
    private String filename;

    public SQLiteDBMS(String filename) throws SQLException, ClassNotFoundException {
	this.filename = filename;

        try {
            Class.forName(CLASS_NAME);
            Properties properties = new Properties();
            properties.setProperty("characterEncoding", "UTF-8");
            properties.setProperty("encoding", "\"UTF-8\"");
            c = DriverManager.getConnection(PROTOCOL + filename, properties);
	    LOGGER.log(Level.FINEST, "Database opened successfully: {0}", filename);
        } catch(ClassNotFoundException cnfe) {
	    LOGGER.log(Level.WARNING, "JDBC class not found: {0}",CLASS_NAME);
            throw cnfe;
        } catch(SQLException sqle) {
	    LOGGER.log(Level.WARNING, "Can't connect to database: {0}{1}" + PROTOCOL, filename);
            throw sqle;
        }
    }

    @Override
    public String getHostNameInfo() {
	File file = new File(filename);
	return file.getName();
    }
    
    @Override
    public void close() throws IOException {
	if(c != null) {
	    try {
		c.close();
		LOGGER.finest("Database closed");
	    } catch (SQLException e) {
		throw new IOException(e);
	    }
	}
    }
}
