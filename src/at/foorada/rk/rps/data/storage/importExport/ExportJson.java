/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.foorada.rk.rps.data.storage.importExport;
import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.RPSMember;
import at.foorada.rk.rps.data.json.JsonConverterToolbox;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Logger;

import org.json.simple.JSONObject;

/**
 *
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class ExportJson implements Exporter {

    private static final Logger LOGGER = Logger.getLogger(ExportJson.class.getName());

    private final File file;
    private final DataAccessModule dataAccess;
    private final ImportExportModule module;
    
    private boolean initDone = false;
    
    public ExportJson(File file, DataAccessModule dataAccess, ImportExportModule module) {
	this.file = file;
	this.dataAccess = dataAccess;
	this.module = module;
    }

    @Override
    public void init() throws IOException {
	
	//create file if not exists
	if (!file.exists()) {
	    file.createNewFile();
	}
	
	if(!(file.canWrite() && file.isFile()))
	    throw new IOException("given file isnt a file or no write permissions.");
	
	
	LOGGER.fine("file for export is ready");
	initDone = true;
    }
    
    @Override
    public void export() throws IOException {
	
	if(!initDone) throw new IllegalStateException("init method has not been called (successfully)");
	
	//get data depending on the selected module
	Collection<RPSMember> collToExport;
	switch(module) {
	    case SELECTED_MEMBER:
		collToExport = dataAccess.getDataAccessProperty().get().getSelectedMemberSet();
		break;
	    case MEMBER_TO_MEMBER:
		collToExport = dataAccess.getDataAccessProperty().get().getM2MSet();
		break;
	    default:
		throw new AssertionError(module.name());
	}
	
	//generate json object for writing to file
	JSONObject root = new JSONObject();
	root.put("memberList",JsonConverterToolbox.memberListToJson(collToExport));
	
	try (FileWriter writer = new FileWriter(file)) {
	    writer.write(root.toJSONString());
	    //no flush or close need her (automatically because of try with resources)
	    LOGGER.info("export in json file was successful");
	}
    }
}