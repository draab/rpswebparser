/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.storage.importExport;

import java.util.logging.Logger;
import javafx.concurrent.Task;

/**
 *
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class ExportTask extends Task<ExportTask.ExportTaskReturn> {

    private static final Logger LOGGER = Logger.getLogger(ExportTask.class.getName());

    private final Exporter exporter;
    
    public ExportTask(Exporter exporter) {
	this.exporter = exporter;
    }

    /**
     *
     * @return @throws Exception
     */
    @Override
    protected ExportTaskReturn call() throws Exception {
	

	exporter.init();
	
	exporter.export();
	    
	return ExportTaskReturn.SUCCEEDED;

    }

    public enum ExportTaskReturn {
	SUCCEEDED, FAILED;
    }
}
