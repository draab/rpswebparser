/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.foorada.rk.rps.data.storage.importExport;
import java.io.IOException;

/**
 *  interface for exporting configurations or module lists
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public interface Exporter {

    /**
     * method for preparing exporting<br>
     * e.g. open/create files etc.
     * @throws IOException 
     */
    public void init() throws IOException;
    
    /**
     * the export work is done here<br>
     * e.g. appending string, writing to file, etc.
     * @throws IOException 
     */
    public void export() throws IOException;

}
