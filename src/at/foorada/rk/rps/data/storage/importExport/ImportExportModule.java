/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.storage.importExport;


/**
 * module for im- and exporting
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public enum ImportExportModule {

    SELECTED_MEMBER("Mitarbeiterliste", true),
    MEMBER_TO_MEMBER("Mitarbeiter zu Mitarbeiter", true);
    //QUICK_LOAD_CONFIGURATION

    private final String desc;
    private final boolean listImportExport;
    
    private boolean addList = false;

    private ImportExportModule(String description, boolean listImportExport) {
	desc = description;
	this.listImportExport = listImportExport;
    }

    @Override
    public String toString() {
	return desc;
    }

    public void setAddList(boolean listParam1) {
	this.addList = listParam1;
    }

    public boolean isAddList() {
	return addList;
    }

    public boolean isListImportExport() {
	return listImportExport;
    }
}