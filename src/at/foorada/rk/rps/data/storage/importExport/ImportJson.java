/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.storage.importExport;

import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.RPSMember;
import at.foorada.rk.rps.data.json.JsonConverterToolbox;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessInterface;
import at.foorada.rk.rps.gui.MainWindowController;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class ImportJson implements Importer {

	private static final Logger LOGGER = Logger.getLogger(ImportJson.class.getName());

	private final File file;
	private final DataAccessModule dataAccess;
	private final ImportExportModule module;
	private boolean initDone = false;
	private Object importedObject = null;

	public ImportJson(File file, DataAccessModule dataAccess, ImportExportModule module) {
		this.file = file;
		this.dataAccess = dataAccess;
		this.module = module;
	}

	@Override
	public void init() throws IOException {

		//check if given file is ready to read
		if (!(file.exists() && file.isFile() && file.canWrite())) {
			throw new IOException("given file isnt a file or no write permissions.");
		}

		LOGGER.fine("file ready for import");
		initDone = true;
	}

	@Override
	public Object readImport() throws IOException {

		if (!initDone) {
			throw new IllegalStateException("init method has not been called (successfully)");
		}

		try {
			//parse json file
			JSONObject root = (JSONObject) new JSONParser().parse(new FileReader(file));

			//convert json object to corresponding objects
			switch (module) {
				case SELECTED_MEMBER:
				case MEMBER_TO_MEMBER:
					importedObject = JsonConverterToolbox.toMemberList((JSONArray) root.get("memberList"));
					break;
				default:
					throw new AssertionError(module.name());
			}

			return importedObject;
		} catch (ParseException ex) {
			throw new IOException(ex);
		}
	}

	@Override
	public void _import() throws IOException {

		//check if read job is already done
		if (importedObject == null) {
			throw new IllegalStateException("readImport method has not been called (successfully)");
		}

		switch (module) {
			case SELECTED_MEMBER:
				//check if values will be added or replace current ones
				if(module.isAddList())
					dataAccess.getDataAccessProperty().get().addSelectedMemberList((Collection<RPSMember>) importedObject);
				else
					dataAccess.getDataAccessProperty().get().setSelectedMemberList((Collection<RPSMember>) importedObject);
				break;

			case MEMBER_TO_MEMBER:
				//check if values will be added or replace current ones
				if(module.isAddList())
					dataAccess.getDataAccessProperty().get().addM2MList((Collection<RPSMember>) importedObject);
				else
					dataAccess.getDataAccessProperty().get().setM2MList((Collection<RPSMember>) importedObject);
				break;

			default:
				throw new AssertionError(module.name());
		}

		MainWindowController.doAppReCalculation();
	}
}
