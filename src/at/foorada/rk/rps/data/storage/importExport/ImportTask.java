/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.foorada.rk.rps.data.storage.importExport;
import at.foorada.rk.rps.gui.MainWindowController;
import java.util.logging.Logger;
import javafx.concurrent.Task;

/**
 *
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class ImportTask extends Task<ImportTask.ImportTaskReturn> {

    private static final Logger LOGGER = Logger.getLogger(ImportTask.class.getName());

    private final Importer importer;
    
    public ImportTask(Importer importer) {
	this.importer = importer;
    }

    /**
     *
     * @return @throws Exception
     */
    @Override
    protected ImportTaskReturn call() throws Exception {
	

	importer.init();
	
	importer.readImport();

	importer._import();
	
	MainWindowController.doAppReCalculation();
	
	return ImportTaskReturn.SUCCEEDED;

    }

    public enum ImportTaskReturn {
	SUCCEEDED, FAILED;
    }
}
