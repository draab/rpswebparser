/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.foorada.rk.rps.data.storage.importExport;
import java.io.IOException;

/**
 * interface for importing files to the application
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public interface Importer {
        
    /**
     * method for preparing importing<br>
     * e.g. open/reading files etc.
     * @throws IOException 
     */
    public void init() throws IOException;
    
    /**
     * the read work for the input is done here
     * and if it successful it will be return by the method<br>
     * the return type is depending on the selected Module
     * @return
     * @throws IOException 
     */
    public Object readImport() throws IOException;
    
    /**
     * the import work is done here<br>
     * e.g. reading to file, building objects etc.
     * @throws IOException 
     */
    public void _import() throws IOException;
}
