package at.foorada.rk.rps.data.storage.main;

import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessInterface;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessModuleEnum;
import javafx.concurrent.Task;

import java.io.File;
import java.util.logging.Logger;

/**
 *
 *
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 17.03.2017.
 */
public class LoadDataAccessTask extends Task<Boolean> {

    private static final Logger LOGGER = Logger.getLogger(LoadDataAccessTask.class.getName());
    private final File fileToLoad;
    private final DataAccessModule dataAccessModule;

    private int stepOffset;
    private final int steps;

    public LoadDataAccessTask(DataAccessModule dataAccess, File fileToLoad) {
        this(dataAccess, fileToLoad, -1, -1);
    }

    public LoadDataAccessTask(DataAccessModule dataAccess, File fileToLoad, int stepOffset, int steps) {
        this.dataAccessModule = dataAccess;
        this.fileToLoad = fileToLoad;
        this.stepOffset = stepOffset;
        this.steps = steps;
    }

    @Override
    protected Boolean call() throws Exception {
        DataAccessInterface dataAccess = DataAccessModuleEnum.getInterfaceFromFile(fileToLoad);

        updateProgress(stepOffset++,steps);
        updateMessage("loading data ...");
        LOGGER.fine("loading data ...");

        updateProgress(stepOffset++,steps);
        updateMessage("loading entries ...");
        LOGGER.fine("loading entries ...");

        dataAccessModule.getDataAccessProperty().set(dataAccess);

        updateProgress(stepOffset++,steps);
        updateMessage("do calculations for displaying ...");
        LOGGER.fine("do calculations for displaying ...");

        updateProgress(stepOffset++,steps);
        updateMessage("updating gui ...");
        LOGGER.fine("updating gui ...");

        return Boolean.TRUE;
    }
}
