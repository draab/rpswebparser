/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.storage.main.dataAccess;

import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import static at.foorada.rk.rps.data.RPSEntry.TIMESTAMP;
import at.foorada.rk.rps.data.RPSMember;
import at.foorada.rk.rps.data.storage.db.DataBase;
import at.foorada.rk.rps.data.QuickLoadConfiguration;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Data Access class for database access<br>
 * <b>only tested with local sqlite database</b>
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class DataAccessDatabase extends DataAccessMemoryBuffer {

	private static final Logger LOGGER = Logger.getLogger(DataAccessDatabase.class.getName());
	private DataBase db;

	public DataAccessDatabase(DataBase db) throws IOException {
		super();
		this.db = db;

		try {
			initDataBaseTables();   // create tables
			loadDataToMemory();	    // load all data from db to memory
		} catch(SQLException e) {
			throw new IOException(e);
		}
	}

	@Override
	public void close() throws IOException {
		super.close();

		if(db != null)
			db.close();
	}

	@Override
	public void save() throws IOException {

		try {
			writeRPSEntriesToStorage();
			writeRPSDepartmentsToStorage();
			writeSelectedMembersToStorage();
			writeM2MToStorage();
			writeQuickLoadConfigurationToStorage();

			memoryHasChanged = false;

		} catch (RPSEntry.RPSEntryException | SQLException ex) {
			throw new IOException("exception in save method on DataAccessDatabase", ex);
		}
	}

    
    /*
    * private methods
    */

	/**
	 * set up all tables for stored data
	 * @throws SQLException
	 */
	private void initDataBaseTables() throws SQLException {
		createConfigurationTable();
		createRPSDepartmentTable();
		createRPSEntryTable();
		createRPSMemberTable();
		createAdditionalMemberTable();
	}

	/**
	 * create table if not exists call for configuration
	 * @throws SQLException
	 */
	private void createConfigurationTable() throws SQLException {

		try ( /*
	 * create configuration table if not exists
	 */ Statement stmt = db.getConnection().createStatement()) {
			String sql = "CREATE TABLE IF NOT EXISTS APP_CONFIGURATION " +
					"(CONFIG_KEY  VARCHAR(50)  PRIMARY KEY  NOT NULL, " +
					" CONFIG_VALUE  TEXT  NOT NULL)";
			stmt.executeUpdate(sql);

			LOGGER.finest("Configuration Table created successfully");
		}
	}

	/**
	 * create table if not exists call for rps departments
	 * @throws SQLException
	 */
	private void createRPSDepartmentTable() throws SQLException {
	 /*
	 * create rps department table if not exists
	 */
		try (Statement stmt = db.getConnection().createStatement()) {
			String sql = "CREATE TABLE IF NOT EXISTS RPS_ENTRY_DEPARTMENT " +
					"(DEPARTMENT_ID  INTEGER PRIMARY KEY NOT NULL, " +
					" DEPARTMENT_NAME  TEXT  NOT NULL, " +
					" ADDED_TIME INTEGER NOT NULL)";
			stmt.executeUpdate(sql);
			LOGGER.finest("RPSDepartment Table created successfully");
		}
	}

	/**
	 * create table if not exists call for rps entries
	 * @throws SQLException
	 */
	private void createRPSEntryTable() throws SQLException {
	 /*
	 * create rps entry table if not exists
	 */
		try ( Statement stmt = db.getConnection().createStatement()) {
			String sql = "CREATE TABLE IF NOT EXISTS RPS_ENTRY " +
					"(ID  INTEGER  PRIMARY KEY  NOT NULL," +
					" DEPARTMENT_ID  INTEGER  NOT NULL, " +
					" TYPE  TEXT  NOT NULL, " +
					" COMMENT  TEXT  NOT NULL, " +
					" START  TEXT  NOT NULL, " +
					" END  TEXT  NOT NULL, " +
					" ANKER  TEXT, " +
					" FUTURE_ENTRY  INTEGER, " +
					" DELETED INTEGER, " +
					" ADDED_TIME INTEGER)";
			stmt.executeUpdate(sql);
			LOGGER.finest("RPSEntry Table created successfully");
		}
	}

	/**
	 * create table if not exists call for rps members
	 * @throws SQLException
	 */
	private void createRPSMemberTable() throws SQLException {
	/*
	 * create rps member table if not exists
	 */
		try ( Statement stmt = db.getConnection().createStatement()) {
			String sql = "CREATE TABLE IF NOT EXISTS RPS_ENTRY_MEMBERS " +
					"(ENTRY_ID  INTEGER  NOT NULL, " +
					" ENTRY_PLACE  INTEGER  NOT NULL, " +
					" MEMBER_NAME  TEXT  NOT NULL)";
			stmt.executeUpdate(sql);
			LOGGER.finest("RPSMember Table created successfully");
		}
	}

	/**
	 * create tabel if not exists call for selected member and member to member module
	 * @throws SQLException
	 */
	private void createAdditionalMemberTable() throws SQLException {
	/*
	 * create rps mapping tables if not exists
	 */
		try ( Statement stmt = db.getConnection().createStatement()) {
			String sql = "CREATE TABLE IF NOT EXISTS RPS_MEMBER_SELECTED " +
					"(MEMBER_NAME  TEXT  NOT NULL)";
			stmt.executeUpdate(sql);
			sql = "CREATE TABLE IF NOT EXISTS RPS_MEMBER_TO_MEMBER " +
					"(MEMBER_NAME  TEXT  NOT NULL)";
			stmt.executeUpdate(sql);
			LOGGER.finest("Additional RPSMember Table created successfully");
		}
	}

	/**
	 * load data from database to memory
	 * @throws SQLException
	 */
	private void loadDataToMemory() throws SQLException {

		loadRPSDepartments();
		loadRPSEntrySet();

		loadSelectedMembers();
		loadM2M();

		loadConfiguration();
	}

	/**
	 * load rps entry from database to memory (included rps members)
	 * @throws SQLException
	 */
	private void loadRPSEntrySet() throws SQLException {

		Connection con = db.getConnection();

		String sql = "SELECT * FROM RPS_ENTRY WHERE " +
				" DELETED = 0";

		try(Statement selectMain = con.createStatement();
			ResultSet entries = selectMain.executeQuery(sql)) {
			PreparedStatement memberSql = db.getConnection().prepareStatement("SELECT * FROM RPS_ENTRY_MEMBERS " +
					" WHERE ENTRY_ID=? ORDER BY ENTRY_PLACE");

			while(entries.next()) {

				RPSEntry newEntry = new RPSEntry();
				newEntry.setDepartment(DataAccessInterface.filterRPSDepartmentFromID(getRPSDepartmentSet(), entries.getInt("DEPARTMENT_ID")));
				newEntry.setType(RPSEntry.Type.getFromString(entries.getString("TYPE")));
				newEntry.setComment(entries.getString("COMMENT"));
				Calendar start = Calendar.getInstance();
				start.setTime(TIMESTAMP.parse(entries.getString("START")));
				newEntry.setStart(start);
				Calendar end = Calendar.getInstance();
				end.setTime(TIMESTAMP.parse(entries.getString("END")));
				newEntry.setEnd(end);
				newEntry.setAnker(entries.getString("ANKER"));
				newEntry.setDeleted(entries.getInt("DELETED") == 1);
				Calendar added = Calendar.getInstance();
				added.setTimeInMillis(entries.getLong("ADDED_TIME"));
				newEntry.setAddedTime(added);

				memberSql.setInt(1, entries.getInt("ID"));
				try (ResultSet memberSet = memberSql.executeQuery()) {
					while(memberSet.next()) {
						RPSMember mem = new RPSMember(memberSet.getString("MEMBER_NAME"));
						newEntry.addMember(mem);
					}
				}
				super.rpsEntrySet.add(newEntry);
			}
		} catch(RPSEntry.RPSEntryException | ParseException ex) {
			LOGGER.log(Level.SEVERE, "error in reading rps entry from db", ex);
		}
	}
    
    /*
    * read from storage to space methods
    */

	/**
	 * read rps departments from database to memory
	 * @throws SQLException
	 */
	private void loadRPSDepartments() throws SQLException {
		rpsDepartmentSet.clear();

		Connection con = db.getConnection();
		String sql = "SELECT * FROM RPS_ENTRY_DEPARTMENT";

		try(Statement selectMain = con.createStatement();
			ResultSet departments = selectMain.executeQuery(sql)) {
			while(departments.next()) {
				RPSDepartment department = new RPSDepartment(departments.getInt("DEPARTMENT_ID"), departments.getString("DEPARTMENT_NAME"));
				Calendar added = Calendar.getInstance();
				added.setTimeInMillis(departments.getLong("ADDED_TIME"));
				department.setAddedTime(added);

				rpsDepartmentSet.add(department);
			}
		}
	}

	/**
	 * load selected members from database
	 * @throws SQLException
	 */
	private void loadSelectedMembers() throws SQLException {
		selectedMemberSet.clear();

		Connection con = db.getConnection();
		String sql = "SELECT * FROM RPS_MEMBER_SELECTED";

		try (Statement selectMain = con.createStatement();
			 ResultSet entries = selectMain.executeQuery(sql)) {
			while(entries.next()) {
				selectedMemberSet.add(new RPSMember(entries.getString("MEMBER_NAME")));
			}
		}
	}

	/**
	 * load all configuration from database
	 * @throws SQLException
	 */
	private void loadConfiguration() throws SQLException {

		Connection con = db.getConnection();
		String sql = "SELECT CONFIG_KEY, CONFIG_VALUE FROM APP_CONFIGURATION";

		Map<String, String> configMap = new HashMap<>();

		try (Statement selectMain = con.createStatement();
			 ResultSet entries = selectMain.executeQuery(sql)) {

			while(entries.next()) {
				configMap.put(entries.getString("CONFIG_KEY"), entries.getString("CONFIG_VALUE"));
			}
		}

		parseQuickLoadConfiguration(configMap);
	}

	/**
	 * parse the given quickload configuration to Object
	 * @param configMap
	 */
	private void parseQuickLoadConfiguration(Map<String, String> configMap) {

		if(configMap.containsKey("QL_MAIL") &&
				configMap.containsKey("QL_OFFSET") &&
				configMap.containsKey("QL_DAYS") &&
				configMap.containsKey("QL_DEPARTMENTS")) {
			super.qlConfig = new QuickLoadConfiguration();
			super.qlConfig.mail = configMap.get("QL_MAIL");
			super.qlConfig.offset = Integer.parseInt(configMap.get("QL_OFFSET"));
			super.qlConfig.days = Integer.parseInt(configMap.get("QL_DAYS"));

			String deptStr = configMap.get("QL_DEPARTMENTS");
			final Set<RPSDepartment> deptList = new HashSet<>();

			Arrays.asList(deptStr.split(System.lineSeparator())).forEach((t) -> {
				String[] dArr = t.split(":");
				if(dArr.length == 2) {
					deptList.add(new RPSDepartment(Integer.parseInt(dArr[0]), dArr[1]));
				}
			});

			super.qlConfig.departments = deptList;
		}
	}

	/**
	 * load member to member list from db
	 * @throws SQLException
	 */
	private void loadM2M() throws SQLException {
		memberToMemberSet.clear();

		Connection con = db.getConnection();
		String sql = "SELECT * FROM RPS_MEMBER_TO_MEMBER";

		try (Statement selectMain = con.createStatement();
			 ResultSet entries = selectMain.executeQuery(sql)) {
			while(entries.next()) {
				memberToMemberSet.add(new RPSMember(entries.getString("MEMBER_NAME")));
			}
		}
	}
    
    /*
    * clear storage methods
    */

	/**
	 * remove all rps entries and rps members from db
	 * @throws SQLException
	 */
	private void clearRPSEntriesOnStorage() throws SQLException {

		Connection con = db.getConnection();
		try (Statement stmt = con.createStatement()) {
			stmt.execute("DELETE FROM RPS_ENTRY");
			stmt.execute("DELETE FROM RPS_ENTRY_MEMBERS");
//	    stmt.execute("VACUUM");
			stmt.close();
		}
	}

	/**
	 * remove all rps departments from db
	 * @throws SQLException
	 */
	private void clearRPSDepartmentsOnStorage() throws SQLException {

		Connection con = db.getConnection();
		try(Statement stmt = con.createStatement()) {
			stmt.execute("DELETE FROM RPS_ENTRY_DEPARTMENT");
//	    stmt.execute("VACUUM");
			stmt.close();
		}
	}

	/**
	 * remove all selected members from database
	 * @throws SQLException
	 */
	private void clearSelectedMembersOnStorage() throws SQLException {

		Connection con = db.getConnection();
		try(Statement stmt = con.createStatement()) {
			stmt.execute("DELETE FROM RPS_MEMBER_SELECTED");
//	    stmt.execute("VACUUM");
			stmt.close();
		}
	}

	/**
	 * remove all member to member from db
	 * @throws SQLException
	 */
	private void clearM2MOnStorage() throws SQLException {

		Connection con = db.getConnection();
		try(Statement stmt = con.createStatement()) {
			stmt.execute("DELETE FROM RPS_MEMBER_TO_MEMBER");
//	    stmt.execute("VACUUM");
			stmt.close();
		}
	}
    
    /*
    * write to storage methods
    */

	/**
	 * write all rps entries from memory to database
	 * @throws at.foorada.rk.rps.data.RPSEntry.RPSEntryException
	 * @throws SQLException
	 */
	private void writeRPSEntriesToStorage() throws RPSEntry.RPSEntryException, SQLException {

		clearRPSEntriesOnStorage();
		int cnt = 0;
		for(RPSEntry entry:rpsEntrySet) {
			try {
				writeRPSEntryToStorage(entry);
				cnt++;
			} catch (RPSEntry.RPSEntryException ree) {
				LOGGER.log(Level.SEVERE, "unable to store rpsEntry to db: {0}", entry.toString());
			}
		}
		LOGGER.log(Level.INFO, "{0} entries stored to db", cnt);
	}

	/**
	 * write a single rps entry to database (inclusive rps members)
	 * @param entry
	 * @throws at.foorada.rk.rps.data.RPSEntry.RPSEntryException
	 * @throws SQLException
	 */
	private void writeRPSEntryToStorage(RPSEntry entry) throws RPSEntry.RPSEntryException, SQLException {

		Connection con = db.getConnection();

		//main entry
		String sqlEntry = "INSERT INTO RPS_ENTRY (DEPARTMENT_ID, TYPE, COMMENT, START, END, ANKER, FUTURE_ENTRY, DELETED, ADDED_TIME) " +
				"VALUES (?,?,?,?,?,?,?,?,?);";
		String sqlMember = "INSERT INTO RPS_ENTRY_MEMBERS (ENTRY_ID, ENTRY_PLACE, MEMBER_NAME) " +
				"VALUES (?, ?, ?)";

		try (PreparedStatement entryStmt = con.prepareStatement(sqlEntry);
			 PreparedStatement memberStmt = con.prepareStatement(sqlMember)) {

			int id = -1;
			entryStmt.setInt(1, entry.getDepartment().getId());
			entryStmt.setString(2, entry.getType().getDescription());
			entryStmt.setString(3, entry.getComment());
			entryStmt.setString(4, TIMESTAMP.format(entry.getStart().getTime()));
			entryStmt.setString(5, TIMESTAMP.format(entry.getEnd().getTime()));
			entryStmt.setString(6, entry.getAnker());
			entryStmt.setInt(7, entry.isFutureEntry() ? 1 : 0);
			entryStmt.setInt(8, entry.isDeleted() ? 1 : 0);
			entryStmt.setLong(9, entry.getAddedTime().getTimeInMillis());
			entryStmt.executeUpdate();

			ResultSet keys = entryStmt.getGeneratedKeys();
			if(keys.next())
				id = keys.getInt(1);

			if(id > 0) {

				memberStmt.setLong(1, id);
				int i = 1;
				for(RPSMember mem: entry.getMemberList()) {
					memberStmt.setInt(2, i++);
					memberStmt.setString(3, mem.getFullName());
					memberStmt.executeUpdate();
				}
			}
		}
	}

	/**
	 * write rps departments from memory to storage
	 * @throws SQLException
	 */
	private void writeRPSDepartmentsToStorage() throws SQLException{

		Connection con = db.getConnection();
		String sql = "INSERT INTO RPS_ENTRY_DEPARTMENT (DEPARTMENT_ID, DEPARTMENT_NAME, ADDED_TIME) " + "" +
				" VALUES (?,?,?);";

		try(PreparedStatement entryStmt = con.prepareStatement(sql)) {

			clearRPSDepartmentsOnStorage();

			for(RPSDepartment d:rpsDepartmentSet) {
				entryStmt.setInt(1, d.getId());
				entryStmt.setString(2, d.getDescription());
				entryStmt.setLong(3, d.getAddedTime().getTimeInMillis());
				entryStmt.executeUpdate();
			}
		}
	}

	/**
	 * write selected member list from memory to databse
	 * @throws SQLException
	 */
	private void writeSelectedMembersToStorage() throws SQLException {

		Connection con = db.getConnection();
		String sql = "INSERT INTO RPS_MEMBER_SELECTED (MEMBER_NAME) " +
				"VALUES (?)";

		try (PreparedStatement memberStmt = con.prepareStatement(sql)) {

			clearSelectedMembersOnStorage();

			for(RPSMember str: selectedMemberSet) {
				memberStmt.setString(1, str.getFullName());
				memberStmt.executeUpdate();
			}
		}
	}

	/**
	 * write member to member list from memory to storage
	 * @throws SQLException
	 */
	private void writeM2MToStorage() throws SQLException {

		Connection con = db.getConnection();
		String sql = "INSERT INTO RPS_MEMBER_TO_MEMBER (MEMBER_NAME) " +
				"VALUES (?)";

		try (PreparedStatement memberStmt = con.prepareStatement(sql)) {

			clearM2MOnStorage();

			for(RPSMember str: memberToMemberSet) {
				memberStmt.setString(1, str.getFullName());
				memberStmt.executeUpdate();
			}

		}
	}

	/**
	 * write quick load configuration object from memory to database
	 * @throws SQLException
	 */
	private void writeQuickLoadConfigurationToStorage() throws SQLException {

		Connection con = db.getConnection();
		String sql = "REPLACE INTO APP_CONFIGURATION (CONFIG_KEY, CONFIG_VALUE) " +
				"VALUES (?, ?)";

		try (PreparedStatement memberStmt = con.prepareStatement(sql)) {

			memberStmt.setString(1, "QL_MAIL");
			memberStmt.setString(2, super.qlConfig.mail);
			memberStmt.executeUpdate();

			memberStmt.setString(1, "QL_OFFSET");
			memberStmt.setString(2, Integer.toString(super.qlConfig.offset));
			memberStmt.executeUpdate();
			memberStmt.executeUpdate();

			memberStmt.setString(1, "QL_DAYS");
			memberStmt.setString(2, Integer.toString(super.qlConfig.days));
			memberStmt.executeUpdate();

			memberStmt.setString(1, "QL_DEPARTMENTS");
			String depStr = "";
			depStr = super.qlConfig.departments.stream().map((dept) -> dept.getId() + ":" + dept.getDescription() + System.lineSeparator()).reduce(depStr, String::concat);
			memberStmt.setString(2, depStr);
			memberStmt.executeUpdate();
		}
	}

	@Override
	public String getStorageInfo() {
		return db.getHostNameInfo();
	}



//<editor-fold defaultstate="collapsed" desc="unused">
    /*
    private List<RPSEntry> parseRPSEntryFromDB(ResultSet entries) throws SQLException, ParseException {
    List<RPSEntry> entryList = new ArrayList<>();
    
    PreparedStatement memberSql = db.getConnection().prepareStatement("SELECT * FROM RPS_ENTRY_MEMBERS " +
    " WHERE ENTRY_ID=? ORDER BY ENTRY_PLACE");
    
    while(entries.next()) {
    try {
    RPSEntry newEntry = new RPSEntry();
    newEntry.setId(entries.getLong("ID"));
    newEntry.setDepartment(getRPSDepartmentFromID(entries.getInt("DEPARTMENT_ID")));
    newEntry.setType(RPSEntry.Type.getFromString(entries.getString("TYPE")));
    newEntry.setComment(entries.getString("COMMENT"));
    Calendar start = Calendar.getInstance();
    start.setTime(TIMESTAMP.parse(entries.getString("START")));
    newEntry.setStart(start);
    Calendar end = Calendar.getInstance();
    end.setTime(TIMESTAMP.parse(entries.getString("END")));
    newEntry.setEnd(end);
    newEntry.setAnker(entries.getString("ANKER"));
    newEntry.setDeleted(entries.getInt("DELETED") == 1);
    Calendar added = Calendar.getInstance();
    added.setTimeInMillis(entries.getLong("ADDED_TIME"));
    newEntry.setAddedTime(added);
    
    memberSql.setLong(1, newEntry.getId());
    ResultSet memberSet = memberSql.executeQuery();
    
    while(memberSet.next()) {
    newEntry.addMember(memberSet.getString("MEMBER_PREFIX"), memberSet.getString("MEMBER_NAME"));
    }
    memberSet.close();
    
    entryList.add(newEntry);
    } catch(RPSEntry.RPSEntryException e) {
    
    }
    }
    
    return entryList;
    }
    */
//</editor-fold>

}