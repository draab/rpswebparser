/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.storage.main.dataAccess;

import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import static at.foorada.rk.rps.data.RPSEntry.TIMESTAMP;
import at.foorada.rk.rps.data.RPSMember;
import at.foorada.rk.rps.data.storage.db.DataBase;
import at.foorada.rk.rps.data.QuickLoadConfiguration;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author danie
 */

@Deprecated
public class DataAccessDatabaseDirect implements DataAccessInterface {

    private static final Logger LOGGER = Logger.getLogger(DataAccessDatabaseDirect.class.getName());

    private DataBase db;

    public DataAccessDatabaseDirect(DataBase db) throws IOException {
        this.db = db;

        configuartionInit();
        rpsEntryInit();
        rpsMemberInit();
        rpsDepartmentInit();
    }

    @Override
    public void close() throws IOException {
        if(db != null)
            db.close();
    }

    private void configuartionInit() throws IOException {

        String sql = "";
        try {
            Statement stmt = db.getConnection().createStatement();
            sql = "CREATE TABLE IF NOT EXISTS APP_CONFIGURATION " +
                    "(CONFIG_ID  INT  PRIMARY KEY  NOT NULL, " +
                    " CONFIG_KEY  TEXT  NOT NULL, " +
                    " CONFIG_VALUE  TEXT  NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            LOGGER.finest("Configuration Table created successfully");

        } catch(SQLException sqle) {
            LOGGER.log(Level.SEVERE, "not able to create table. statement: {0}", sql);
            throw new IOException(sqle);
        }
    }

    private void rpsEntryInit() throws IOException {

        String sql = "";
        try {
            Statement stmt = db.getConnection().createStatement();
            sql = "CREATE TABLE IF NOT EXISTS RPS_ENTRY " +
                    "(ID  INTEGER  PRIMARY KEY  NOT NULL," +
                    " DEPARTMENT_ID  INTEGER  NOT NULL, " +
                    " TYPE  TEXT  NOT NULL, " +
                    " COMMENT  TEXT  NOT NULL, " +
                    " START  TEXT  NOT NULL, " +
                    " END  TEXT  NOT NULL, " +
                    " ANKER  TEXT, " +
                    " FUTURE_ENTRY  INTEGER, " +
                    " DELETED INTEGER, " +
                    " ADDED_TIME INTEGER)";
            stmt.executeUpdate(sql);
            sql = "CREATE TABLE IF NOT EXISTS RPS_ENTRY_MEMBERS " +
                    "(ENTRY_ID  INTEGER  NOT NULL, " +
                    " ENTRY_PLACE  INTEGER  NOT NULL, " +
                    " MEMBER_PREFIX  TEXT  NOT NULL, " +
                    " MEMBER_NAME  TEXT  NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            LOGGER.finest("RPSEntry Table created successfully");

        } catch(SQLException sqle) {
            LOGGER.log(Level.SEVERE, "not able to create table. statement: {0}", sql);
            throw new IOException(sqle);
        }
    }

    private void rpsMemberInit() throws IOException {

        String sql = "";
        try {
            Statement stmt = db.getConnection().createStatement();
            sql = "CREATE TABLE IF NOT EXISTS RPS_MEMBER_SELECTED " +
                    "(MEMBER_NAME  TEXT  NOT NULL)";
            stmt.executeUpdate(sql);
            sql = "CREATE TABLE IF NOT EXISTS RPS_MEMBER_TO_MEMBER " +
                    "(MEMBER_NAME  TEXT  NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            LOGGER.finest("RPSMember Table created successfully");

        } catch(SQLException sqle) {
            LOGGER.log(Level.SEVERE, "not able to create table. statement: {0}", sql);
            throw new IOException(sqle);
        }
    }

    private void rpsDepartmentInit() throws IOException {
        String sql = "";
        try {
            Statement stmt = db.getConnection().createStatement();
            sql = "CREATE TABLE IF NOT EXISTS RPS_ENTRY_DEPARTMENT " +
                    "(DEPARTMENT_ID  INTEGER PRIMARY KEY NOT NULL, " +
                    " DEPARTMENT_NAME  TEXT  NOT NULL, " +
                    " ADDED_TIME INTEGER NOT NULL)";
            stmt.executeUpdate(sql);
            stmt.close();
            LOGGER.finest("RPSDepartment Table created successfully");

        } catch(SQLException sqle) {
            LOGGER.log(Level.SEVERE, "not able to create table. statement: {0}", sql);
            throw new IOException(sqle);
        }
    }

    @Override
    public Set<RPSEntry> getRPSEntrySet() throws IOException {

        Set<RPSEntry> entryList = new HashSet<>();
        Connection con = db.getConnection();
        try {

            String sql = "SELECT * FROM RPS_ENTRY WHERE " +
                    " DELETED = 0";

            Statement selectMain = con.createStatement();

            ResultSet entries = selectMain.executeQuery(sql);

            entryList.addAll(parseRPSEntryFromDB(entries));

            entries.close();

        }catch(SQLException | ParseException sqle) {

            throw new IOException("Exception in loadAllRPSEntroes", sqle);
        }

        return entryList;
    }

    @Override
    public Set<RPSMember> getRPSMemberSet() throws IOException {

        Set<RPSMember> member = new HashSet<>();
        Connection con = db.getConnection();

        try {

            String sql = "SELECT DISTINCT MEMBER_NAME FROM RPS_ENTRY_MEMBERS";

            Statement memberStat = con.createStatement();
            ResultSet memberResult = memberStat.executeQuery(sql);

            while(memberResult.next()) {
                member.add(new RPSMember(memberResult.getString("MEMBER_NAME")));
            }

        }catch(SQLException sqle) {
            throw new IOException(sqle);
        }

        return member;
    }

    //<editor-fold defaultstate="collapsed" desc="additional code; maybe in future needed again">
    /*
    @Override
    public List<RPSEntry> getRPSEntriesByStartDate(Calendar date, int days) throws IOException, ParseException {
	
        List<RPSEntry> entryList = new ArrayList<>();
        Connection con = db.getConnection();
        try {

            String sql = "SELECT * FROM RPS_ENTRY WHERE " +
                    "START > ? AND START < ? AND DELETED = 0";

            Calendar start = (Calendar)date.clone();
            start.set(Calendar.HOUR, 0);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);

            PreparedStatement selectMain = con.prepareStatement(sql);
            selectMain.setString(1, TIMESTAMP.format(start.getTime()));

            Calendar end = (Calendar) start.clone();
            end.add(Calendar.DAY_OF_MONTH, days-1);
            end.set(Calendar.HOUR, 23);
            end.set(Calendar.MINUTE, 59);
            end.set(Calendar.SECOND, 59);
            selectMain.setString(2, TIMESTAMP.format(end.getTime()));

            ResultSet entries = selectMain.executeQuery();

            entryList = parseRPSEntryFromDB(entries);

            entries.close();

        }catch(SQLException | ParseException sqle) {
            sqle.printStackTrace();
            throw new IOException(sqle);
        }
        return entryList;
    }

    @Override
    public List<RPSEntry> getEntriesByDateDepartment(Calendar date, RPSDepartment department) throws IOException, ParseException {

        List<RPSEntry> entryList = new ArrayList<>();
        Connection con = db.getConnection();
        try {

            String sql = "SELECT * FROM RPS_ENTRY WHERE " +
                    "START > ? AND START < ? AND DEPARTMENT_ID = ? AND DELETED = 0";

            Calendar start = (Calendar)date.clone();
            start.set(Calendar.HOUR, 0);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);

            PreparedStatement selectMain = con.prepareStatement(sql);
            selectMain.setString(1, TIMESTAMP.format(start.getTime()));
            start.set(Calendar.HOUR, 23);
            start.set(Calendar.MINUTE, 59);
            start.set(Calendar.SECOND, 59);
            selectMain.setString(2, TIMESTAMP.format(start.getTime()));

            selectMain.setInt(3, department.getId());

            ResultSet entries = selectMain.executeQuery();

            entryList = parseRPSEntryFromDB(entries);

            entries.close();

        }catch(SQLException | ParseException sqle) {
            sqle.printStackTrace();
            throw new IOException(sqle);
        }
        return entryList;
    }

    @Override
    public List<RPSEntry> getEntriesByDepartment(RPSDepartment department) throws IOException, ParseException {

        List<RPSEntry> entryList = new ArrayList<>();
        Connection con = db.getConnection();
        try {

            String sql = "SELECT * FROM RPS_ENTRY WHERE " +
                    " DEPARTMENT_ID = ? AND DELETED = 0";

            PreparedStatement selectMain = con.prepareStatement(sql);

            selectMain.setInt(1, department.getId());

            ResultSet entries = selectMain.executeQuery();

            entryList = parseRPSEntryFromDB(entries);

            entries.close();

        }catch(SQLException | ParseException sqle) {
            sqle.printStackTrace();
            throw new IOException(sqle);
        }
        return entryList;
    }

    */
    //</editor-fold>

    @Override
    public void removeEntriesByTypeDepartment(Calendar date, int days, RPSEntry.Type type, RPSDepartment department) throws IOException {

        Connection con = db.getConnection();
        try {
            int parameterIdx = 0;
            String sql = "DELETE FROM RPS_ENTRY WHERE " +
                    " START > ? AND START < ?";
            parameterIdx = 2;

            if(type != null && type != RPSEntry.Type.ALL_TYPES) {
                sql += " AND TYPE=?";
                parameterIdx++;
            }

            if(department != null &&
                    department != RPSDepartment.UNKNOWN &&
                    department != RPSDepartment.ALL) {
                sql += " AND DEPARTMENT_ID=?";
                parameterIdx++;
            }

            Calendar start = (Calendar)date.clone();
            start.set(Calendar.HOUR, 0);
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.SECOND, 0);

            PreparedStatement removeStatement = con.prepareStatement(sql);
            removeStatement.setString(1, TIMESTAMP.format(start.getTime()));

            Calendar end = (Calendar) start.clone();
            end.add(Calendar.DAY_OF_MONTH, days-1);
            end.set(Calendar.HOUR, 23);
            end.set(Calendar.MINUTE, 59);
            end.set(Calendar.SECOND, 59);
            removeStatement.setString(2, TIMESTAMP.format(end.getTime()));

            if(department != null)
                removeStatement.setInt(parameterIdx--, department.getId());
            if(type != null)
                removeStatement.setString(parameterIdx, type.getDescription());

            removeStatement.execute();
            removeStatement.close();

        }catch(SQLException sqle) {
            throw new IOException(sqle);
        }

    }

//    @Override
//    public void deleteEntriesByTypeDepartment(Calendar date, int days, RPSEntry.Type type, RPSDepartment department) throws IOException {
//        Connection con = db.getConnection();
//        try {
//            int parameterIdx = 0;
//            String sql = "UPDATE RPS_ENTRY SET " +
//                    " DELETED=1 WHERE " +
//                    " START > ? AND START < ?";
//            parameterIdx = 2;
//
//            if(type != null && type != RPSEntry.Type.ALL_TYPES) {
//                sql += " AND TYPE=?";
//                parameterIdx++;
//            }
//
//            if(department != null &&
//                    department != RPSDepartment.UNKNOWN &&
//                    department != RPSDepartment.ALL) {
//                sql += " AND DEPARTMENT_ID=?";
//                parameterIdx++;
//            }
//
//            Calendar start = (Calendar)date.clone();
//            start.set(Calendar.HOUR, 0);
//            start.set(Calendar.MINUTE, 0);
//            start.set(Calendar.SECOND, 0);
//
//            PreparedStatement removeStatement = con.prepareStatement(sql);
//            removeStatement.setString(1, TIMESTAMP.format(start.getTime()));
//
//            Calendar end = (Calendar) start.clone();
//            end.add(Calendar.DAY_OF_MONTH, days-1);
//            end.set(Calendar.HOUR, 23);
//            end.set(Calendar.MINUTE, 59);
//            end.set(Calendar.SECOND, 59);
//            removeStatement.setString(2, TIMESTAMP.format(end.getTime()));
//
//            if(department != null)
//                removeStatement.setInt(parameterIdx--, department.getId());
//            if(type != null)
//                removeStatement.setString(parameterIdx, type.getDescription());
//
//            removeStatement.execute();
//            removeStatement.close();
//
//        }catch(SQLException sqle) {
//            sqle.printStackTrace();
//            throw new IOException(sqle);
//        }
//    }


    @Override
    public void addRPSEntry(RPSEntry entry) throws IOException {

        Connection con = db.getConnection();
        String sql = "";

        try {
            boolean prevAutoCommit = con.getAutoCommit();
            con.setAutoCommit(false);

            //main entry
            sql = "INSERT INTO RPS_ENTRY (DEPARTMENT_ID, TYPE, COMMENT, START, END, ANKER, FUTURE_ENTRY, DELETED, ADDED_TIME) " +
                    "VALUES (?,?,?,?,?,?,?,?,?);";
            int id = -1;
            PreparedStatement entryStmt = con.prepareStatement(sql);
            entryStmt.setInt(1, entry.getDepartment().getId());
            entryStmt.setString(2, entry.getType().getDescription());
            entryStmt.setString(3, entry.getComment());
            entryStmt.setString(4, TIMESTAMP.format(entry.getStart().getTime()));
            entryStmt.setString(5, TIMESTAMP.format(entry.getEnd().getTime()));
            entryStmt.setString(6, entry.getAnker());
            entryStmt.setInt(7, entry.isFutureEntry() ? 1 : 0);
            entryStmt.setInt(8, entry.isDeleted() ? 1 : 0);
            entryStmt.setLong(9, entry.getAddedTime().getTimeInMillis());
            entryStmt.executeUpdate();

            ResultSet keys = entryStmt.getGeneratedKeys();
            if(keys.next())
                id = keys.getInt(1);

            entryStmt.close();

            if(id > 0) {

                sql = "INSERT INTO RPS_ENTRY_MEMBERS (ENTRY_ID, ENTRY_PLACE, MEMBER_PREFIX, MEMBER_NAME) " +
                        "VALUES (?, ?, ?, ?)";
                PreparedStatement memberStmt = con.prepareStatement(sql);
                memberStmt.setLong(1, id);
                int i = 1;
                for(RPSMember str: entry.getMemberList()) {
                    memberStmt.setInt(2, i++);
                    memberStmt.setString(3, str.prefix);
                    memberStmt.setString(4, str.name);
                    memberStmt.executeUpdate();
                }
                memberStmt.close();

            } else {
                throw new RPSEntry.RPSEntryException("error while inserting rps entry to db");
            }

            con.commit();
            con.setAutoCommit(prevAutoCommit);
        } catch ( SQLException sqle ) {
            LOGGER.log(Level.SEVERE, "not able to insert rps entry to db. Statement:{0}", sql);
            LOGGER.log(Level.INFO, "entry: {0}", this.toString());
            if(con != null) {
                try {
                    con.rollback();
                } catch(SQLException npe){}
            }
            throw new IOException("not able to insert rps entry to db", sqle);
        } catch (RPSEntry.RPSEntryException e) {
            try {
                con.rollback();
            } catch(SQLException|NullPointerException npe){}
            throw new IOException(e);
        }
    }

    @Override
    public void addRPSEntryList(Collection<RPSEntry> list) {

        int cnt = 0;
        for(RPSEntry entry:list) {
            try {
                addRPSEntry(entry);
                cnt++;
            } catch (IOException ioe) {
                LOGGER.log(Level.WARNING, "unable to store rpsEntry to db:\n{0}", entry.toString());
            }
        }
        LOGGER.log(Level.INFO, "{0}entries stored to db", cnt);
    }

    @Override
    public void addRPSDepartment(RPSDepartment department) throws IOException {
        setRPSDepartmentList(Arrays.asList(department));
    }

    @Override
    public void setRPSDepartmentList(Collection<RPSDepartment> list) throws IOException {

        Connection con = db.getConnection();
        String sql;

        try {
            sql = "INSERT INTO RPS_ENTRY_DEPARTMENT (DEPARTMENT_ID, DEPARTMENT_NAME, ADDED_TIME) " + "" +
                    " VALUES (?,?,?);";
            PreparedStatement entryStmt = con.prepareStatement(sql);

            for(RPSDepartment d:list) {
                try {
                    entryStmt.setInt(1, d.getId());
                    entryStmt.setString(2, d.getDescription());
                    entryStmt.setLong(3, d.getAddedTime().getTimeInMillis());
                    entryStmt.executeUpdate();
                } catch(SQLException innerSQL) {
                    //most probably already in db
                    LOGGER.log(Level.FINE, "could not insert department ({0}) entry", d.getDescription());
                }
            }
            entryStmt.close();
        } catch(SQLException sqle) {
            LOGGER.log(Level.SEVERE, "could not prepare statement for insert department");
            throw new IOException(sqle);        }
    }

    @Override
    public Set<RPSDepartment> getRPSDepartmentSet() {

        Set<RPSDepartment> newDepartmentSet = new HashSet<>();
        Connection con = db.getConnection();
        try {

            String sql = "SELECT * FROM RPS_ENTRY_DEPARTMENT";

            Statement selectMain = con.createStatement();
            ResultSet departments = selectMain.executeQuery(sql);

            while(departments.next()) {
                RPSDepartment department = new RPSDepartment(departments.getInt("DEPARTMENT_ID"), departments.getString("DEPARTMENT_NAME"));
                Calendar added = Calendar.getInstance();
                added.setTimeInMillis(departments.getLong("ADDED_TIME"));
                department.setAddedTime(added);

                newDepartmentSet.add(department);
            }
            departments.close();

        } catch (SQLException ex) {
            Logger.getLogger(DataAccessDatabaseDirect.class.getName()).log(Level.SEVERE, null, ex);
        }

        return newDepartmentSet;
    }

    private List<RPSEntry> parseRPSEntryFromDB(ResultSet entries) throws SQLException, ParseException {
        List<RPSEntry> entryList = new ArrayList<>();

        PreparedStatement memberSql = db.getConnection().prepareStatement("SELECT * FROM RPS_ENTRY_MEMBERS " +
                " WHERE ENTRY_ID=? ORDER BY ENTRY_PLACE");

        while(entries.next()) {
            try {
                int id = entries.getInt("ID");
                RPSEntry newEntry = new RPSEntry();
                newEntry.setDepartment(DataAccessInterface.filterRPSDepartmentFromID(getRPSDepartmentSet(), entries.getInt("DEPARTMENT_ID")));
                newEntry.setType(RPSEntry.Type.getFromString(entries.getString("TYPE")));
                newEntry.setComment(entries.getString("COMMENT"));
                Calendar start = Calendar.getInstance();
                start.setTime(TIMESTAMP.parse(entries.getString("START")));
                newEntry.setStart(start);
                Calendar end = Calendar.getInstance();
                end.setTime(TIMESTAMP.parse(entries.getString("END")));
                newEntry.setEnd(end);
                newEntry.setAnker(entries.getString("ANKER"));
                newEntry.setDeleted(entries.getInt("DELETED") == 1);
                Calendar added = Calendar.getInstance();
                added.setTimeInMillis(entries.getLong("ADDED_TIME"));
                newEntry.setAddedTime(added);

                memberSql.setLong(1, id);
                ResultSet memberSet = memberSql.executeQuery();

                while (memberSet.next()) {
                    newEntry.addMember(memberSet.getString("MEMBER_PREFIX"), memberSet.getString("MEMBER_NAME"));
                }
                memberSet.close();

                entryList.add(newEntry);
            } catch (RPSEntry.RPSEntryException e) {

            }
        }

        return entryList;
    }

    @Override
    public Set<RPSMember> getSelectedMemberSet() throws IOException {
        Set<RPSMember> memberSet = new HashSet<>();
        Connection con = db.getConnection();
        try {

            String sql = "SELECT * FROM RPS_MEMBER_SELECTED";

            Statement selectMain = con.createStatement();
            ResultSet entries = selectMain.executeQuery(sql);

            while(entries.next()) {
                memberSet.add(new RPSMember(entries.getString("MEMBER_NAME")));
            }
            entries.close();

        }catch(SQLException sqle) {
            throw new IOException(sqle);
        }

        return memberSet;
    }

    @Override
    public void setSelectedMemberList(Collection<RPSMember> memberList) throws IOException {

        if(memberList == null || db == null)
            return;

        Connection con = db.getConnection();
        String sql = "";
        boolean prevAutoCommit = true;

        try {
            prevAutoCommit = con.getAutoCommit();
            con.setAutoCommit(false);
            sql = "DELETE FROM RPS_MEMBER_SELECTED";
            Statement delete = con.createStatement();
            delete.execute(sql);
            delete.close();

            sql = "INSERT INTO RPS_MEMBER_SELECTED (MEMBER_NAME) " +
                    "VALUES (?)";
            PreparedStatement memberStmt = con.prepareStatement(sql);
            for(RPSMember str: memberList) {
                memberStmt.setString(1, str.getFullName());
                memberStmt.executeUpdate();
            }
            memberStmt.close();

            con.commit();
            con.setAutoCommit(prevAutoCommit);

        } catch ( SQLException sqle ) {
            throw new IOException("not able to insert rps entry to db. Statement:" + sql, sqle);
        }
        try {
            con.setAutoCommit(prevAutoCommit);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "error in setting auto commit back.");
        }
    }

    @Override
    public Set<RPSMember> getM2MSet() throws IOException {
        Set<RPSMember> memberSet = new HashSet<>();
        Connection con = db.getConnection();
        try {

            String sql = "SELECT * FROM RPS_MEMBER_TO_MEMBER";

            Statement selectMain = con.createStatement();
            ResultSet entries = selectMain.executeQuery(sql);

            while(entries.next()) {
                memberSet.add(new RPSMember(entries.getString("MEMBER_NAME")));
            }
            entries.close();

        }catch(SQLException sqle) {
            throw new IOException(sqle);
        }

        return memberSet;
    }

    @Override
    public void setM2MList(Collection<RPSMember> memberList) throws IOException {

        if(memberList == null || db == null)
            return;

        Connection con = db.getConnection();
        String sql = "";
        boolean prevAutoCommit = true;

        try {
            prevAutoCommit = con.getAutoCommit();
            con.setAutoCommit(false);
            sql = "DELETE FROM RPS_MEMBER_TO_MEMBER";
            Statement delete = con.createStatement();
            delete.execute(sql);
            delete.close();

            sql = "INSERT INTO RPS_MEMBER_TO_MEMBER (MEMBER_NAME) " +
                    "VALUES (?)";
            PreparedStatement memberStmt = con.prepareStatement(sql);
            for(RPSMember str: memberList) {
                memberStmt.setString(1, str.getFullName());
                memberStmt.executeUpdate();
            }
            memberStmt.close();

            con.commit();
            con.setAutoCommit(prevAutoCommit);

        } catch ( SQLException sqle ) {
            throw new IOException(sqle);
        }
        try {
            con.setAutoCommit(prevAutoCommit);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "error in setting auto commit back.");
        }
    }

    @Override
    public void save() {}




    @Override
    public void addSelectedMember(RPSMember member) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addM2M(RPSMember member) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getStorageInfo() {
        return db.getHostNameInfo();
    }

    @Override
    public void setQuickLoadConfiguration(QuickLoadConfiguration qlConfig) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public QuickLoadConfiguration getQuickLoadConfiguration() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeSelectedMember(RPSMember member) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void removeM2M(RPSMember member) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Set<String> getAnkerSet() throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addSelectedMemberList(Collection<RPSMember> memberList) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addM2MList(Collection<RPSMember> memberList) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}