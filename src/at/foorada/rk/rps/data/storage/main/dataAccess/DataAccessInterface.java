/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.storage.main.dataAccess;

import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.data.RPSMember;
import at.foorada.rk.rps.data.QuickLoadConfiguration;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * interface for accessing stored data
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public interface DataAccessInterface {
    
    /**
     * get information about storage for displaying in main message label
     * @return
     */
    public String getStorageInfo();

    /**
     * @return a set of all known RPSMember
     * @throws IOException 
     */
    public Set<RPSMember> getRPSMemberSet() throws IOException;
    
    public Set<String> getAnkerSet() throws IOException;
    
    //<editor-fold defaultstate="collapsed" desc="methods for managing entries">
    /**
     * @return returns a set of all RPSEnties 
     * @throws IOException 
     */
    public Set<RPSEntry> getRPSEntrySet() throws IOException;
    
    /**
     * The method removes entries by start date, days count, type and
     * department permanently from storage.
     * @param date
     * @param days
     * @param type
     * @param department
     * @throws IOException 
     */
    public void removeEntriesByTypeDepartment(Calendar date, int days, RPSEntry.Type type, RPSDepartment department) throws IOException;
    
//    /**
//     * The method sets a delete flag for entries by start date, days count, type and
//     * department.
//     * @param date
//     * @param days
//     * @param type
//     * @param department
//     * @throws IOException 
//     */
//    public void deleteEntriesByTypeDepartment(Calendar date, int days, RPSEntry.Type type, RPSDepartment department) throws IOException;
    
    /**
     * add a rps entry to the storage
     * @param entry
     * @throws IOException 
     */
    public void addRPSEntry(RPSEntry entry) throws IOException;
    
    /**
     * add a collection of entris to storage
     * @param list 
     * @throws java.io.IOException 
     */
    public void addRPSEntryList(Collection<RPSEntry> list) throws IOException;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="methods for managing departments">
    /**
     * add a rps department to the storage
     * @param department 
     * @throws java.io.IOException 
     */
    public void addRPSDepartment(RPSDepartment department) throws IOException;
    
    /**
     * add a rps department collection to the storage
     * @param list 
     * @throws java.io.IOException 
     */
    public void setRPSDepartmentList(Collection<RPSDepartment> list) throws IOException;
    
    /**
     * get a set of rps departments from the storage
     * @return set of rps departments
     * @throws IOException 
     */
    public Set<RPSDepartment> getRPSDepartmentSet() throws IOException;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="methods for member overview module">
    /**
     * get a set of rps members for the member overview module
     * @return
     * @throws IOException 
     */
    public Set<RPSMember> getSelectedMemberSet() throws IOException;
    
    /**
     * add a rps member to member list for overview module
     * @param member 
     * @throws java.io.IOException 
     */
    public void addSelectedMember(RPSMember member) throws IOException;
    
    /**
     * add a rps member list to member list for overview module
     * @param memberList 
     * @throws java.io.IOException 
     */
    public void addSelectedMemberList(Collection<RPSMember> memberList) throws IOException;
    
    /**
     * remove the given rps member from the member list for the overview module
     * @param member
     * @throws IOException 
     */
    public void removeSelectedMember(RPSMember member) throws IOException;
    
    /**
     * delete old set of members for overview module and
     * adds members from given collection
     * @param memberList 
     * @throws java.io.IOException 
     */
    public void setSelectedMemberList(Collection<RPSMember> memberList) throws IOException;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="methods for member to member module">
    /**
     * returns a set of members for the member to member module
     * @return 
     * @throws java.io.IOException 
     */
    public Set<RPSMember> getM2MSet() throws IOException;
    
    /**
     * add the given member to the list for member to member modules
     * @param member 
     * @throws java.io.IOException 
     */
    public void addM2M(RPSMember member) throws IOException;
    
    /**
     * add the given member list to the list for member to member modules
     * @param memberList 
     * @throws java.io.IOException 
     */
    public void addM2MList(Collection<RPSMember> memberList) throws IOException;
    
    /**
     * remove given member from the list of the member to member module
     * @param member 
     * @throws java.io.IOException 
     */
    public void removeM2M(RPSMember member) throws IOException;
    
    /**
     * delete the set of members for the member to member module and
     * adds members from given collection
     * @param memberList 
     * @throws java.io.IOException 
     */
    public void setM2MList(Collection<RPSMember> memberList) throws IOException;
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="methods for quick load configuration">
    /**
     * replace the stored quick load configuration object with the given one
     * @param qlConfig 
     */
    public void setQuickLoadConfiguration(QuickLoadConfiguration qlConfig);
    
    /**
     * returns the current stored quick load configuration object
     * @return 
     */
    public QuickLoadConfiguration getQuickLoadConfiguration();
    //</editor-fold>
    
    /**
     * write all changes persistent to file. depends on subclass.
     * @throws IOException 
     */
    public void save() throws IOException;
    
    /**
     * close the storage, possibly not necessary; do not use object after calling this method
     * @throws java.io.IOException
     */
    public void close() throws IOException;
    
    
    //<editor-fold defaultstate="collapsed" desc="static method for interface">
    /**
     * filter given rps department list with given id
     * @param departmentList
     * @param id
     * @return 
     */
    public static RPSDepartment filterRPSDepartmentFromID(Collection<RPSDepartment> departmentList, int id) {
        
        return departmentList.stream().filter((d)-> d.getId()==id).findFirst().orElse(RPSDepartment.UNKNOWN);
    }
    
    /**
     * filter given entry collection by given department
     * @param entryList
     * @param department
     * @return 
     */
    public static List<RPSEntry> filterRPSEntryListByDepartment(Collection<RPSEntry> entryList, RPSDepartment department){
        
        List<RPSEntry> list = new ArrayList<>();
        
        entryList.stream().filter(
                (entry) -> (entry.getDepartment().equals(department))
        ).forEachOrdered((entry) -> {
            list.add(entry);
        });
        
        return list;
    }
    
    /**
     * filter given entry collection by given start date and days span.
     * @param entryList
     * @param date
     * @param days
     * @return list of entries in range of given parameters
     */
    public static List<RPSEntry> filterRPSEntryListByStartDate(Collection<RPSEntry> entryList, Calendar date, int days) {
        
        List<RPSEntry> list = new ArrayList<>();
        
        Calendar start = (Calendar)date.clone();
        start.set(Calendar.HOUR, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        
        Calendar end = (Calendar) start.clone();
        end.add(Calendar.DAY_OF_MONTH, days-1);
        end.set(Calendar.HOUR, 23);
        end.set(Calendar.MINUTE, 59);
        end.set(Calendar.SECOND, 59);
        
        entryList.stream().filter(
                (entry) -> (entry.getStart().compareTo(start) >= 0 && entry.getStart().compareTo(end) <= 0)
        ).forEachOrdered((entry) -> {
            list.add(entry);
        });
        
        return list;
    }
    
    /**
     * filter given collection by given date and department
     * @param entryList
     * @param date
     * @param department
     * @return list with entries from given entry list with matching date and
     * department
     */
    public static List<RPSEntry> filterRPSEntryListByDateDepartment(Collection<RPSEntry> entryList, Calendar date, RPSDepartment department) {
        
        List<RPSEntry> list = new ArrayList<>();
        
        Calendar start = (Calendar)date.clone();
        start.set(Calendar.HOUR, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        
        Calendar end = (Calendar) start.clone();
        end.set(Calendar.HOUR, 23);
        end.set(Calendar.MINUTE, 59);
        end.set(Calendar.SECOND, 59);
        
        entryList.stream().filter((entry) ->
                (entry.getStart().compareTo(start) >= 0 && entry.getStart().compareTo(end) <= 0 &&
                        (entry.getDepartment().equals(department) || entry.getDepartment().equals(RPSDepartment.ALL)))
        ).forEachOrdered((entry) -> {
            list.add(entry);
        });
        
        return list;
    }
//</editor-fold>
}
