/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.storage.main.dataAccess;

import at.foorada.rk.rps.data.json.JsonConverterToolbox;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author danie
 */
public class DataAccessJson extends DataAccessMemoryBuffer {
    
    private static final Logger LOGGER = Logger.getLogger(DataAccessJson.class.getName());
    
    private static final String CHARSET_NAME = "UTF-8";
    private static final Level DEBUG_LOGGING = Level.INFO;
    
    private final File file;

    public DataAccessJson(File jsonFile) throws IOException {
	LOGGER.log(DEBUG_LOGGING, "create new dataAccessJasn object: {0}", jsonFile.getAbsolutePath());
	this.file = jsonFile;
	
	loadMemory();
    }
    
    @Override
    public String getStorageInfo() {
		return (memoryHasChanged ? "* " : "") + file.getName();
    }

    @Override
    public void save() throws IOException {

	try (FileWriter writer = new FileWriter(file)) {
	    JSONObject root = new JSONObject();
	    
	    //collect data
	    root.put("config", configToJson());
	    root.put("rpsDepartmentList", JsonConverterToolbox.departmentListToJson(rpsDepartmentSet));
	    root.put("rpsEntryList", JsonConverterToolbox.entryListToJson(rpsEntrySet));
	    root.put("rpsSelectedList", JsonConverterToolbox.memberListToJson(selectedMemberSet));
	    root.put("rpsM2MList", JsonConverterToolbox.memberListToJson(memberToMemberSet));
	    
	    writer.write(root.toJSONString());
	    
	    memoryHasChanged = false;
	    //no flush or close need her (automatically because of try with resources)
	    LOGGER.info("memory successfully stored in json file.");
	}
    }
    
    
    /**
     * collects all configuration values and returns a json object containing the values
     * @return JSONObject containing all config values
     */
    private JSONObject configToJson() {
        
        JSONObject configJson = new JSONObject();
        
        configJson.put("qlConfig", JsonConverterToolbox.quickLoadConfigToJson(qlConfig));
        
        return configJson;
    }
    
    /**
     * if the json file contains information about: <br/>
     * * quick load module, the qlConfig object will be set in parent class <br/>
     * * ... thats all at the moment
     * @param jsonConfig contains the config part of the given file
     */
    private void laodConfigFromJson(JSONObject jsonConfig) {
        
        //check if json contains information for quick load configuration
        if(jsonConfig.containsKey("qlConfig")) {
            JSONObject o = (JSONObject) jsonConfig.get("qlConfig");
            super.qlConfig = JsonConverterToolbox.toQuickLoadConfig(o);
        }
    }
    
    /**
     * loading the content of the given file to the memory objects of the parent class
     * @exception IOException can occure while writing to file
     */
    private void loadMemory() throws IOException {
	
	try {
	    
	    //read file
	    JSONObject root = (JSONObject) new JSONParser().parse(new FileReader(file));
	    LOGGER.log(Level.FINE, "content of file successfully read");
	    
            //read config from json
            laodConfigFromJson((JSONObject) root.get("config"));
            
            
	    //read departments from json
	    JSONArray departments = (JSONArray) root.get("rpsDepartmentList");
	    super.rpsDepartmentSet.clear();
	    super.rpsDepartmentSet.addAll(JsonConverterToolbox.toDepartmentList(departments));
	    
	    //read entries from json
	    JSONArray entries = (JSONArray) root.get("rpsEntryList");
	    super.rpsEntrySet.clear();
	    super.rpsEntrySet.addAll(JsonConverterToolbox.toEntryList(entries));
	    
	    //read selected members from json
	    JSONArray selectedMembers = (JSONArray) root.get("rpsSelectedList");
	    super.selectedMemberSet.clear();
	    super.selectedMemberSet.addAll(JsonConverterToolbox.toMemberList(selectedMembers));
	    
	    //read member to member list from json
	    JSONArray m2m = (JSONArray) root.get("rpsM2MList");
	    super.memberToMemberSet.clear();
	    super.memberToMemberSet.addAll(JsonConverterToolbox.toMemberList(m2m));
	    
	    LOGGER.log(Level.FINE, "file successfully parsed");
	    
	} catch (ParseException ex) {
	    LOGGER.log(DEBUG_LOGGING, "new file", ex);
	
	    //new file
	    super.rpsEntrySet.clear();
	    super.rpsDepartmentSet.clear();
	    super.selectedMemberSet.clear();
	    super.memberToMemberSet.clear();
	}
    }
}