/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.storage.main.dataAccess;

import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.data.RPSMember;
import at.foorada.rk.rps.data.QuickLoadConfiguration;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

/**
 *
 * @author danie
 */
public abstract class DataAccessMemoryBuffer implements DataAccessInterface {

	private static final Logger LOGGER = Logger.getLogger(DataAccessMemoryBuffer.class.getName());

	protected Set<RPSEntry> rpsEntrySet;
	protected Set<RPSDepartment> rpsDepartmentSet;

	protected Set<RPSMember> selectedMemberSet;
	protected Set<RPSMember> memberToMemberSet;

	protected QuickLoadConfiguration qlConfig;

	protected boolean memoryHasChanged;

	public DataAccessMemoryBuffer() {
		this.rpsEntrySet = new HashSet<>();
		this.rpsDepartmentSet = new HashSet<>();
		this.selectedMemberSet = new HashSet<>();
		this.memberToMemberSet = new HashSet<>();

		this.qlConfig = new QuickLoadConfiguration();

		this.memoryHasChanged = false;
	}
	//<editor-fold defaultstate="collapsed" desc="quick load configuration functions">
	@Override
	public void setQuickLoadConfiguration(QuickLoadConfiguration qlConfig) {
		if(qlConfig != null)
			this.qlConfig = qlConfig;
		else
			this.qlConfig = new QuickLoadConfiguration();

		memoryHasChanged = true;
	}

	@Override
	public QuickLoadConfiguration getQuickLoadConfiguration() {
		return qlConfig;
	}
	//</editor-fold>


	//<editor-fold defaultstate="collapsed" desc="entry methods">

	@Override
	public Set<RPSEntry> getRPSEntrySet() throws IOException {
		return rpsEntrySet;
	}

	@Override
	public void removeEntriesByTypeDepartment(Calendar date, int days, RPSEntry.Type type, RPSDepartment department) throws IOException {

		Calendar start = (Calendar)date.clone();
		start.set(Calendar.HOUR, 0);
		start.set(Calendar.MINUTE, 0);
		start.set(Calendar.SECOND, 0);

		Calendar end = (Calendar) start.clone();
		end.add(Calendar.DAY_OF_MONTH, days-1);
		end.set(Calendar.HOUR, 23);
		end.set(Calendar.MINUTE, 59);
		end.set(Calendar.SECOND, 59);

		rpsEntrySet.removeIf((entry) -> {
			//check if its in time
			if(entry.getStart().compareTo(start) >= 0 && entry.getStart().compareTo(end) <= 0) {

				//check if type match
				if((entry.getType().equals(type) ||  type.equals(RPSEntry.Type.ALL_TYPES))) {

					//check if department match
					if(entry.getDepartment().equals(department) || department.equals(RPSDepartment.ALL)) {
						return true;
					}
				}
			}
			return false;
		});

		memoryHasChanged = true;
	}

//    @Override
//    public void deleteEntriesByTypeDepartment(Calendar date, int days, RPSEntry.Type type, RPSDepartment department) throws IOException {
//	removeEntriesByTypeDepartment(date, days, type, department);
//    }

	@Override
	public void addRPSEntry(RPSEntry entry) {
		rpsEntrySet.add(entry);
		memoryHasChanged = true;
	}

	@Override
	public void addRPSEntryList(Collection<RPSEntry> list) {
		rpsEntrySet.addAll(list);
		memoryHasChanged = true;
	}

	//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="member methods">

	@Override
	public Set<RPSMember> getRPSMemberSet() {
		Set<RPSMember> set = new HashSet();

		rpsEntrySet.stream().forEach((e) -> {
			set.addAll(e.getMemberList());
		});

		return set;
	}

	//</editor-fold>

	@Override
	public Set<String> getAnkerSet() throws IOException {
		Set<String> ankerSet = new HashSet<>();
		getRPSEntrySet().forEach((t) -> {
			if(t.hasAnker() && t.getAnker() != null)
				ankerSet.add(t.getAnker());
		});
		return ankerSet;
	}


	//<editor-fold defaultstate="collapsed" desc="department functions">

	@Override
	public void setRPSDepartmentList(Collection<RPSDepartment> list) {
		rpsDepartmentSet.clear();
		rpsDepartmentSet.addAll(list);
		memoryHasChanged = true;
	}

	@Override
	public void addRPSDepartment(RPSDepartment department) {
		rpsDepartmentSet.add(department);
		memoryHasChanged = true;
	}

	@Override
	public Set<RPSDepartment> getRPSDepartmentSet() {

		return rpsDepartmentSet;
	}

//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="selected member functions">

	@Override
	public Set<RPSMember> getSelectedMemberSet() {
		return selectedMemberSet;
	}

	@Override
	public void setSelectedMemberList(Collection<RPSMember> memberList) {
		selectedMemberSet.clear();
		addSelectedMemberList(memberList);
	}

	@Override
	public void addSelectedMember(RPSMember member) {
		selectedMemberSet.add(member);
		memoryHasChanged = true;
	}

	@Override
	public void addSelectedMemberList(Collection<RPSMember> memberList) {
		selectedMemberSet.addAll(memberList);
		memoryHasChanged = true;
	}

	@Override
	public void removeSelectedMember(RPSMember member) {
		selectedMemberSet.remove(member);
		memoryHasChanged = true;
	}

	//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="member to member functions">

	@Override
	public Set<RPSMember> getM2MSet() {
		return memberToMemberSet;
	}

	@Override
	public void setM2MList(Collection<RPSMember> memberList) {
		memberToMemberSet.clear();
		addM2MList(memberList);
	}

	@Override
	public void addM2M(RPSMember member) {
		memberToMemberSet.add(member);
		memoryHasChanged = true;
	}

	@Override
	public void addM2MList(Collection<RPSMember> memberList) {
		memberToMemberSet.addAll(memberList);
		memoryHasChanged = true;
	}

	@Override
	public void removeM2M(RPSMember member) {
		memberToMemberSet.remove(member);
		memoryHasChanged = true;
	}

	//</editor-fold>

    
    
    
    /*
    * private methods
    */

	@Override
	public void close() throws IOException {

		/**
		 * prompts a dialog if data should be stored, if memory has changed
		 */
		if(memoryHasChanged) {

			Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
			alert.setTitle("Speichern & Schließen");
			alert.setHeaderText("Daten speichern ?");
			alert.setContentText("Wollen sie die Daten vor dem Schließen speichern ?");

			ButtonType yes = new ButtonType("Ja");
			ButtonType no = new ButtonType("Nein, danke");

			alert.getButtonTypes().setAll(yes, no);

			Optional<ButtonType> result = alert.showAndWait();

			if(result == null || result.equals(no)) {
				LOGGER.log(Level.INFO, "save action is declined");
			} else {
				save();
			}
		}
	}
}
