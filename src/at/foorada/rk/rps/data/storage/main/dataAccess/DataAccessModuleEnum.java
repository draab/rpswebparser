/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.storage.main.dataAccess;

import at.foorada.rk.rps.data.storage.db.SQLiteDBMS;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author danie
 */
public enum DataAccessModuleEnum {
    SQLITE("rkrps"),
    JSON("rkjson");

    
    private final String extension;

    private DataAccessModuleEnum(String extension) {
	this.extension = extension;
    }
    
    public String getPlainExtension() {
	return extension;
    }
    
    public String getFilterExtension() {
	return "*."+extension;
    }
    
    public static DataAccessModuleEnum getFileTypeEnumFromFile(File file) throws IOException {
	
	if(file != null) {
	    String path = file.getAbsolutePath();
	    int dotIdx = path.lastIndexOf(".");
	    String extension = path.substring(dotIdx+1);

	    for(DataAccessModuleEnum iter : values()) {
		if(iter.getPlainExtension().equals(extension)) {
		    return iter;
		}
	    }
	    throw new IOException("not a valid extensions");
	}
	throw new IOException("null is given");
    }
    
    public static DataAccessInterface getInterfaceFromFile(File file) throws IOException {
	
	switch(getFileTypeEnumFromFile(file)) {
	    case JSON:
		return new DataAccessJson(file);
	    case SQLITE:
		try {
		    return new DataAccessDatabase(new SQLiteDBMS(file.getAbsolutePath()));
		} catch (ClassNotFoundException | SQLException ex) {
		    throw new IOException(ex);
		}
	    default:
		return new NoDataAccess();
	}
    }
}
