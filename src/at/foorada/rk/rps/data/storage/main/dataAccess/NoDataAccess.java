/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.storage.main.dataAccess;

import java.io.IOException;
import java.util.logging.Logger;

/**
 *
 * @author danie
 */
public class NoDataAccess extends DataAccessMemoryBuffer {

    private static final Logger LOGGER = Logger.getLogger(NoDataAccess.class.getName());

    @Override
    public String getStorageInfo() {
        return "only memory";
    }

    @Override
    public void save() throws IOException {
        LOGGER.info("The save-action does not have an effect");
        throw new RuntimeException("The save-action does not have an effect");
    }

    @Override
    public void close() {}
}