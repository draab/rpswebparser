package at.foorada.rk.rps.data.web;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class BrowserSimulator {
    
    private static final Logger LOGGER = Logger.getLogger(BrowserSimulator.class.getName());

    private List<String> cookies;
    private HttpsURLConnection conn;

    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36";
    private final static String ACCEPT_LANGUAGE = "de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4";
    private final static String ACCEPT = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

    public BrowserSimulator() {
        // make sure cookies is turn on
        CookieHandler.setDefault(new CookieManager());
        cookies = new ArrayList<>();
    }

    private void loadCookiesToConnection() {
        if(conn != null  && this.cookies != null)
            for (String cookie : this.cookies) {
                conn.addRequestProperty("Cookie", cookie.split(";", 1)[0]);
            }
    }

    private void loadCookiesFromConnection() {

        // Get the response cookies
        if(conn != null)
            this.cookies = conn.getHeaderFields().get("Set-Cookie");
    }

    public String sendPOSTRequest(String url, String host) throws IOException {
        return sendPOSTRequest(url, host, Collections.emptyMap(), USER_AGENT, ACCEPT, ACCEPT_LANGUAGE);
    }
    public String sendPOSTRequest(String url, String host, Map<String, String> properties) throws IOException {
        return sendPOSTRequest(url, host, properties, USER_AGENT, ACCEPT, ACCEPT_LANGUAGE);
    }

    public String sendPOSTRequest(String url, String host, Map<String, String> properties, String userAgent) throws IOException {
        return sendPOSTRequest(url, host, properties, userAgent, ACCEPT, ACCEPT_LANGUAGE);
    }

    public String sendPOSTRequest(String url, String host, Map<String, String> properties, String userAgent, String accept, String acceptLanguage) throws IOException {
        try {
            URL obj = new URL(url);
            conn = (HttpsURLConnection) obj.openConnection();

            //add reuqest header
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Host", host);

            conn.setRequestProperty("User-Agent", userAgent);
            conn.setRequestProperty("Accept", accept);
            conn.setRequestProperty("Accept-Language", acceptLanguage);
            conn.setRequestProperty("Accept-Charset", "UTF-8");

            loadCookiesToConnection();


            conn.setRequestProperty("Connection", "keep-alive");
//            conn.setRequestProperty("Referer", "https://dienstplan.o.roteskreuz.at");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");

            boolean first = true;
            StringBuffer bfr = new StringBuffer();
            for(Map.Entry<String, String> entry : properties.entrySet()) {
                if(first) {
                    first = false;
                } else {
                    bfr.append("&");
                }
                bfr.append(entry.getKey());
                bfr.append("=");
                bfr.append(entry.getValue());
            }

            conn.setRequestProperty("Content-Length", Integer.toString(bfr.length()));

            conn.setDoOutput(true);
            conn.setDoInput(true);

            // Send post request

            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
            wr.writeBytes(bfr.toString());
            wr.flush();
            wr.close();

            int responseCode = conn.getResponseCode();
	    LOGGER.log(Level.FINE, "Sending ''POST'' request to URL : {0}", url);
            LOGGER.log(Level.FINEST, "Post parameters : {0}", bfr.toString());
            LOGGER.log(Level.FINE, "Response Code : {0}", responseCode);

            BufferedReader in =
                    new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            loadCookiesFromConnection();

	    LOGGER.log(Level.FINER, "Ports closed");

            return response.toString();
        }
        catch(IOException ioe) {
            throw ioe;
        }
    }

    public static void writeInFile(File f, String text) throws IOException {
        if (!f.exists()) {
            if (!f.createNewFile())
                throw new IOException("not able to create file");
        }

        Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(f), "utf-8"));
        out.write(text);
        out.close();
    }
}
