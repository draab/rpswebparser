/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.web;

import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessInterface;
import at.foorada.rk.rps.gui.MainWindowController;
import at.foorada.rk.rps.parser.RPSWebParserException;
import at.foorada.rk.rps.parser.RPSWebParserManager;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import javafx.concurrent.Task;

/**
 *
 * @author danie
 */
public class LoadingTask extends Task<RPSWebParserManager> {

    private static final Logger LOGGER = Logger.getLogger(LoadingTask.class.getName());
    
    private LoadingConfiguration config;
    private final DataAccessModule dataAccess;
    private RPSWebParserManager manager;
    
    /**
     * 
     * @param config
     * @param dataAccess
     * @param manager
     */
    public LoadingTask(LoadingConfiguration config, DataAccessModule dataAccess, RPSWebParserManager manager) {
	this.config = config;
	this.dataAccess = dataAccess;
	this.manager = manager;
    }
    
    @Override
    protected RPSWebParserManager call() throws IOException, RPSWebParserException {
	LOGGER.finest("loading task has been started");

	long steps = 4; long curStep=0;
	if(config.delete)
	    steps++;
	
	for(RPSDepartment d:config.departments) {
	    if(config.delete) {
		updateProgress(curStep++, steps);
		updateMessage("Alte Einträge werden gelöscht ...");
		dataAccess.getDataAccessProperty().get().removeEntriesByTypeDepartment(config.start, config.days, RPSEntry.Type.ALL_TYPES, d);
		LOGGER.fine("old entries removed");
	    }

	    updateProgress(curStep++, steps);
	    updateMessage("Einstellungen werden getroffen ...");
	    List<RPSWebParserManager.RPSParseProperty> parsePropertyList = RPSWebParserManager.filterOutParsed(
	    		dataAccess.getDataAccessProperty().get().getRPSDepartmentSet(),
				dataAccess.getDataAccessProperty().get().getRPSEntrySet(), config.start, config.days, d);
	    LOGGER.fine("property list for parsing created");

	    updateProgress(curStep++, steps);
	    updateMessage("Daten für "+d.getDescription()+" werden von Website abgerufen ...");
	    List<RPSEntry> entryList = manager.parseDays(parsePropertyList);
	    LOGGER.fine("parsing from website finished");


	    updateProgress(curStep++, steps);
	    updateMessage("In Datenbank schreiben ...");
	    dataAccess.getDataAccessProperty().get().addRPSEntryList(entryList);
	    LOGGER.fine("entries successfully insert into db");
	}

	updateProgress(curStep++, steps);
	updateMessage("Anzeige aktualisieren ...");
	
	MainWindowController.doAppReCalculation();
	
	return manager;
    }
    
    /**
     * helper class for the loading values
     */
    public static class LoadingConfiguration {

	private Calendar start = null;
	private int days = 0;
	private Set<RPSDepartment> departments;// = new HashSet<>();
	private boolean delete = true;
	
	public LoadingConfiguration(Calendar start, int days, Set<RPSDepartment> departments, boolean delete) {
	    this.start = start;
	    this.days = days;
	    this.departments = departments;
	    this.delete = delete;
	}
	
	public LoadingConfiguration(LocalDate start, int days, Set<RPSDepartment> departments, boolean delete) {
	    this((Calendar)null, days, departments, delete);
	    
	    if(start != null) {
		Date parse = Date.from(start.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
		this.start = Calendar.getInstance();
		this.start.setTime(parse);
	    }
	}

	public Calendar getStart() {
	    return start;
	}

	public void setStart(Calendar start) {
	    this.start = start;
	}

	public int getDays() {
	    return days;
	}

	public void setDays(int days) {
	    this.days = days;
	}

	public Set<RPSDepartment> getDepartments() {
	    return departments;
	}

	public void setDepartment(Set<RPSDepartment> departments) {
	    this.departments = departments;
	}
	
	public void addDepartment(RPSDepartment department) {
	    departments.add(department);
	}

	public boolean isDelete() {
	    return delete;
	}

	public void setDelete(boolean delete) {
	    this.delete = delete;
	}
    }
}