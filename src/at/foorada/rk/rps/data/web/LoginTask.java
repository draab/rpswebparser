/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.data.web;

import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessInterface;
import at.foorada.rk.rps.parser.RPSWebParser;
import at.foorada.rk.rps.parser.RPSWebParserManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;

/**
 *
 * @author danie
 */
public class LoginTask extends Task<RPSWebParserManager> {

	private static final Logger LOGGER = Logger.getLogger(LoginTask.class.getName());

	private final String user;
	private final String password;
	private final DataAccessModule dataAccess;

	public LoginTask(String user, String password, DataAccessModule dataAccess) {
		this.user = user;
		this.password = password;
		this.dataAccess = dataAccess;
	}

	@Override
	protected RPSWebParserManager call() throws Exception {
		LOGGER.finest("loggin in task has been started");
		long steps = 3;


		updateProgress(1, steps);
		updateMessage("In Website einloggen.");
		RPSWebParserManager manager = new RPSWebParserManager(user, password);
		LOGGER.log(Level.INFO, "successfully logged in");

		updateProgress(2, steps);
		updateMessage("Abteilungen speichern");
		dataAccess.getDataAccessProperty().get().setRPSDepartmentList(RPSWebParser.parseDepartmentFromJobContainer(manager.getJobContainerFromWebsite()));
		LOGGER.log(Level.INFO, "departments stored");

		updateProgress(steps, steps);
		updateMessage("Erfolgreich auf der Website eingelogged");

		return manager;
	}
}