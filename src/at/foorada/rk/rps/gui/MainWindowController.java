package at.foorada.rk.rps.gui;

import at.foorada.rk.rps.Main;
import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.storage.main.LoadDataAccessTask;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessModuleEnum;
import at.foorada.rk.rps.data.web.LoadingTask;
import at.foorada.rk.rps.data.web.LoginTask;
import at.foorada.rk.rps.gui.dialog.loading.LoadingController;
import at.foorada.rk.rps.gui.dialog.overview.OverviewController;
import at.foorada.rk.rps.gui.dialog.PasswordInputDialog;
import at.foorada.rk.rps.gui.dialog.remove.RemoveController;
import at.foorada.rk.rps.gui.dialog.settings.SettingController;
import at.foorada.rk.rps.gui.dialog.versioning.VersioningController;
import at.foorada.rk.rps.gui.mainTabs.EntryTableController;
import at.foorada.rk.rps.gui.mainTabs.MemberTableController;
import at.foorada.rk.rps.gui.mainTabs.MemberToMemberTableController;
import at.foorada.rk.rps.data.QuickLoadConfiguration;
import at.foorada.rk.rps.parser.RPSWebParserException;
import at.foorada.rk.rps.parser.RPSWebParserManager;
import at.foorada.system.DesktopHelper;
import at.foorada.versioning.data.ReleasedVersion;
import at.foorada.versioning.net.VersionUtils;
import javafx.animation.*;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

import javafx.stage.WindowEvent;
import javafx.util.Duration;

public class MainWindowController implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(MainWindowController.class.getName());

    private static MainWindowController instance = null;

    @FXML private MenuItem newStorageMenuItem;
    @FXML private MenuItem loadStorageMenuItem;
    @FXML private MenuItem saveStorageMenuItem;
    @FXML private MenuItem settingsMenuItem;
    @FXML private MenuItem exitMenuItem;
    @FXML private MenuItem loadedOverviewMenuItem;
    @FXML private MenuItem removeEntryMenuItem;
    @FXML private MenuItem quickLoadMenuItem;
    @FXML private MenuItem parseMenuItem;
    @FXML private MenuItem helpMenuItem;
    @FXML private MenuItem infoMenuItem;

    @FXML private GridPane entryTableGridPane;
    @FXML private GridPane memberTableGridPane;

    @FXML public EntryTableController entryTableGridPaneController;
    @FXML public MemberTableController memberTableGridPaneController;
    @FXML public MemberToMemberTableController memberToMemberTableGridPaneController;

    private LoadingController loadingController = null;
    private OverviewController overviewController = null;
    private RemoveController removeController = null;
    private SettingController settingController = null;
    private VersioningController versionController = null;

    @FXML private TabPane contentTabPane;
    @FXML private Tab entryTableTab;
    @FXML private Tab memberTableTab;
    @FXML private Tab memberToMemberTableTab;


    public Alert storageOpenedAlert = null;
    public Alert storageErrorAlert = null;
    public Label mainMessageLabel;
    public Label storageLabel;
    public ProgressBar mainProgressBar;

    private final DataAccessModule dataAccess;

    private Preferences preferences;
    private RPSWebParserManager manager = null;
    private Task<Boolean> saveTask;

    public static MainWindowController loadWindow(Stage primary) throws IOException {

        final FXMLLoader loader = new FXMLLoader(
                MainWindowController.class.getResource("mainWindow.fxml")
        );

        Parent root = loader.load();
        MainWindowController cntr = loader.getController();
        primary.setTitle("RK RPS Parser");
        primary.setScene(new Scene(root, 800, 600));
        primary.setMinWidth(550);

        return cntr;
    }

    public MainWindowController() {
        instance = this;
        dataAccess = new DataAccessModule();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        preferences = Preferences.systemNodeForPackage(this.getClass());

        //add listener for changes on the dataAccess object; replaces the other calls after reloading
        dataAccess.getDataAccessProperty().addListener((observable, oldValue, newValue) -> {
            try {
                doCalculation();
                Platform.runLater(this::doGuiUpdate);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "do calculation failed", e);
            }
        });

        try {
            entryTableGridPaneController.setParentOfTab(this);
            entryTableGridPaneController.initDataAccessModule(dataAccess);
            memberTableGridPaneController.setParentOfTab(this);
            memberTableGridPaneController.initDataAccessModule(dataAccess);
            memberToMemberTableGridPaneController.setParentOfTab(this);
            memberToMemberTableGridPaneController.initDataAccessModule(dataAccess);

            // add listener for switching tab, bind the property from current open tab to main message label
            contentTabPane.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {

                Tab tab = null;
                try {
                    tab = contentTabPane.getTabs().get(newValue.intValue());
                } catch(Exception e) {}

                if(tab != null) {
                    mainMessageLabel.textProperty().unbind();
                    if(tab.equals(entryTableTab)) {
                        mainMessageLabel.textProperty().bind(entryTableGridPaneController.mainMessageStringProperty);
                    } else if(tab.equals(memberTableTab)) {
                        mainMessageLabel.textProperty().bind(memberTableGridPaneController.mainMessageStringProperty);
                    } else if(tab.equals(memberToMemberTableTab)) {
                        mainMessageLabel.textProperty().bind(memberToMemberTableGridPaneController.mainMessageStringProperty);
                    } else {
                        LOGGER.log(Level.WARNING, "selected tab is unknown");
                    }
                } else {
                    LOGGER.log(Level.WARNING, "error in getting selected tab");
                }
            });

//            MainWindowController.doAppReCalculation();
//            MainWindowController.doAppGuiUpdate();

            resetMainMessageProgress();

            setUpVersionChecker();

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "", e);
        }

    }

    private void showVersionCheckerErrorDialog(WorkerStateEvent event) {

        final Alert alert = new Alert(Alert.AlertType.NONE);
        alert.setTitle("Versionscheck");
        alert.setHeaderText("Es konnte nicht überprüft werden ob eine neue Version vorliegt.");
        alert.setContentText("Wollen sie auf der Website nachsehen ob eine neue Version existiert ?");

        ButtonType showWebsite = new ButtonType("Website öffnen");
        ButtonType no = new ButtonType("Nein");

        alert.getButtonTypes().add(showWebsite);
        alert.getButtonTypes().add(no);

        //auto close dialog after 10 seconds
        PauseTransition pt = new PauseTransition(Duration.seconds(10));
        pt.setOnFinished(event1 -> alert.close());
        pt.play();

        Platform.runLater(() -> {
            ButtonType result = alert.showAndWait().orElse(no);
            if(result.equals(showWebsite)) {
                try {
                    DesktopHelper.openWebpage(VersionUtils.getWebURL());
                } catch (IOException | URISyntaxException e) {
                    LOGGER.log(Level.WARNING, e.getMessage());
                }
            }
        });

        Main.APP_LOGGER.log(Level.WARNING, "failed to check version", event.getSource().getException());
    }

    private void setUpVersionChecker() {

        //initiate version checker task
        VersioningController.VersionCheckerTask checkerTask = new VersioningController.VersionCheckerTask();

        // show version window if task was successful
        checkerTask.setOnSucceeded(event -> {
            List<ReleasedVersion> list = checkerTask.getValue();
            if(list != null && !list.isEmpty()) {

                try {
                    versionController = VersioningController.loadWindow();
                    versionController.getStage().setTitle("Neue Version verfügbar");
                    versionController.setContentList(list);
                    versionController.getStage().show();
                } catch (IOException e) {
                    Main.APP_LOGGER.log(Level.WARNING, "not able to open html window for version overview", e);
                }
            }
        });

        // if the task failed show alert dialog with button to open website
        checkerTask.setOnFailed(this::showVersionCheckerErrorDialog);
        checkerTask.setOnCancelled(checkerTask.getOnFailed());

        new Thread(checkerTask).start();
    }

    /**
     * shows message in the message label of the loading window for given milliseconds<br>
     * if millis <= 0 message label will not disapear
     * @param msg
     * @param millis
     */
    private void showMessage(String msg, double millis) {
        mainMessageLabel.setText(msg);
        LOGGER.log(Level.INFO, "show message: {0}", msg);

        //in case of parameter >= label will not be reseted
        if(millis > 0) {
            new Timeline(new KeyFrame(
                    Duration.millis(millis), (event) -> {
                //reset text and make label invisible
                mainMessageLabel.setText("");
            })).playFromStart();
        }
    }
    private void showMessage(String msg) {showMessage(msg, 0);}

    public void bindTaskToMessageProgress(Task task) {
        setGlobalCursor(Cursor.WAIT);

        mainProgressBar.setVisible(true);

        mainProgressBar.progressProperty().unbind();
        mainProgressBar.progressProperty().bind(task.progressProperty());

        mainMessageLabel.textProperty().unbind();
        mainMessageLabel.textProperty().bind(task.messageProperty());
    }

    private void setSceneCursor(Cursor cursor) {
        Scene scene = entryTableGridPane.getScene();
        if(scene != null)
            scene.getRoot().setCursor(cursor);
    }

    public void displayMainMessage(String message, double durationMs) {
        mainMessageLabel.setText(message);

        //in case of parameter >= label will not be reseted
        if(durationMs > 0) {
            new Timeline(new KeyFrame(
                    Duration.millis(durationMs), (event) -> {
                resetMainMessageProgress();
            })).playFromStart();
        }
    }

    /**
     * resets the main message textField and main progressBar<br/>
     * the textProperty of the main message textField will be bind
     * to the selected tab controller
     */
    public void resetMainMessageProgress() {
        mainProgressBar.progressProperty().unbind();
        mainProgressBar.setVisible(false);

        setGlobalCursor(Cursor.DEFAULT);

        mainMessageLabel.textProperty().unbind();
        if(entryTableTab.isSelected()) {
            mainMessageLabel.textProperty().bind(entryTableGridPaneController.mainMessageStringProperty);
        } else if(memberTableTab.isSelected()) {
            mainMessageLabel.textProperty().bind(memberTableGridPaneController.mainMessageStringProperty);
        } else if(memberToMemberTableTab.isSelected()) {
            mainMessageLabel.textProperty().bind(memberToMemberTableGridPaneController.mainMessageStringProperty);
        } else {
            LOGGER.log(Level.WARNING, "selected tab is unknown");
        }
    }

    public void loadNewDataAccess(File dataAccessFile) {

        LoadDataAccessTask loadTask = new LoadDataAccessTask(dataAccess, dataAccessFile);

        loadTask.setOnFailed(event -> {
            resetMainMessageProgress();
            LOGGER.log(Level.SEVERE, "loading task failed");
            storageOpenedAlert = new Alert(Alert.AlertType.INFORMATION);
            storageOpenedAlert.setTitle("Fehler beim öffnen");
            storageOpenedAlert.setHeaderText("Datenbank konnte nicht geöffnet werden.");
            storageOpenedAlert.showAndWait();
        });
        loadTask.setOnCancelled(loadTask.getOnFailed());

        loadTask.setOnSucceeded(event -> {
            if(loadTask.getValue()) {

                storageOpenedAlert = new Alert(Alert.AlertType.INFORMATION);
                storageOpenedAlert.setTitle("Datei geöffnet");
                storageOpenedAlert.setHeaderText(dataAccessFile.getName()+" wurde erfolgreich geöffnet werden.");
                storageOpenedAlert.show();

            } else {
                storageOpenedAlert = new Alert(Alert.AlertType.INFORMATION);
                storageOpenedAlert.setTitle("Fehler beim öffnen");
                storageOpenedAlert.setHeaderText("Datenbank konnte nicht geöffnet werden.");
                storageOpenedAlert.showAndWait();
            }

            resetMainMessageProgress();
        });

        bindTaskToMessageProgress(loadTask);

        new Thread(loadTask).start();
    }

    private void openDataAccessMenu(boolean newDataAccessFile) {

        FileChooser chooser = new FileChooser();
        chooser.setTitle("Datei auswählen");
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("RK RPS File", DataAccessModuleEnum.JSON.getFilterExtension()));
//        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("lokale Datenbankdatei", DataAccessModuleEnum.SQLITE.getFilterExtension()));

        File dataAccessFile = null;
        if(newDataAccessFile) {
            try {
                dataAccessFile = chooser.showSaveDialog(contentTabPane.getScene().getWindow());
                if(dataAccessFile != null) dataAccessFile.createNewFile();
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        } else
            dataAccessFile = chooser.showOpenDialog(contentTabPane.getScene().getWindow());

        if(dataAccessFile==null) {
            LOGGER.finest("open dialog closed");
            return;
        }

        //create file in if clause above
        if(/*newDataAccessFile ||*/ (dataAccessFile.exists() && dataAccessFile.isFile() && dataAccessFile.canRead()&& dataAccessFile.canWrite())) {

            loadNewDataAccess(dataAccessFile);

        } else {

            storageErrorAlert = new Alert(Alert.AlertType.ERROR);
            storageErrorAlert.setTitle("Fehler beim öffnen");
            storageErrorAlert.setHeaderText("Die Datei kann nicht geöffnet werden");
            storageErrorAlert.showAndWait();
        }
    }

    public void saveData() {

        //save task is already running; do not start a second one
        if(saveTask != null && saveTask.isRunning())
            return;

        saveTask = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                try {
                    updateMessage("save to storage ...");
                    updateProgress(-1, -1);
                    dataAccess.getDataAccessProperty().get().save();
                } catch (IOException iOException) {
                    LOGGER.log(Level.SEVERE, "saveTask failed", iOException);
                    throw iOException;
                }
                return Boolean.TRUE;
            }
        };

        saveTask.setOnFailed((event) -> {

            storageErrorAlert = new Alert(Alert.AlertType.INFORMATION);
            storageErrorAlert.setTitle("Information");
            storageErrorAlert.setHeaderText("Es wird nichts gespeichert");
            storageErrorAlert.showAndWait();

            //reset message also on failing
            resetMainMessageProgress();
            saveStorageMenuItem.setDisable(false);  //enable save menuItem
        });

        saveTask.setOnCancelled((event) -> {
            //reset message after canceling
            resetMainMessageProgress();
            saveStorageMenuItem.setDisable(false);  //enable save menuItem
        });

        saveTask.setOnSucceeded((event) -> {
            //reset message after succeed
            resetMainMessageProgress();
            saveStorageMenuItem.setDisable(false);  //enable save menuItem
        });

        // bind main message and progress to task
        bindTaskToMessageProgress(saveTask);
        saveStorageMenuItem.setDisable(true);	//disable save menuItem

        new Thread(saveTask).start();	//start save task

    }

    /**
     * performs close call for every object that needs a close call for a nice shutdown
     */
    public void close() {
        try {
            dataAccess.getDataAccessProperty().get().close();
        } catch (IOException ex) {
            LOGGER.log(Level.WARNING, "unable to close data access object", ex);
        }
        if(loadingController != null) {
            loadingController.getStage().close();
        }
        if(overviewController != null) {
            overviewController.getStage().close();
        }
        if(removeController != null) {
            removeController.getStage().close();
        }
        if(settingController != null) {
            settingController.getStage().close();
        }
        if(entryTableGridPaneController != null) {
            entryTableGridPaneController.close();
        }
        if(memberTableGridPaneController != null) {
            memberTableGridPaneController.close();
        }
        if(memberToMemberTableGridPaneController != null) {
            memberToMemberTableGridPaneController.close();
        }

        if(versionController != null)
            versionController.getStage().close();

    }


    public static void setGlobalCursor(Cursor c) {
        if(instance != null) instance.setSceneCursor(c);
    }

    public static void doAppReCalculation() throws IOException {
        LOGGER.log(Level.FINE, "static app re calculation called");
        if(instance != null) instance.doCalculation();
    }

    public static void doAppGuiUpdate() {
        LOGGER.log(Level.FINE, "static app gui update called");
        if(instance != null) instance.doGuiUpdate();
    }

    /**
     * recalculates the data after it has changed<br/>
     * should run in own thread or at least not in gui thread
     * @throws IOException
     */
    public void doCalculation() throws IOException {
        entryTableGridPaneController.doCalculation();
        memberTableGridPaneController.doCalculation();
        memberToMemberTableGridPaneController.doCalculation();
        LOGGER.fine("calculations after data-change done");
    }

    /**
     * gui updates after data has changed<br/>
     * has to run in gui thread or in Platform->runLater clause
     * @throws IOException
     */
    public void doGuiUpdate() {
        storageLabel.setText(dataAccess.getDataAccessProperty().get().getStorageInfo());

        entryTableGridPaneController.doGuiUpdate();
        memberTableGridPaneController.doGuiUpdate();
        memberToMemberTableGridPaneController.doGuiUpdate();

        LOGGER.fine("gui update done");
    }

    @FXML private void loadMenuItemAction(ActionEvent actionEvent) {

        try {

            loadingController = LoadingController.loadWindow();
            loadingController.initDataAccessModule(dataAccess);

            parseMenuItem.setDisable(true);
            LOGGER.finest("load entry stage opened");
            loadingController.getStage().setOnCloseRequest(event -> {
                parseMenuItem.setDisable(false);
                LOGGER.finest("load entry stage closed");
            });
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "io exception in loadMenuItemAction", e);
        }
    }

    /**
     * handle click on overview menu item button click
     * @param actionEvent
     */
    @FXML private void loadedOverviewMenuItemAction(ActionEvent actionEvent) {
        try {
            overviewController = OverviewController.loadWindow();
            overviewController.initDataAccessModule(dataAccess);
            loadedOverviewMenuItem.setDisable(true);
            LOGGER.finest("overview stage opened");

            overviewController.getStage().setOnCloseRequest(event -> {
                LOGGER.finest("overview stage closed");
                loadedOverviewMenuItem.setDisable(false);
            });
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "io exception in loadedOverviewMenuItemAction", e);
        }

    }

    /**
     * handling remove entry menu item button click
     * @param actionEvent
     */
    @FXML private void removeEntryMenuItemAction(ActionEvent actionEvent) {
        try {
            removeController = RemoveController.loadWindow();
            removeController.initDataAccessModule(dataAccess);

            removeEntryMenuItem.setDisable(true);
            LOGGER.finest("remove stage opened");
            removeController.getStage().setOnCloseRequest(event -> {
                LOGGER.finest("remove stage closed");
                removeEntryMenuItem.setDisable(false);
            });
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "io exception in removeEntryMenuItemAction", e);
        }
    }

    /**
     * help menu button <br/>
     * not implemented at the moment
     */
    @FXML private void helpMenuItemAction(ActionEvent actionEvent) {
    }

    /**
     * starts the info popup
     * @param actionEvent
     */
    @FXML private void infoMenuItemAction(ActionEvent actionEvent) {
        VersioningController.VersionCheckerTask checkerTask = new VersioningController.VersionCheckerTask();
        checkerTask.setOnSucceeded(event -> {
            try {
                versionController = VersioningController.loadWindow();
                versionController.getStage().setTitle("Info");

                List<ReleasedVersion> list = checkerTask.getValue();
                if(list != null && !list.isEmpty()) {
                    versionController.setContentList(list);
                }

                versionController.getStage().show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        checkerTask.setOnFailed(this::showVersionCheckerErrorDialog);
        checkerTask.setOnCancelled(checkerTask.getOnFailed());

        new Thread(checkerTask).start();
    }

    /**
     * method for executing the quick load <br/>
     * called via FXML from mainWindow.fxml (MenuItem)<br/>
     * first the login task is doing the login call to website;
     * afterwards loading task is downloading entries from website
     * (in case of successful login task)
     * @param actionEvent
     */
    @FXML private void quickLoadAction(ActionEvent actionEvent) {

        quickLoadMenuItem.setDisable(true);
        QuickLoadConfiguration qlConfig = dataAccess.getDataAccessProperty().get().getQuickLoadConfiguration();

        //initate password dialog
        PasswordInputDialog passwordDialog = new PasswordInputDialog();
        passwordDialog.setTitle("Quick load");
        passwordDialog.setHeaderText("Passwortabfrage");
        passwordDialog.setContentText("Passwort für \'"+qlConfig.mail+"\':");

        //show password dialog and wait for input
        String pass = passwordDialog.showAndWait().orElse(null);

        //end this method if pass is null, dialog is closed
        if(pass == null) {
            quickLoadMenuItem.setDisable(false);
            LOGGER.log(Level.FINEST, "quickload password dialog is closed");
            return;
        }

        //prepare loginTask for the login process
        LoginTask loginTask = new LoginTask(qlConfig.mail, pass, dataAccess);

        //set hander for succeed and ever abort
        loginTask.setOnSucceeded(loginEvent -> {

            //set start date for loading task
            Calendar start = Calendar.getInstance();
            start.add(Calendar.DAY_OF_MONTH, qlConfig.offset);

            // prepare loading task
            LoadingTask.LoadingConfiguration config = new LoadingTask.LoadingConfiguration(start, qlConfig.days, qlConfig.departments, true);
            LoadingTask loadingTask = new LoadingTask(config, dataAccess, loginTask.getValue());

            //set hander for succeed and ever abort
            loadingTask.setOnSucceeded(event -> {

                resetMainMessageProgress();

                //doCalculation done in task
                MainWindowController.doAppGuiUpdate();

                quickLoadMenuItem.setDisable(false);
            });

            //set handler for failed (same as cancelled)
            loadingTask.setOnFailed((event) -> {
                resetMainMessageProgress();
                quickLoadMenuItem.setDisable(false);

                Throwable t = event.getSource().getException();

                if(t instanceof IOException) {
                    // thrown in case of connection problems
                    showMessage("Es kann keine Verbindung hergestellt werden.");
                } else if(t instanceof RPSWebParserException) {
                    // thrown in case of parsing problems or wrong login data.
                    showMessage("Fehler beim lesen der Daten.");
                } else {
                    showMessage("Fehler beim Laden der Daten.");
                }

                LOGGER.log(Level.SEVERE, "exception in login task", t);
            });
            loadingTask.setOnCancelled(loadingTask.getOnFailed());

            //bind message label to loading task
            bindTaskToMessageProgress(loadingTask);

            //start loading task
            new Thread(loadingTask).start();
        });

        //set failed action (same as cancelled action)
        loginTask.setOnFailed((event) -> {
            resetMainMessageProgress();

            Throwable t = event.getSource().getException();

            if(t instanceof IOException) {
                // thrown in case of connection problems
                showMessage("Es kann keine Verbindung hergestellt werden.");
            } else if(t instanceof RPSWebParserException) {
                // thrown in case of parsing problems or wrong login data.
                showMessage("Login fehlgeschlagen. Überprüfe die Daten.");
            } else {
                showMessage("Fehler beim Einloggen.");
            }

            LOGGER.log(Level.SEVERE, "exception in login task", t);
        });
        loginTask.setOnCancelled(loginTask.getOnFailed());

        //bind message label to login task
        bindTaskToMessageProgress(loginTask);

        //start login task
        new Thread(loginTask).start();
    }

    /**
     * method for menuItem settings<br/>
     * starts the setting window
     * @param event
     */
    @FXML private void settingsAction(ActionEvent event) {
        try {
            settingController = SettingController.loadWindow();
            settingController.initDataAccessModule(dataAccess);

            settingsMenuItem.setDisable(true);
            LOGGER.finest("setting stage opened");
            settingController.getStage().setOnCloseRequest(closeEvent -> {
                settingsMenuItem.setDisable(false);
                LOGGER.finest("setting stage closed");
            });
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

    /**
     * method for menuItem exit<br/>
     * fires a WINDOW_CLOSE_REQUEST for this stage (stage of contentTabPane)
     * @param event
     */
    @FXML private void exitAction(ActionEvent event) {

        Stage stage  = (Stage) contentTabPane.getScene().getWindow();
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));

    }

    /**
     * handling load data menu item click
     * @param actionEvent
     */
    @FXML private void loadDataAction(ActionEvent actionEvent) {
        openDataAccessMenu(false);
    }

    /**
     * handling new data menu item click
     * @param actionEvent
     */
    @FXML private void newDataAction(ActionEvent actionEvent) {
        openDataAccessMenu(true);
    }

    /**
     * handling save data menu item button click
     * @param actionEvent
     */
    @FXML private void saveDataAction(ActionEvent actionEvent) {
        saveData();
    }

    public void switchToTab(MainWindowTab tab) {

        Tab tabToSwitch = null;

        switch(tab) {
            case ENTRY_LIST:
                tabToSwitch = entryTableTab;
                break;
            case MEMBER_TO_MEMBER:
                tabToSwitch = memberToMemberTableTab;
                break;
            case SELECTED_MEMBERS:
                tabToSwitch = memberTableTab;
                break;
        }

        contentTabPane.getSelectionModel().select(tabToSwitch);
    }

    public enum MainWindowTab {
        ENTRY_LIST, SELECTED_MEMBERS, MEMBER_TO_MEMBER;
    }
}