package at.foorada.rk.rps.gui.console;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URL;
import java.util.ResourceBundle;


public class TextController implements Initializable {

    public TextArea consoleTextArea;
    private PrintStream ps ;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ps = new PrintStream(new Console(consoleTextArea)) ;

        consoleTextArea.setEditable(false);
    }

    public PrintStream getPrintStream() {
        return ps;
    }

    public class Console extends OutputStream {
        private TextArea console;

        public Console(TextArea console) {
            this.console = console;
        }

        public void appendText(String valueOf) {
            Platform.runLater(() -> console.appendText(valueOf));
        }

        @Override
        public void write(int b) throws IOException {
            appendText(String.valueOf((char)b));
        }
    }
}
