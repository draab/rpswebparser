package at.foorada.rk.rps.gui.dialog.loading;

import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.EscKeyClosingEventHandler;
import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.web.LoadingTask;
import at.foorada.rk.rps.data.web.LoginTask;
import at.foorada.rk.rps.gui.MainWindowController;
import at.foorada.rk.rps.parser.RPSWebParserException;
import at.foorada.rk.rps.parser.RPSWebParserManager;
import java.io.IOException;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.stage.Stage;
import javafx.util.Duration;

public class LoadingController implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(LoadingController.class.getName());

    public Button loginButton;
    public ProgressBar loadingProgressBar;
    public Label loadingMessageLabel;
    public CheckBox deleteFutureCheckBox;
    private Stage thisStage;

    public DatePicker dateDatePicker;
    public ChoiceBox<RPSDepartment> departmentChoiceBox;
    public Slider daysSlider;
    public Label daysLabel;
    public TextField usernameTextField;
    public PasswordField passwordPasswordField;

    public Button loadingButton;

    private DataAccessModule dataAccess;

    private RPSWebParserManager manager = null;
    private static String preFillUserName = null;

    private LoginTask loginTask = null;
    private LoadingTask loadingTask =null;


    public static LoadingController loadWindow() throws IOException {

        final FXMLLoader loadingSceneLoader = new FXMLLoader(
                LoadingController.class.getResource("loading.fxml")
        );

        Parent page = loadingSceneLoader.load();
        Scene scene = new Scene(page);
        //handler for ESC-Key -> close window (fire close event)
        scene.addEventHandler(KeyEvent.KEY_PRESSED, new EscKeyClosingEventHandler());

        Stage loadingWindow = new Stage();
        loadingWindow.setTitle("Einträge laden");
        loadingWindow.setScene(scene);
        loadingWindow.show();

        LoadingController controller = loadingSceneLoader.getController();
        controller.thisStage = loadingWindow;
        return controller;
    }


    public Stage getStage() {
        return thisStage;
    }

    /**
     * Add listener for ESC Key to close window.
     * init day slider.
     * set visibility for fields according to login state (manager != null)
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        //init slider
        daysSlider.setMin(1);
        daysSlider.setMax(31);
        daysSlider.setBlockIncrement(1);
        daysSlider.setValue(1);
        daysSlider.setMinorTickCount(0);
        daysSlider.setMajorTickUnit(1);
        daysSlider.snapToTicksProperty().set(true);     //no comma values
        daysLabel.textProperty().bind(Bindings.format("%.0f",daysSlider.valueProperty()));

        //set data picker to today
        dateDatePicker.setValue(LocalDate.now());

        //fill username field with last input
        if(preFillUserName != null)
            usernameTextField.setText(preFillUserName);

        deleteFutureCheckBox.setSelected(true);

        //check if logged in
        if(manager == null) {
            setGUILoginState();
        } else {
            setGUILoadState();
        }
    }


    /**
     * do call this only once on object life time
     * store Database
     * Preload department CheckBox and init day slider.
     * @param dataAccessModule dataAccess for performing actions
     * @throws java.io.IOException
     */
    public void initDataAccessModule(DataAccessModule dataAccessModule) throws IOException {
        if(dataAccess != null)
            return;

        dataAccess = dataAccessModule;

        updateDepartmentChoiceBox();
        departmentChoiceBox.setValue(RPSDepartment.ALL);
    }

    /**
     * fills the department ChoiceBox with Department values
     */
    private void updateDepartmentChoiceBox() throws IOException {
        ObservableList<RPSDepartment> departmentList = FXCollections.observableArrayList(dataAccess.getDataAccessProperty().get().getRPSDepartmentSet());
        departmentList.add(RPSDepartment.ALL);
        FXCollections.sort(departmentList);
        departmentChoiceBox.setItems(departmentList);
    }

    /**
     * set the gui elements in state of logged out
     * and waiting for logging in.
     * Unbinds progressBar and messageLabel
     * and sets cursor to default.
     */
    private void setGUILoginState() {
        //enable login fields
        usernameTextField.setDisable(false);
        passwordPasswordField.setDisable(false);
        loginButton.setDisable(false);
        loginButton.setDefaultButton(true);
        loginButton.setText("Login");

        // disable load fields
        departmentChoiceBox.setDisable(true);
        dateDatePicker.setDisable(true);
        daysSlider.setDisable(true);
        deleteFutureCheckBox.setDisable(true);
        loadingButton.setDisable(true);
        loadingButton.setDefaultButton(false);

        // set cursor to default
        MainWindowController.setGlobalCursor(Cursor.DEFAULT);

        // reset progressBar and messageLabel
        bindProgressBar(null, null);
        showMessage("Bitte einloggen");
    }

    /**
     * set the gui elements in state of logged in
     * and waiting for loading.
     * Unbinds progressBar and messageLabel
     * and sets cursor to default.
     */
    private void setGUILoadState() {
        // disable login fields
        usernameTextField.setDisable(true);
        passwordPasswordField.setDisable(true);
        loginButton.setDisable(false);
        loginButton.setDefaultButton(false);
        loginButton.setText("Logout");

        // enable load fields
        departmentChoiceBox.setDisable(false);
        dateDatePicker.setDisable(false);
        daysSlider.setDisable(false);
        deleteFutureCheckBox.setDisable(false);
        loadingButton.setDisable(false);
        loadingButton.setDefaultButton(true);

        // set cursor to default
        MainWindowController.setGlobalCursor(Cursor.DEFAULT);

        // reset progressBar and messageLabel
        bindProgressBar(null, null);
        showMessage("");
    }

    /**
     * binds the given properties to loadingMessageLabel and loadingProgressBar
     * or unbinds them if null is given.
     * @param message property for loadingMessageLabel
     * @param progress property for loadingProgressBar
     */
    private void bindProgressBar(ReadOnlyStringProperty message, ReadOnlyDoubleProperty progress) {
        // unbinds properties
        loadingMessageLabel.textProperty().unbind();
        loadingProgressBar.progressProperty().unbind();

        // bind properties if not null
        if(message != null)
            loadingMessageLabel.textProperty().bind(message);
        if(message != null)
            loadingProgressBar.progressProperty().bind(progress);
    }

    /**
     * Function is called on click on login button.
     * If already logged in, it will perform an logout.
     * Otherwise a task to login to the website will be started.
     * The task will be bind to the messageLabel and progressBar.
     * On successful login the departments will be parsed and added to the database.
     * Moreover the gui will update to enable loading entries.
     * @param actionEvent fired action event
     */
    @FXML private void loginAction(ActionEvent actionEvent) {
        //check if an other login is already running
        if(loginTask != null && loginTask.isRunning())
            return;

        // check if already logged in.
        if(manager != null) {
            manager = null; // logout
            setGUILoginState(); // refresh gui
            return;
        }

        loginTask = new LoginTask(usernameTextField.getText(), passwordPasswordField.getText(), dataAccess);
        loginTask.setOnSucceeded(event -> {
            LOGGER.log(Level.FINE, "login in task has end SUCCESSFUL");

            try {
                this.manager = loginTask.getValue();
                setGUILoadState();
                updateDepartmentChoiceBox();
                loadingProgressBar.setProgress(1d);
                MainWindowController.doAppGuiUpdate();

            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        });

        loginTask.setOnFailed((event) -> {
            Throwable t = event.getSource().getException();

            if(t instanceof IOException) {
                // thrown in case of connection problems
                showMessage("Es kann keine Verbindung hergestellt werden.");
            } else if(t instanceof RPSWebParserException) {
                // thrown in case of parsing problems or wrong login data.
                showMessage("Login fehlgeschlagen. Überprüfe die Daten.");
            } else {
                showMessage("Fehler beim Einloggen.");
            }

            LOGGER.log(Level.SEVERE, "exception in login task", t);
            loadingProgressBar.setProgress(0d);
        });
        loginTask.setOnCancelled(loginTask.getOnFailed());

        preFillUserName = usernameTextField.getText();

        // prepare gui for starting task to login
        bindProgressBar(loginTask.messageProperty(), loginTask.progressProperty());
        loginButton.setDisable(true);
        MainWindowController.setGlobalCursor(Cursor.WAIT);

        new Thread(loginTask).start();//start task
    }

    /**
     * Function is called on click to the load button.
     * If the values from the gui are valid a background task
     * will do the call to the website and parse the response.
     * In case of an successful load the gui will be refreshed.
     * @param actionEvent fired action event
     */
    @FXML private void loadAction(ActionEvent actionEvent) {

        //check if already loading
        if(loadingTask != null && loadingTask.isRunning())
            return;

        if(departmentChoiceBox.getValue() == null)
            return;

        Set<RPSDepartment> depSet = new HashSet<>();
        depSet.add(departmentChoiceBox.getValue());
        LoadingTask.LoadingConfiguration config = new LoadingTask.LoadingConfiguration(
                dateDatePicker.getValue(), (int) daysSlider.getValue(), depSet, deleteFutureCheckBox.isSelected());
        loadingTask = new LoadingTask(config, dataAccess, manager);

        loadingTask.setOnSucceeded(event -> {
            LOGGER.log(Level.FINE, "loading task has end: {0}", event.getSource().getValue());
            setGUILoadState();
            try {

                loadingProgressBar.setProgress(1d);
                MainWindowController.doAppReCalculation();
                MainWindowController.doAppGuiUpdate();
                showMessage("Daten erfolgreich geladen.");

            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        });

        loadingTask.setOnFailed((event) -> {
            Throwable t = event.getSource().getException();
            if(t instanceof IOException) {
                // thrown in case of connection problems
                showMessage("Es kann keine Verbindung hergestellt werden.");
            } else if(t instanceof RPSWebParserException) {
                // thrown in case of parsing problems or wrong login data.
                showMessage("Fehler beim lesen der Daten.");
            } else {
                showMessage("Fehler beim Laden der Daten.");
            }

            LOGGER.log(Level.SEVERE, "exception in login task", t);
            loadingProgressBar.setProgress(0d);
        });

        // prepare gui for starting task to login
        bindProgressBar(loadingTask.messageProperty(), loadingTask.progressProperty());
        loadingButton.setDisable(true);
        MainWindowController.setGlobalCursor(Cursor.WAIT);

        new Thread(loadingTask).start();  // start background task
    }



    /**
     * shows message in the message label of the loading window for given milliseconds<br>
     * if millis <= 0 message label will not disapear
     * @param msg
     * @param millis
     */
    private void showMessage(String msg, double millis) {
        loadingMessageLabel.setText(msg);
        loadingMessageLabel.setVisible(true);

        LOGGER.log(Level.INFO, "show message: {0}", msg);

        //in case of parameter >= label will not be reseted
        if(millis > 0) {
            new Timeline(new KeyFrame(
                    Duration.millis(millis), (event) -> {
                //reset text and make label invisible
                loadingMessageLabel.setText("");
                loadingMessageLabel.setVisible(false);
            })).playFromStart();
        }
    }
    private void showMessage(String msg) {showMessage(msg, 0);}
}