package at.foorada.rk.rps.gui.dialog.overview;

import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.storage.db.DataBase;
import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.EscKeyClosingEventHandler;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessInterface;
import at.foorada.rk.rps.gui.MainWindowController;
import at.foorada.rk.rps.gui.dialog.loading.LoadingController;
import at.foorada.rk.rps.parser.RPSWebParserManager;
import java.io.IOException;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.stage.Stage;

public class OverviewController implements Initializable {
	private static final Logger LOGGER = Logger.getLogger(OverviewController.class.getName());
	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

	public TextArea overviewTextArea;
	public ProgressBar overviewProgressBar;
	public Label overviewMessageLabel;

	private Stage thisStage;
	private DataAccessModule dataAccess;

	private ParsePropertyTask overviewTask = null;

	/**
	 * binds the given properties to loadingMessageLabel and loadingProgressBar
	 * or unbinds them if null is given.
	 * @param message property for loadingMessageLabel
	 * @param progress property for loadingProgressBar
	 */
	private void bindProgressBar(ReadOnlyStringProperty message, ReadOnlyDoubleProperty progress) {
		// unbinds properties
		overviewMessageLabel.textProperty().unbind();
		overviewProgressBar.progressProperty().unbind();

		// bind properties if not null
		if(message != null)
			overviewMessageLabel.textProperty().bind(message);
		if(message != null)
			overviewProgressBar.progressProperty().bind(progress);
	}


	public static OverviewController loadWindow() throws IOException {

		final FXMLLoader loadingSceneLoader = new FXMLLoader(
				OverviewController.class.getResource("overview.fxml")
		);

		Parent page = loadingSceneLoader.load();
		Scene scene = new Scene(page);
		//handler for ESC-Key -> close window (fire close event)
		scene.addEventHandler(KeyEvent.KEY_PRESSED, new EscKeyClosingEventHandler());

		Stage overviewWindow = new Stage();
		overviewWindow.setTitle("Eintragübersicht");
		overviewWindow.setScene(scene);
		overviewWindow.show();

		OverviewController controller = loadingSceneLoader.getController();
		controller.thisStage = overviewWindow;
		return controller;
	}


	public Stage getStage() {
		return thisStage;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		overviewTextArea.setEditable(false);

	}

	/**
	 * do call this method only once in object lifetime
	 * sets the data access module and do calculation work
	 * @param dataAccessModule
	 */
	public void initDataAccessModule(DataAccessModule dataAccessModule) {
		if(dataAccess != null)
			return;
		dataAccess = dataAccessModule;

		calculateAndDisplayOverview();
	}

	private void calculateAndDisplayOverview() {

		//check if calculation is already running
		if(overviewTask != null && overviewTask.isRunning())
			return;

		overviewTask = new ParsePropertyTask(dataAccess);
		overviewTask.setOnSucceeded(event -> {
			List<RPSWebParserManager.RPSParseProperty> list = overviewTask.getValue();

			if(list != null) {
				LOGGER.log(Level.INFO, "task to calculate overview has end; rows: {0}", list.size());
				StringBuilder bldr = new StringBuilder();

				for(RPSWebParserManager.RPSParseProperty prop : list) {
					bldr.append(prop.toString());
					bldr.append(DataBase.LINE_SEPERATOR);
				}
				overviewTextArea.setText(bldr.toString());
			} else {
				//should never happen
				LOGGER.log(Level.WARNING, "error in calculating overview");
				overviewTextArea.setText("Fehler beim berechnen der Daten");
			}

			MainWindowController.setGlobalCursor(Cursor.DEFAULT);
			bindProgressBar(null, null);
		});

		overviewTask.setOnFailed((event) -> {

			MainWindowController.setGlobalCursor(Cursor.DEFAULT);
			bindProgressBar(null, null);

			LOGGER.log(Level.WARNING, "error in calculating overview", event.getSource().getException());
			overviewTextArea.setText("Fehler beim berechnen der Daten");
		});
		overviewTask.setOnCancelled(overviewTask.getOnFailed());


		overviewTextArea.setText("");
		MainWindowController.setGlobalCursor(Cursor.WAIT);
		bindProgressBar(overviewTask.messageProperty(), overviewTask.progressProperty());

		new Thread(overviewTask).start();
	}

	@FXML private void keyPressedAction(KeyEvent event) {

		if(event.getCode().equals(KeyCode.F5)) {
			calculateAndDisplayOverview();
		}
	}

	private static class ParsePropertyTask extends Task<List<RPSWebParserManager.RPSParseProperty>> {

		private final DataAccessModule dataAccess;

		public ParsePropertyTask(DataAccessModule dataAccess) {
			this.dataAccess = dataAccess;
		}

		@Override
		protected List<RPSWebParserManager.RPSParseProperty> call() throws IOException {

			LOGGER.fine("task to calculate overview has been started");

			List<RPSWebParserManager.RPSParseProperty> parsedList = new ArrayList<>();
			RPSWebParserManager.RPSParseProperty currentProperty = null;
			Calendar lastCal = null;

			Set<RPSDepartment> depList = new HashSet<>(dataAccess.getDataAccessProperty().get().getRPSDepartmentSet());
			depList.add(RPSDepartment.UNKNOWN);
			long steps = depList.size();
			long cnt = 0;

			for (RPSDepartment department : depList) {
				updateMessage("Berechnungen für " + department.getDescription());
				updateProgress(cnt++, steps);
				try {
					List<RPSEntry> list = DataAccessInterface.filterRPSEntryListByDepartment(
							dataAccess.getDataAccessProperty().get().getRPSEntrySet(),department);
					list.sort(Comparator.comparing(RPSEntry::getStart));

					for (RPSEntry entry : list) {
						if (currentProperty == null) {
							currentProperty = new RPSWebParserManager.RPSParseProperty((Calendar) entry.getStart().clone(), 1, department);
							lastCal = (Calendar) entry.getStart().clone();
						} else {
							Calendar start = entry.getStart();
							if (!(lastCal.get(Calendar.YEAR) == start.get(Calendar.YEAR) &&
									lastCal.get(Calendar.MONTH) == start.get(Calendar.MONTH) &&
									lastCal.get(Calendar.DAY_OF_MONTH) == start.get(Calendar.DAY_OF_MONTH))) {

								lastCal.add(Calendar.DAY_OF_MONTH, 1);
								if (lastCal.get(Calendar.YEAR) == start.get(Calendar.YEAR) &&
										lastCal.get(Calendar.MONTH) == start.get(Calendar.MONTH) &&
										lastCal.get(Calendar.DAY_OF_MONTH) == start.get(Calendar.DAY_OF_MONTH)) {
									currentProperty.days++;
								} else {
									parsedList.add(currentProperty);
									lastCal = (Calendar) start.clone();
									currentProperty = new RPSWebParserManager.RPSParseProperty(start, 1, department);
								}
							}
						}
					}
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, "could not load entries for department: {0}", department.getDescription());
				}
			}

			if (currentProperty != null)
				parsedList.add(currentProperty);

			updateMessage("Ubersicht berechnet");
			updateProgress(1, 1);

			return parsedList;
		}
	}
}