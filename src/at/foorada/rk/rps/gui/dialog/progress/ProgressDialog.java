package at.foorada.rk.rps.gui.dialog.progress;

import java.io.IOException;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class ProgressDialog {

    private static final Logger LOGGER = Logger.getLogger(ProgressDialog.class.getName());

    private ProgressDialogController controller;

    public ProgressDialog(String title, String message) throws IOException {

        controller = ProgressDialogController.loadWindow();
        controller.setContent(title, message);

        LOGGER.finest("progress stage opened");
        controller.getStage().setOnCloseRequest(event -> {
            LOGGER.finest("progress stage closed");
        });
    }

    public void close() {
        if(controller != null) {
            controller.getStage().fireEvent(
                    new WindowEvent(
                            controller.getStage(),
                            WindowEvent.WINDOW_CLOSE_REQUEST
                    )
            );
        }
    }

    public void bindProgressProperty(ReadOnlyDoubleProperty progress, ReadOnlyStringProperty message) {
        controller.bindProgressProperty(progress, message);
    }

    public void progressFinished() {
        progressFinished("");
    }

    public void progressFinished(String message) {
        controller.progressFinished(message);
    }
}