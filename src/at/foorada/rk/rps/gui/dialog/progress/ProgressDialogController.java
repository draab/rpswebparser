package at.foorada.rk.rps.gui.dialog.progress;

import at.foorada.rk.rps.EscKeyClosingEventHandler;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * controller for progress.fxml view
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public class ProgressDialogController implements Initializable {
    public Label dialogTitleLabel;
    public Label dialogMessageLabel;
    public ProgressBar processingBar;
    public Label progressMessageLabel;
    public Button progressDoneButton;

    private ProgressDialog caller;
    private Stage thisStage;

    public static ProgressDialogController loadWindow() throws IOException {

        final FXMLLoader progressSceneLoader = new FXMLLoader(
                ProgressDialogController.class.getResource("progress.fxml")
        );

        Parent page = progressSceneLoader.load();
        Scene scene = new Scene(page);
        //handler for ESC-Key -> close window (fire close event)
//        scene.addEventHandler(KeyEvent.KEY_PRESSED, new EscKeyClosingEventHandler());

        Stage progressWindow = new Stage();
        progressWindow.setTitle("Einträge laden");
        progressWindow.setScene(scene);
        progressWindow.show();

        ProgressDialogController controller = progressSceneLoader.getController();
        controller.thisStage = progressWindow;
        return controller;
    }


    public Stage getStage() {
        return thisStage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        progressDoneButton.setDisable(true);
    }

    public void setContent(String title, String message) {

        dialogTitleLabel.setText(title);
        dialogMessageLabel.setText(message);
    }



    public void bindProgressProperty(ReadOnlyDoubleProperty prop, ReadOnlyStringProperty message) {
        if(message != null) {
            progressMessageLabel.textProperty().unbind();
            progressMessageLabel.textProperty().bind(message);
        } else {
            progressMessageLabel.textProperty().unbind();
            progressMessageLabel.setText("");
        }

        if(prop != null) {
            processingBar.progressProperty().unbind();
            processingBar.progressProperty().bind(prop);
        } else {
            processingBar.progressProperty().unbind();
//            processingBar.indeterminateProperty()
        }
    }

    public void progressFinished(String message) {
        progressMessageLabel.textProperty().unbind();
        processingBar.progressProperty().unbind();
        progressMessageLabel.setText(message);
        processingBar.setProgress(1d);

        this.progressDoneButton.setDisable(false);
    }
}