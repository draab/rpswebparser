package at.foorada.rk.rps.gui.dialog.remove;

import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.EscKeyClosingEventHandler;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessInterface;
import at.foorada.rk.rps.gui.MainWindowController;
import at.foorada.rk.rps.gui.dialog.loading.LoadingController;
import at.foorada.rk.rps.parser.RPSWebParserManager;
import java.io.IOException;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyDoubleProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.stage.Stage;

public class RemoveController implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(RemoveController.class.getName());

    public DatePicker dateDatePicker;
    public ChoiceBox<RPSDepartment> departmentChoiceBox;
    public ChoiceBox<RPSEntry.Type> typeChoiceBox;
    public Slider daysSlider;
    public Label daysLabel;

    public ProgressBar removingProgressBar;
    public Label removingMessageLabel;
    public Button removeButton;

    private DataAccessModule dataAccess;

    private RemovingTask removeTask = null;
    private Stage thisStage;


    public static RemoveController loadWindow() throws IOException {

        final FXMLLoader removeGuiLoader = new FXMLLoader(
                RemoveController.class.getResource("remove.fxml")
        );

        Parent page = removeGuiLoader.load();
        Scene scene = new Scene(page);
        //handler for ESC-Key -> close window (fire close event)
        scene.addEventHandler(KeyEvent.KEY_PRESSED, new EscKeyClosingEventHandler());

        Stage loadingWindow = new Stage();
        loadingWindow.setTitle("Einträge löschen");
        loadingWindow.setScene(scene);
        loadingWindow.show();

        RemoveController controller = removeGuiLoader.getController();
        controller.thisStage = loadingWindow;
        return controller;
    }

    public Stage getStage() {
        return thisStage;
    }

    /**
     * sets up the Scene and initialize the nodes which are independent from the data access object
     * @param location
     * @param resources
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        typeChoiceBox.setItems(FXCollections.observableArrayList(RPSEntry.Type.values()));
        typeChoiceBox.setValue(RPSEntry.Type.ALL_TYPES);

        daysSlider.setMin(1);
        daysSlider.setMax(31);
        daysSlider.setBlockIncrement(1);
        daysSlider.setValue(1);
        daysSlider.setMinorTickCount(0);
        daysSlider.setMajorTickUnit(1);
        daysSlider.snapToTicksProperty().set(true);
        daysLabel.textProperty().bind(Bindings.format("%.0f",daysSlider.valueProperty()));

        dateDatePicker.setValue(LocalDate.now());
    }

    /**
     * call only once in object life time
     * sets the DataAccessModule and initialize the department choice box with the values
     * @param dataAccessModule
     * @throws IOException
     */
    public void initDataAccessModule(DataAccessModule dataAccessModule) throws IOException {
        if(dataAccess != null)
            return;
        dataAccess = dataAccessModule;

        departmentChoiceBox.getItems().clear();
        departmentChoiceBox.getItems().addAll(dataAccess.getDataAccessProperty().get().getRPSDepartmentSet());
        departmentChoiceBox.getItems().add(0, RPSDepartment.ALL);
        departmentChoiceBox.getItems().add(RPSDepartment.UNKNOWN);

        departmentChoiceBox.setValue(RPSDepartment.ALL);
    }

    /**
     * binds the given properties to loadingMessageLabel and loadingProgressBar
     * or unbinds them if null is given.
     * @param message property for loadingMessageLabel
     * @param progress property for loadingProgressBar
     */
    private void bindProgressBar(ReadOnlyStringProperty message, ReadOnlyDoubleProperty progress) {
        // unbinds properties
        removingMessageLabel.textProperty().unbind();
        removingProgressBar.progressProperty().unbind();

        // bind properties if not null
        if(message != null)
            removingMessageLabel.textProperty().bind(message);
        if(message != null)
            removingProgressBar.progressProperty().bind(progress);
    }

    @FXML private void deleteAction(ActionEvent actionEvent) {

        if(removeTask != null && removeTask.isRunning())
            return;

        removeTask = new RemovingTask(dataAccess, dateDatePicker.getValue(), departmentChoiceBox.getValue(),
                typeChoiceBox.getValue(), (int)daysSlider.getValue());

        removeTask.setOnSucceeded(event -> {
            LOGGER.log(Level.INFO, "removing task has end: {0}", removeTask.getValue());

            resetGuiAfterTask(true);

            removingMessageLabel.setText("Erfolgreich gelöscht.");

            MainWindowController.doAppGuiUpdate();
        });

        removeTask.setOnFailed((event) -> {
            resetGuiAfterTask(false);

            removingMessageLabel.setText("Fehler beim löschen der Daten");

        });

        removeButton.setDisable(true);
        MainWindowController.setGlobalCursor(Cursor.WAIT);
        bindProgressBar(removeTask.messageProperty(), removeTask.progressProperty());

        new Thread(removeTask).start();
    }

    private void resetGuiAfterTask(boolean succeed) {
        removeButton.setDisable(false);
        MainWindowController.setGlobalCursor(Cursor.DEFAULT);
        bindProgressBar(null, null);
        removingProgressBar.setProgress(succeed ? 1d : 0d);
    }

    private static class RemovingTask extends Task<Object> {

        private final Calendar start;
        private final RPSDepartment department;
        private final RPSEntry.Type type;
        private final int days;

        private final DataAccessModule dataAccess;

        public RemovingTask(DataAccessModule dataAccess, LocalDate startDate, RPSDepartment department, RPSEntry.Type type, int days) {
            start = Calendar.getInstance();
            Date remove = Date.from(startDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
            start.setTime(remove);

            this.dataAccess = dataAccess;
            this.department = department;
            this.type = type;
            this.days = days;
        }

        @Override
        protected Object call() throws IOException {
            LOGGER.fine("removing task has been started");
            int steps = 5;
            int curStep = 0;

            LOGGER.info("check removing input data");
            updateMessage("Eingaben überprüfen ...");
            updateProgress(curStep++, steps);

            LOGGER.log(Level.INFO, "start removing from: {0} days: {1} department: {2} type: {3}", new Object[]{start.getTime().toString(), days, department.getDescription(), type.getDescription()});
            updateMessage("Einträge entfernen ...");
            updateProgress(curStep++, steps);

            dataAccess.getDataAccessProperty().get().removeEntriesByTypeDepartment(start, days, type, department);

            MainWindowController.doAppReCalculation();

            return true;
        }
    }
}