/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.foorada.rk.rps.gui.dialog.settings;

import at.foorada.rk.rps.EscKeyClosingEventHandler;
import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.storage.importExport.ExportTask;
import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessInterface;
import at.foorada.rk.rps.data.QuickLoadConfiguration;
import at.foorada.rk.rps.data.storage.importExport.ExportJson;
import at.foorada.rk.rps.data.storage.importExport.ImportExportModule;
import at.foorada.rk.rps.data.storage.importExport.ImportJson;
import at.foorada.rk.rps.data.storage.importExport.ImportTask;
import at.foorada.rk.rps.gui.MainWindowController;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import at.foorada.rk.rps.gui.dialog.loading.LoadingController;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Duration;

/**
 *
 * @author danie
 */
public class SettingController implements Initializable {

	private static final Logger LOGGER = Logger.getLogger(SettingController.class.getName());

	private static final double DEFAULT_MESSAGE_DURATION = 5000;

	//<editor-fold defaultstate="collapsed" desc="fxml quick load">
	@FXML private TextField qlMailTextField;
	@FXML private TextField qlOffsetTextField;
	@FXML private TextField qlDaysTextField;
	@FXML private ListView<RPSDepartment> qlAvailableDepartmentList;
	@FXML private ListView<RPSDepartment> qlSelectedDepartmentList;
	@FXML private Button qlAddBtn;
	@FXML private Button qlRemoveBtn;
	//</editor-fold>

	//<editor-fold defaultstate="collapsed" desc="fxml import export">
	@FXML private ChoiceBox<ImportExportModule> listExportChoiceBox;
	@FXML private Button listExportBtn;
	@FXML private Button selectImportFileBtn;
	@FXML private Label fileImportLbl;
	@FXML private ChoiceBox<ImportExportModule> listImportChoiceBox;
	@FXML private RadioButton importReplaceRdBtn;
	@FXML private RadioButton importAddRdBtn;
	@FXML private Button listImportBtn;
	//</editor-fold>

	@FXML private Label messageLbl;
	@FXML private Button saveSettingBtn;
	@FXML private Button exitSettingBtn;

	private Stage thisStage;
	private DataAccessModule dataAccess;

	private ObservableList<RPSDepartment> availableDepartments;
	private ObservableList<RPSDepartment> selectedDepartments;

	private File importFile = null;
	private Task<ExportTask.ExportTaskReturn> exportTask = null;
	private Task<ImportTask.ImportTaskReturn> importTask = null;

	public static SettingController loadWindow() throws IOException {

		final FXMLLoader settingSceneLoader = new FXMLLoader(
				SettingController.class.getResource("setting.fxml")
		);

		Parent page = settingSceneLoader.load();
		Scene scene = new Scene(page);
		//handler for ESC-Key -> close window (fire close event)
		scene.addEventHandler(KeyEvent.KEY_PRESSED, new EscKeyClosingEventHandler());

		Stage settingWindow = new Stage();
		settingWindow.setTitle("Einstellungen");
		settingWindow.setScene(scene);
		settingWindow.show();

		SettingController controller = settingSceneLoader.getController();
		controller.thisStage = settingWindow;
		return controller;
	}

	public Stage getStage() {
		return thisStage;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {


		//disable add button if no item in available list is selected
		qlAvailableDepartmentList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		qlAvailableDepartmentList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			qlAddBtn.setDisable(newValue == null);
		});

		//disable remove button if no item in selected list is selected
		qlSelectedDepartmentList.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
		qlSelectedDepartmentList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
			qlRemoveBtn.setDisable(newValue == null);
		});

		//initImportExportTab
		listExportChoiceBox.getItems().add(ImportExportModule.SELECTED_MEMBER);
		listExportChoiceBox.getItems().add(ImportExportModule.MEMBER_TO_MEMBER);
		listImportChoiceBox.setItems(listExportChoiceBox.getItems());
	}

	public void initDataAccessModule(DataAccessModule dataAccessModule) throws IOException {
		if(dataAccess != null)
			return;
		dataAccess = dataAccessModule;
		initQuickLoadTab();
	}

	/**
	 * init everything in the quick load tab
	 * @throws IOException
	 */
	private void initQuickLoadTab() throws IOException {
		QuickLoadConfiguration config = dataAccess.getDataAccessProperty().get().getQuickLoadConfiguration();

		// init text fields with values from config object (read from storage)
		qlMailTextField.setText(config.mail);
		qlOffsetTextField.setText(String.valueOf(config.offset));
		qlDaysTextField.setText(String.valueOf(config.days));

		initLists(config.departments);
	}


	/**
	 * initialization of list for the quick load tab
	 * @param departments
	 * @throws IOException
	 */
	private void initLists(Collection departments) throws IOException {

		//read selected departments from configuration
		selectedDepartments = FXCollections.observableArrayList(departments);

		FXCollections.sort(selectedDepartments);
		qlSelectedDepartmentList.setItems(selectedDepartments);
		qlRemoveBtn.setDisable(qlSelectedDepartmentList.getSelectionModel().getSelectedItem() == null);

		//get all departments and remove the selected ones
		availableDepartments = FXCollections.observableArrayList(dataAccess.getDataAccessProperty().get().getRPSDepartmentSet());
		availableDepartments.removeAll(selectedDepartments);	//remove selected departments
		FXCollections.sort(availableDepartments);
		qlAvailableDepartmentList.setItems(availableDepartments);
		qlAddBtn.setDisable(qlAvailableDepartmentList.getSelectionModel().getSelectedItem() == null);
	}

	/**
	 * shows message in the message label of the settings window for given milliseconds<br>
	 * if millis <= 0 message label will not disapear
	 * @param msg
	 * @param millis
	 */
	private void showMessage(String msg, double millis) {
		messageLbl.setText(msg);
		messageLbl.setVisible(true);

		LOGGER.log(Level.INFO, "show message: {0}", msg);

		//in case of parameter >= label will not be reseted
		if(millis > 0) {
			new Timeline(new KeyFrame(
					Duration.millis(millis), (event) -> {
				//reset text and make label invisible
				messageLbl.setText("");
				messageLbl.setVisible(false);
			})).playFromStart();
		}
	}


	//<editor-fold defaultstate="collapsed" desc="quick load view actions (controller)">

	@FXML private void qlAddAction(ActionEvent event) {
		RPSDepartment dept = qlAvailableDepartmentList.getSelectionModel().getSelectedItem();
		if(dept != null) {
			selectedDepartments.add(dept);
			availableDepartments.remove(dept);
		}
	}

	@FXML private void qlRemoveAction(ActionEvent event) {
		RPSDepartment dept = qlSelectedDepartmentList.getSelectionModel().getSelectedItem();
		if(dept != null) {
			selectedDepartments.remove(dept);
			availableDepartments.add(dept);
		}
	}

	@FXML private void posIntKeeper(KeyEvent event) {
		String input = event.getCharacter();
		if (Pattern.matches("[^0-9]", input))
			event.consume();
	}

	//</editor-fold>


	//<editor-fold defaultstate="collapsed" desc="import export view actions (controller)">

	@FXML private void listExportBtnAction(ActionEvent actionEvent) {
	
	/* do not allow two export task at the same time;
	 * stop execution method if a export task is allready running
	*/
		if(exportTask != null && exportTask.isRunning())
			return;

		ImportExportModule selectedModule = listExportChoiceBox.getSelectionModel().getSelectedItem();

		//check if a module is selected otherwise do nothing
		if(selectedModule == null) {
			showMessage("Bitte ein Module auswählen", DEFAULT_MESSAGE_DURATION);
			return;
		}

		FileChooser chooser = new FileChooser();
		chooser.setTitle("Liste exportieren");
		chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("RK Member List", "*.rkml"));

		File exportFile = chooser.showSaveDialog(listExportBtn.getScene().getWindow());

		if(exportFile==null) {
			LOGGER.finest("open dialog closed");
			return;
		}

		//set up export task
		setUpExportTask(exportFile, selectedModule);

		//disable button for export until thread has finished;
		listExportBtn.setDisable(true);

		//start thread for writing export
		new Thread(exportTask).start();
	}

	/**
	 * sets up a new Export task<br>
	 * <b>method does not check if a task is currently running</b>
	 * @param exportFile
	 * @param selectedModule
	 */
	private void setUpExportTask(File exportFile, ImportExportModule selectedModule) {

		exportTask = new ExportTask(new ExportJson(exportFile, dataAccess, selectedModule));

		exportTask.setOnSucceeded((event) -> {
			listExportBtn.setDisable(false);

			switch(exportTask.getValue()) {
				case SUCCEEDED:
					showMessage("Liste erfolgreich exportiert.", DEFAULT_MESSAGE_DURATION);
					break;
				default:
					showMessage("Fehler beim exportieren.", DEFAULT_MESSAGE_DURATION);
			}
		});
		exportTask.setOnFailed((event) -> {
			listExportBtn.setDisable(false);
			showMessage("Fehler beim exportieren.", DEFAULT_MESSAGE_DURATION);
			LOGGER.log(Level.SEVERE, "exception while exporting", event.getSource().getException());
		});
		exportTask.setOnCancelled(exportTask.getOnFailed());
	}

	/**
	 * opens a dialog to choose exported file for import.
	 * A check for ability to read is done also.
	 * @param actionEvent
	 */
	@FXML private void selectImportFileBtnAction(ActionEvent actionEvent) {
		/**
		 * do not change the import file while a import task is running
		 */
		if(exportTask != null && exportTask.isRunning()) {
			LOGGER.finest("btn for import file is pressed while import task is running");
			showMessage("Ein Importierung ist noch am laufen.", DEFAULT_MESSAGE_DURATION);
			return;
		}

		/**
		 * open file chooser to select exported file for import
		 */
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Liste importieren");
		chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("RK Member List", "*.rkml"));

		File newFile = chooser.showOpenDialog(listImportBtn.getScene().getWindow());

		//check if we got new file and we are able to read from it
		//TODO check is done twice, also in init of ImportJson.init()
		if(newFile != null && newFile.exists() && newFile.isFile() && newFile.canRead()) {
			importFile = newFile;
			fileImportLbl.setText(importFile.getAbsolutePath());
			LOGGER.fine("selected file is ok for read");
		}
	}

	/**
	 * import the selected file
	 * @param actionEvent
	 */
	@FXML private void listImportBtnAction(ActionEvent actionEvent) {

		/**
		 * do not allow two import task at the same time;
		 * stop execution method if a import task is already running
		 */
		if(exportTask != null && exportTask.isRunning()) {
			LOGGER.finest("btn for importing is pressed while import task is already running");
			showMessage("Ein Importierung ist noch am laufen.", DEFAULT_MESSAGE_DURATION);
			return;
		}

		//check if file is choosen
		if(importFile == null) {
			LOGGER.finest("not ready for importing, file not choosen");
			showMessage("Bitte eine Datei auswählen", DEFAULT_MESSAGE_DURATION);
			return;
		}

		//check if a module is selected otherwise do nothing
		ImportExportModule selectedModule = listImportChoiceBox.getSelectionModel().getSelectedItem();
		if(selectedModule == null) {
			LOGGER.finest("not ready for importing, module not choosen");
			showMessage("Bitte ein Module auswählen", DEFAULT_MESSAGE_DURATION);
			return;
		}

		//if its a list import check if we add or replace the current list
		if(selectedModule.isListImportExport())
			selectedModule.setAddList(importAddRdBtn.isSelected());

		//set up import task to be called
		setUpImportTask(selectedModule);

		//disable buttons for import until thread has finished;
		selectImportFileBtn.setDisable(true);
		listImportBtn.setDisable(true);

		//start thread for reading and storing import
		new Thread(importTask).start();
	}

	/**
	 * sets up a new Export task<br>
	 * <b>method does not check if a task is currently running</b>
	 * @param selectedModule
	 */
	private void setUpImportTask(ImportExportModule selectedModule) {
		importTask = new ImportTask(new ImportJson(importFile, dataAccess, selectedModule));


		importTask.setOnSucceeded((event) -> {
			selectImportFileBtn.setDisable(false);
			listImportBtn.setDisable(false);

			switch(importTask.getValue()) {
				case SUCCEEDED:
					MainWindowController.doAppGuiUpdate();
					showMessage("Liste erfolgreich importiert.", DEFAULT_MESSAGE_DURATION);
					break;
				default:
					showMessage("Fehler beim importieren.", DEFAULT_MESSAGE_DURATION);
			}
		});
		importTask.setOnFailed((event) -> {
			selectImportFileBtn.setDisable(false);
			listImportBtn.setDisable(false);

			showMessage("Fehler beim importieren.", DEFAULT_MESSAGE_DURATION);
			LOGGER.log(Level.SEVERE, "exception while importing", event.getSource().getException());
		});
		importTask.setOnCancelled(importTask.getOnFailed());
	}

	//</editor-fold>


	@FXML private void exitSettingAction(ActionEvent actionEvent) {

		//todo dialog -- nothing will be saved

		Window stage = exitSettingBtn.getScene().getWindow();
		stage.fireEvent(new WindowEvent(stage,WindowEvent.WINDOW_CLOSE_REQUEST));
	}

	@FXML private void saveSettingAction(ActionEvent event) {
		QuickLoadConfiguration qlConfig = new QuickLoadConfiguration();

		qlConfig.mail = qlMailTextField.getText();
		qlConfig.offset = Integer.parseInt(qlOffsetTextField.getText());
		qlConfig.days = Integer.parseInt(qlDaysTextField.getText());
		qlConfig.departments.clear();
		qlConfig.departments.addAll(selectedDepartments);

		dataAccess.getDataAccessProperty().get().setQuickLoadConfiguration(qlConfig);

		showMessage("Einstellungen gespeichert", DEFAULT_MESSAGE_DURATION);
	}

}
