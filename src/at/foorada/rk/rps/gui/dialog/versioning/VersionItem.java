package at.foorada.rk.rps.gui.dialog.versioning;

import at.foorada.versioning.data.ReleasedVersion;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 *
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 15.03.2017.
 */
public class VersionItem extends GridPane {

    private static final SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @FXML private Label descLabel;
    @FXML private Label linkLabel;
    @FXML private Label releaseDateLabel;
    @FXML private Label versionLabel;

    public VersionItem() {

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("versionItem.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void setVersion(ReleasedVersion version) {
        this.versionLabel.setText(version.getAppVersion());
        this.descLabel.setText(version.getMessage());
        this.linkLabel.setText(version.getLink());
        this.releaseDateLabel.setText(timeFormat.format(version.getReleaseDate().getTime()));
    }
}
