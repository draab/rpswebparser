package at.foorada.rk.rps.gui.dialog.versioning;

import at.foorada.rk.rps.EscKeyClosingEventHandler;
import at.foorada.rk.rps.Main;
import at.foorada.system.DesktopHelper;
import at.foorada.versioning.data.ReleasedVersion;
import at.foorada.versioning.net.VersionUtils;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 15.03.2017.
 */
public class VersioningController implements Initializable{

    private static final Logger LOGGER = Logger.getLogger(VersioningController.class.getName());
    private Stage controllerStage;

    @FXML private Label currentVersionLabel;
    @FXML private Label mailLabel;

    @FXML private ListView<VersionItem> versionListView;
    @FXML private Label noVersionLabel;

    public static VersioningController loadWindow() throws IOException {

        final FXMLLoader versioningSceneLoader = new FXMLLoader(
                VersioningController.class.getResource("versioning.fxml")
        );

        Parent page = versioningSceneLoader.load();
        Scene scene = new Scene(page, 400, 400);
        scene.addEventHandler(KeyEvent.KEY_PRESSED, new EscKeyClosingEventHandler());

        Stage versioningWindow = new Stage();
        versioningWindow.setScene(scene);
        versioningWindow.show();

        VersioningController htmlInfoController = versioningSceneLoader.getController();
        htmlInfoController.controllerStage = versioningWindow;
        return htmlInfoController;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        currentVersionLabel.setText(Main.APP_VERSION.getAppVersion());
        mailLabel.setOnMouseClicked(event -> {
                    try {
                        DesktopHelper.openMailClient(mailLabel.getText(),
                                Main.APP_VERSION.getAppName() + " - " + Main.APP_VERSION.getAppVersion());
                    } catch (Exception e) {
                        LOGGER.log(Level.WARNING, "not able to open mail client");
                    }
                }
        );
        resetVersionList();
    }

    public Stage getStage() {
        return controllerStage;
    }

    private void resetVersionList() {
        noVersionLabel.setManaged(true);
        noVersionLabel.setVisible(true);
        versionListView.setManaged(false);
        versionListView.setVisible(false);
    }

    public void setContentList(List<ReleasedVersion> list) {

        if(list == null || list.isEmpty()) {
            resetVersionList();
        } else {
            noVersionLabel.setManaged(false);
            noVersionLabel.setVisible(false);
            versionListView.setManaged(true);
            versionListView.setVisible(true);

            for (ReleasedVersion rv : list) {
                VersionItem vi = new VersionItem();
                vi.setVersion(rv);
                vi.setOnMouseClicked(event -> {
                    if (event.getClickCount() >= 2) {
                        try {
                            DesktopHelper.openWebpage(rv.getLink());
                        } catch (Exception e) {
                            LOGGER.log(Level.WARNING, "not able to open browser");
                        }
                    }
                });
                versionListView.getItems().add(vi);
            }
        }
    }












    public static class VersionCheckerTask extends Task<List<ReleasedVersion>> {

        @Override
        protected List<ReleasedVersion> call() throws Exception {

            List<ReleasedVersion> list = VersionUtils.getVersionsFromServer("http://versioning.draab.at/check.php", Main.APP_VERSION.getAppName());

            return VersionUtils.checkForNewVersionInList(list, Main.APP_VERSION);
        }
    }
}
