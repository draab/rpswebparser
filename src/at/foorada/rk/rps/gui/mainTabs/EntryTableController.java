package at.foorada.rk.rps.gui.mainTabs;

import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.data.RPSMember;
import at.foorada.rk.rps.MyComboBoxKeyEventHandler;
import at.foorada.rk.rps.gui.MainWindowController;
import java.io.IOException;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.net.URL;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EntryTableController implements MainTabController {

    private static final Logger LOGGER = Logger.getLogger(EntryTableController.class.getName());
    private static final Level FILTER_LOG_LEVEL = Level.FINEST;

    public static boolean showPrefix = true;

    public CheckBox autoFilterCheckbox;

    public ComboBox<RPSDepartment> departmentFilterComboBox;
    public ComboBox<String> ankerFilterComboBox;
    public ComboBox<RPSEntry.Type> typeFilterComboBox;
    public ComboBox<RPSMember> memberFilterComboBox;
    public DatePicker startFilterStartDatePicker;
    public DatePicker startFilterEndDatePicker;

    public TableView<RPSEntryTableEntry> entryTableView;
    public TableColumn<RPSEntryTableEntry, String> mainTableDepartmentColumn;
    public TableColumn<RPSEntryTableEntry, String> mainTableTypeColumn;
    public TableColumn<RPSEntryTableEntry, Date> mainTableStartColumn;
    public TableColumn<RPSEntryTableEntry, Date> mainTableEndColumn;
    public TableColumn<RPSEntryTableEntry, String> mainTableAnkerColumn;
    public TableColumn<RPSEntryTableEntry, RPSMember> mainTableMember1Column;
    public TableColumn<RPSEntryTableEntry, RPSMember> mainTableMember2Column;
    public TableColumn<RPSEntryTableEntry, RPSMember> mainTableMember3Column;
    public TableColumn<RPSEntryTableEntry, String> mainTableFurtherMemberColumn;
    public TableColumn<RPSEntryTableEntry, String> mainTableCommentColumn;
    public CheckBox showPrefixCheckBox;

    private MainWindowController parent;
    private DataAccessModule dataAccess;
    private List<RPSEntryTableEntry> entryTableItemList;

    private ObservableList<RPSDepartment> departmentList;
    private ObservableList<String> ankerList;
    private ObservableList<RPSMember> memberList;
    private long filteredHours;

    //    public String toeBoardText = "";
    public StringProperty mainMessageStringProperty;

    /**
     * calculation for table<br>
     * invoke filter, etc.<br>
     * <b> no gui updates done here, call doGuiUpdate()</b>
     * there is only one list stored, the filtered one. This saves memory but we have to two more calculations
     * @throws java.io.IOException
     */
    @Override
    public void doCalculation() throws IOException {

        /**
         * loading of data from dataAccess here
         */
        departmentList = FXCollections.observableArrayList(dataAccess.getDataAccessProperty().get().getRPSDepartmentSet());
        FXCollections.sort(departmentList);

        ankerList = FXCollections.observableArrayList(dataAccess.getDataAccessProperty().get().getAnkerSet());
        FXCollections.sort(ankerList);

        memberList = FXCollections.observableArrayList(dataAccess.getDataAccessProperty().get().getRPSMemberSet());
        FXCollections.sort(memberList);

        setUpTableView();

    }

    private void setUpTableView() throws IOException {

        RPSDepartment department = departmentFilterComboBox.getValue();
        String anker = ankerFilterComboBox.getValue();
        RPSEntry.Type type = typeFilterComboBox.getValue();
        RPSMember member = memberFilterComboBox.getValue();
        LocalDate startDate = startFilterStartDatePicker.getValue();
        LocalDate endDate = startFilterEndDatePicker.getValue();

        entryTableItemList.clear();

        long hourCnt = 0;

        /*
        * add entries to table, check filter first
        * calculate hours in table
         */
        for(RPSEntry e : dataAccess.getDataAccessProperty().get().getRPSEntrySet()) {

            //filter for MEMBER
            if (member != null && !e.hasMember(member)) {
                    continue;
            }

            //filter for START
            if (startDate != null) {
                Date entryEnd = e.getEnd().getTime();
                LocalDate entryEndLocalDate = entryEnd.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                if (entryEndLocalDate.compareTo(startDate) < 0) {
                    continue;
                }
            }

            //filter for END
            if (endDate != null) {
                Date entryStart = e.getStart().getTime();
                LocalDate entryStartLocalDate = entryStart.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                if (entryStartLocalDate.compareTo(endDate) > 0) {
                    continue;
                }
            }

            //filter for DEPARTMENT
            if (department != null && !department.equals(RPSDepartment.ALL) &&
                    !e.getDepartment().equals(department)) {
                continue;
            }

            //filter for ANKER
            if (anker != null && !(e.hasAnker() && e.getAnker().equals(anker))) {
                continue;
            }

            // filter for TYPE
            if (type != null && !type.equals(RPSEntry.Type.ALL_TYPES) && !e.getType().equals(type)) {
                continue;
            }

            entryTableItemList.add(new RPSEntryTableEntry(e));

            //calculate all listed hours
            long millis = e.getShiftDurationInMillis();
            millis /= 3600000; // in hours
            hourCnt += millis;
        }

        filteredHours = hourCnt;

        if (department != null || anker != null|| type != null|| startDate != null|| endDate != null|| member != null)
            LOGGER.log(FILTER_LOG_LEVEL,"Filter : -----------------------");

        if (department != null) LOGGER.log(FILTER_LOG_LEVEL, String.format("department filter: %s", department.toString()));
        if (anker != null) LOGGER.log(FILTER_LOG_LEVEL, String.format("anker filter: %s", anker));
        if (type != null) LOGGER.log(FILTER_LOG_LEVEL, String.format("type filter: %s", type.toString()));
        if (startDate != null) LOGGER.log(FILTER_LOG_LEVEL, String.format("start filter: %s", startDate.toString()));
        if (endDate != null) LOGGER.log(FILTER_LOG_LEVEL, String.format("end filter: %s", endDate.toString()));
        if (member != null) LOGGER.log(FILTER_LOG_LEVEL, String.format("member filter: %s", member.getFullName()));

    }

    /**
     * the filters and the table will be updated<br>
     * <b>only the gui, nothing will be calculated</b><br>
     * call doCalculation() for this before
     * @throws IOException
     */
    @Override
    public void doGuiUpdate() {
        updateFilterGui();
        updateTableGui();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        showPrefix = showPrefixCheckBox.isSelected();
        mainMessageStringProperty = new SimpleStringProperty("");

        memberFilterComboBox.setOnKeyReleased(new MyComboBoxKeyEventHandler());

        setTableViewColumns();

//        entryTableView.setItems(FXCollections.observableArrayList());
        entryTableItemList = FXCollections.observableArrayList();
        mainTableStartColumn.setSortType(TableColumn.SortType.DESCENDING);
        entryTableView.getSortOrder().add(mainTableStartColumn);
    }

    @Override
    public void setParentOfTab(MainWindowController mainWindowController) {

        this.parent = mainWindowController;
    }

    @Override
    public void initDataAccessModule(DataAccessModule dataAccessModule) throws IOException {
        if(dataAccess != null)
            return;
        dataAccess = dataAccessModule;

        doCalculation();
        updateFilterGui();
        updateTableGui();
    }

    /**
     * set up columns for entry table<br>
     * the entry table contains a list of RPSEntryTableEntry<br>
     * coloring, click events, etc. are set here
     */
    private void setTableViewColumns() {

        /*
         * the table view contains a list of TableRow and the factory generates the TableRow
         * a TableRow is represented by a RPSEntryTableEntry
         * set own row factory to add a color and a tooltip to the rows
         */
        entryTableView.setRowFactory(param1 -> new TableRow<RPSEntryTableEntry>(){
            @Override
            protected void updateItem(RPSEntryTableEntry item, boolean empty) {
                super.updateItem(item, empty);

                //remove all css classes for default condition
                getStyleClass().removeAll("futureNightShiftTableRow", "futureDayShiftTableRow", "nightShiftTableRow");

                Tooltip tooltip = null;

                if(item != null && !empty) {
                    Calendar datetime = item.entry.getStart();

                    //create new tooltip with loading date info
                    tooltip = new Tooltip("geladen: "+RPSEntryTableEntry.DATE_TIME_FORMAT_NO_SEC.format(item.entry.getAddedTime().getTime()));

                    if(item.entry.isFutureEntry()) {
                        //set css class for night color (day and night shift)
                        if (datetime.get(Calendar.HOUR_OF_DAY) >= 18)
                            getStyleClass().add("futureNightShiftTableRow");
                        else
                            getStyleClass().add("futureDayShiftTableRow");
                    } else if (datetime.get(Calendar.HOUR_OF_DAY) >= 18) {
                        //set css class for night shift; day shift is the standard version, no class nesseccesary
                        getStyleClass().add("nightShiftTableRow");
                    }
                }

                /**
                 * if row is empty and item == null also tooltip == null so remove the old
                 * tooltip from the row with calling setTooltip(null)
                 */
                setTooltip(tooltip);
            }
        });

        /**
         * define the cell factories for the column
         * The cell value factory produces the content for the specific value
         * For the PropertyValueFactory the representing row object (RPSEntryTableEntry)
         * has to hold getter (and setter) for the given property string name
         */
        mainTableDepartmentColumn.setCellValueFactory(new PropertyValueFactory("department"));
        mainTableTypeColumn.setCellValueFactory(new PropertyValueFactory("type"));

        /**
         * for the start and end time the return of the RPSEntryTableEntry getter
         * is a Date object so we set a CellFactory to define in which way the
         * given parameter hast to be shown
         */
        mainTableStartColumn.setCellValueFactory(new PropertyValueFactory("startTime"));
        mainTableStartColumn.setCellFactory(param -> new TableCell<RPSEntryTableEntry, Date>() {
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                if (item != null && !empty) {
                    setText(RPSEntryTableEntry.DATE_TIME_FORMAT_NO_SEC_WITH_NAME.format(item));
                } else
                    setText("");    //necessary for removing artefacts
            }
        });
        mainTableEndColumn.setCellValueFactory(new PropertyValueFactory("endTime"));
        mainTableEndColumn.setCellFactory(param -> new TableCell<RPSEntryTableEntry, Date>() {
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);

                if (item != null && !empty)
                    setText(RPSEntryTableEntry.DATE_TIME_FORMAT_NO_SEC.format(item));
                else
                    setText("");    //necessary for removing artefacts
            }
        });
        mainTableAnkerColumn.setCellValueFactory(new PropertyValueFactory("anker"));

        /**
         * Set up a Callback for the CellValueFactory of the member columns
         * is is necessary to be able to decide if the name will be shown
         * with or without prefix,
         * for the functionality to change the filter by double clicking on an
         * member; and its necessary to give the callback to for the
         * to more factories (avoiding multiple inner class with the same code)
         */
        Callback<TableColumn<RPSEntryTableEntry, RPSMember>, TableCell<RPSEntryTableEntry, RPSMember>> memberCellFactory =
                (TableColumn<RPSEntryTableEntry, RPSMember> param) -> {
                    TableCell<RPSEntryTableEntry, RPSMember> cell = new TableCell<RPSEntryTableEntry, RPSMember>() {
                        @Override
                        protected void updateItem(RPSMember item, boolean empty) {
                            super.updateItem(item, empty);

                            if (item != null && !empty) {
                                if (EntryTableController.showPrefix)    //value of checkbox for showing prefix of member
                                    setText(item.getFullName());
                                else
                                    setText(item.getName());


                            } else
                            /**
                             * necessary for cleaning cells
                             * otherwise it is possible to have old fragments in cells
                             */
                                setText("");
                        }
                    };

                    /**
                     * double click on member will filter change the member filter for the clicking member
                     */
                    cell.addEventFilter(MouseEvent.MOUSE_CLICKED, event -> {
                        if ((event.getClickCount() >= 2) && event.getSource() instanceof TableCell) {
                            try {
                                TableCell<RPSEntryTableEntry, RPSMember> clickedCell = (TableCell<RPSEntryTableEntry, RPSMember>) event.getSource();
                                memberFilterComboBox.setValue(clickedCell.getItem());
                                doCalculation();
                                updateTableGui();
                            } catch (IOException ex) {
                                LOGGER.log(Level.SEVERE, "", ex);
                            }
                        }
                    });

                    return cell;
                };

        mainTableMember1Column.setCellValueFactory(new PropertyValueFactory("member1"));
        mainTableMember1Column.setCellFactory(memberCellFactory);
        mainTableMember2Column.setCellValueFactory(new PropertyValueFactory("member2"));
        mainTableMember2Column.setCellFactory(memberCellFactory);
        mainTableMember3Column.setCellValueFactory(new PropertyValueFactory("member3"));
        mainTableMember3Column.setCellFactory(memberCellFactory);

        /**
         * the further member and the comment column are simply strings
         */
        mainTableFurtherMemberColumn.setCellValueFactory(new PropertyValueFactory("furtherMember"));
        mainTableCommentColumn.setCellValueFactory(new PropertyValueFactory("comment"));
    }

    /**
     * resets simply the filter gui to nothing chosen<br>
     * <b>the table will not calculated and is not refreshed</b>
     */
    public void resetFilterGui() {
        departmentFilterComboBox.setValue(null);
        memberFilterComboBox.setValue(null);
        startFilterStartDatePicker.setValue(null);
        startFilterEndDatePicker.setValue(null);
        ankerFilterComboBox.setValue(null);
        typeFilterComboBox.setValue(null);
    }

    /**
     * resets the filter gui, so nothing is chosen<br>
     * and recalculates the table items
     * @throws IOException
     */
    public void resetFilterAndCalculate() throws IOException {

        departmentFilterComboBox.setValue(null);
        memberFilterComboBox.setValue(null);
        startFilterStartDatePicker.setValue(null);
        startFilterEndDatePicker.setValue(null);
        ankerFilterComboBox.setValue(null);
        typeFilterComboBox.setValue(null);

        doCalculation();
    }

    /**
     * update filter gui<br>
     * necessary in case of updating entries with new members, new ankers, etc.
     * or deleting something like that.<br>
     * the data will be hold new from dataAccess and and filled to the gui
     * @throws IOException
     */
    private void updateFilterGui() {

        departmentFilterComboBox.getItems().clear();
        departmentFilterComboBox.getItems().add(RPSDepartment.ALL);
        departmentFilterComboBox.getItems().addAll(departmentList);
        departmentFilterComboBox.getItems().add(RPSDepartment.UNKNOWN);

        List<RPSEntry.Type> typeList = Arrays.asList(RPSEntry.Type.values());
        typeList.sort((o1, o2) -> o1.getDescription().compareTo(o2.getDescription()));
        typeFilterComboBox.setItems(FXCollections.observableArrayList(typeList));

        ankerFilterComboBox.setItems(ankerList);

        memberFilterComboBox.setItems(memberList);
    }

    /**
     * update gui table with filtered items<br>
     * <b>but the items will not be calculated in this method</b>
     */
    private void updateTableGui() {
        TableColumn sortColumn = null;
        TableColumn.SortType sortType = null;

        if (entryTableView.getSortOrder().size() > 0) {
            sortColumn = entryTableView.getSortOrder().get(0);
            sortType = sortColumn.getSortType();
        }

        entryTableView.setItems(FXCollections.observableArrayList(entryTableItemList));

        if(sortColumn !=null && sortType != null) {
            entryTableView.getSortOrder().add(sortColumn);
            sortColumn.setSortType(sortType);
            sortColumn.setSortable(true); // This performs a sort
        }

//        toeBoardText = "angezeigte Einträge: " + this.filterdTableItems.size();
        mainMessageStringProperty.set(String.format("angezeigte Einträge: %d; geleistete Stunden: %d", entryTableView.getItems().size(), filteredHours));
    }

    /**
     * nothing to do on closing at the moment
     */
    public void close() {}

    /**
     * fxml controller method for changing the prefix checkbox
     * @param actionEvent
     */
    @FXML private void prefixCheckBoxAction(ActionEvent actionEvent) {
        showPrefix = showPrefixCheckBox.isSelected();
        entryTableView.refresh();   //lets invoke the factories again
    }

    /**
     * fxml controller method for filter gui on auto update table will be
     * updated after each gui filter change without clicking the button
     * @param actionEvent
     */
    @FXML private void filterChangedAction(ActionEvent actionEvent) {
        if(autoFilterCheckbox.isSelected()) {
            try {
                doCalculation();
                // dont call updateFilterGui() (the filter settings will removed otherwise)
                updateTableGui();
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
        }
    }
    /**
     * fxml controller method for set filter button
     * @param actionEvent
     */
    @FXML private void filterChangedButtonAction(ActionEvent actionEvent) {
        try {
            doCalculation();
            // dont call updateFilterGui() (the filter settings will removed otherwise)
            updateTableGui();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
    }

    /**
     * fxml controller method for reset filter button
     * @param actionEvent
     */
    @FXML private void resetFilterAction(ActionEvent actionEvent) {
        try {
            resetFilterAndCalculate();
            updateTableGui();
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "error in reseting filter", ex);
        }
    }
}