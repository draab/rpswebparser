/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package at.foorada.rk.rps.gui.mainTabs;
import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.gui.MainWindowController;
import javafx.fxml.Initializable;

import java.io.IOException;

/**
 *
 * @author Daniel Raab <daniel.raab.at@gmail.com>
 */
public interface MainTabController extends Initializable {

    /**
     *
     * @param mainWindowController
     */
    void setParentOfTab(MainWindowController mainWindowController);

    /**
     * do call this only one in controller object life time
     * init the controller with the dataAccessModule and do calculation and gui updating work
     * @param dataAccessModule
     */
    void initDataAccessModule(DataAccessModule dataAccessModule) throws IOException;


    /**
     * only the calculations for the content of the tab is done in this method,
     * the method <b>must not</b> change or refresh gui elements<br>
     * maybe this call should be done in own thread
     * @throws IOException
     */
    void doCalculation() throws IOException;

    /**
     * only updates to the gui are done here<br>
     * no calculations are done here
     */
    void doGuiUpdate();
}
