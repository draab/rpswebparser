package at.foorada.rk.rps.gui.mainTabs;

import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.storage.db.DataBase;
import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.data.RPSMember;
import at.foorada.rk.rps.MyComboBoxKeyEventHandler;
import at.foorada.rk.rps.gui.MainWindowController;
import java.io.IOException;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class MemberTableController implements  MainTabController {

    private static final Logger LOGGER = Logger.getLogger(MemberTableController.class.getName());

    public static final SimpleDateFormat FORMAT_FOR_COLUMN = new SimpleDateFormat("EEE dd.MM");
    public static final SimpleDateFormat FORMAT_TIME_NO_SEC = new SimpleDateFormat("HH:mm");

    public ComboBox<RPSMember> removeMemberComboBox;
    public DatePicker startRangeDatePicker;
    public DatePicker endRangeDatePicker;
    public ComboBox<RPSMember> addMemberComboBox;
    public TableView<RPSMemberTableEntry> memberTableView;
    public Button removeMemberButton;
    public Button addMemberButton;
    public CheckBox showPrefixCheckBox;
    public CheckBox showTypeCheckbox;

    private MainWindowController parent;
    private DataAccessModule dataAccess;

    private ObservableList<RPSMemberTableEntry> tableEntryList;
    private ObservableList<RPSMember> removeMemberList;
    private ObservableList<RPSMember> addMemberList;

    private Calendar rangeStart;
    private Calendar rangeEnd;

    private RPSEntry selectedRPSEntry;
    private RPSMemberTableEntry selectedRPSMemberTableEntry;

    public StringProperty mainMessageStringProperty;

    /**
     *
     *	    TODO do calculation for entry swap in own function in the doCalcutation() method
     *		 prepare the table fully in the doCalculation() method and
     *		 only displaying is done in the gui update method or in callback class for
     *		 cell factory
     *
     * callback object for cell factory of date column
     */
    private Callback<TableColumn<RPSMemberTableEntry, List<RPSEntry>>, TableCell<RPSMemberTableEntry, List<RPSEntry>>> timeCellFactory =
            new Callback<TableColumn<RPSMemberTableEntry, List<RPSEntry>>, TableCell<RPSMemberTableEntry, List<RPSEntry>>>() {
                @Override
                public TableCell call(TableColumn param) {

                    /**
                     * create cell for given table column<br>
                     * in the updateItem method of the cell, the calculations for the visuals are done
                     * (weekend, nightShift, dayShift, selected, possible)
                     */
                    EntryTableCell cell = new EntryTableCell<RPSMemberTableEntry, List<RPSEntry>>(){

                        @Override
                        protected void updateItem(List<RPSEntry> item, boolean empty) {
                            super.updateItem(item, empty);

                            //initial remove all css classes
                            getStyleClass().removeAll("weekendTableCell", "nightShiftTableCell", "dayShiftTableCell", "selectedCell", "possibleCell");
                            Tooltip tooltip = null;

                            //check if cell is in the DateTableColumn (should be everytime)
                            if(param instanceof DateTableColumn) {
                                DateTableColumn column = (DateTableColumn)param;
                                Calendar date = column.date;

                                /**
                                 * color cell if its a Saturday or Sunday
                                 */
                                int dayOfWeek = date.get(Calendar.DAY_OF_WEEK);
                                if(dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
                                    getStyleClass().add("weekendTableCell");
                                }

                                /**
                                 * if cell is not empty, iterate RPSEntry list and search entry for current date (column)
                                 */
                                if(item != null && !empty) {
                                    for(RPSEntry currentEntry :item) {

                                        /**
                                         * if entry found prepare cell for display
                                         */
                                        Calendar start = currentEntry.getStart();
                                        if(start.get(Calendar.DAY_OF_MONTH) == date.get(Calendar.DAY_OF_MONTH) &&
                                                start.get(Calendar.MONTH) == date.get(Calendar.MONTH) &&
                                                start.get(Calendar.YEAR) == date.get(Calendar.YEAR)) {
                                            thisRPSEntryObject = currentEntry;

                                            //create tooltip Text
                                            StringBuilder toolTipBldr = new StringBuilder();
                                            toolTipBldr.append(currentEntry.getDepartment().getDescription()); toolTipBldr.append("\n");
                                            toolTipBldr.append(currentEntry.getType().getDescription()); toolTipBldr.append("\n");
                                            toolTipBldr.append(RPSEntryTableEntry.DATE_TIME_FORMAT_NO_SEC.format(currentEntry.getStart().getTime()));
                                            toolTipBldr.append(" - ");
                                            toolTipBldr.append(RPSEntryTableEntry.DATE_TIME_FORMAT_NO_SEC.format(currentEntry.getEnd().getTime()));
                                            toolTipBldr.append("\n");
                                            for(RPSMember rpsM : currentEntry.getMemberList()) {
                                                toolTipBldr.append(rpsM.getFullName());
                                                toolTipBldr.append("\n");
                                            }
                                            toolTipBldr.append("geladen: ");
                                            toolTipBldr.append(RPSEntryTableEntry.DATE_TIME_FORMAT_NO_SEC.format(currentEntry.getAddedTime().getTime()));

                                            //tooltip with builded string
                                            tooltip = new Tooltip(toolTipBldr.toString());
                                            setTooltip(tooltip);

                                            //wrap text in cell
                                            setWrapText(true);

                                            //build content text
                                            StringBuilder cellContent = new StringBuilder();
                                            //if checkbox for type is checked append shortcut from type
                                            if(showTypeCheckbox.isSelected()) {
                                                cellContent.append(currentEntry.getType().getShurtcut()).append(" ");
                                            }
                                            //show start and end time
                                            cellContent.append(FORMAT_TIME_NO_SEC.format(start.getTime()));
                                            cellContent.append(DataBase.LINE_SEPERATOR).append("-");
                                            cellContent.append(FORMAT_TIME_NO_SEC.format(currentEntry.getEnd().getTime()));
                                            setText(cellContent.toString());

                                            /**
                                             *
                                             *	    TODO move this code to "doCalculation()" method,
                                             *		here should simply displaying work be done
                                             *
                                             * if a selected entry is set check if the current entry is the selected one
                                             * or this entry could be a possible entry for changing
                                             */
                                            if(selectedRPSEntry != null) {
                                                if(selectedRPSEntry.equals(currentEntry)) {
                                                    getStyleClass().add("selectedCell");
                                                    return;
                                                } else if(!currentEntry.hasMember(selectedRPSMemberTableEntry.member) && currentEntry.getType().isTypeForSwapping()) {

                                                    //check if member of selected entry can take current entry
                                                    if (!RPSEntry.isRPSEntryOccupied(currentEntry, selectedRPSMemberTableEntry.entryList, 10*60, selectedRPSEntry)) {

                                                        //check if current member of row can take the entry of selectedEntry
                                                        if(!RPSEntry.isRPSEntryOccupied(selectedRPSEntry, item, 10*60, currentEntry)) {
                                                            getStyleClass().add("possibleCell");
                                                            return;
                                                        }
                                                    }
                                                }
                                            }
                                            if (start.get(Calendar.HOUR_OF_DAY) >= 18)
                                                getStyleClass().add("nightShiftTableCell");
                                            else
                                                getStyleClass().add("dayShiftTableCell");

                                            return;
                                        }
                                    }
                                }
                            }
                            setTooltip(tooltip);
                            setText("");
                            setCursor(Cursor.DEFAULT);
                        }
                    };

                    /**
                     * add event handler mouse clicks on sell:
                     * - click with control down sets the selected RPSEntry for the given cell
                     * - double click sets filter in entry table tab and switch to tab
                     */
                    cell.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
                        TableCell c = (TableCell) event.getSource();
                        Object entry = c.getTableRow().getItem();
                        if (c.getText().length() > 0) {
                            if (param instanceof DateTableColumn && entry != null && entry instanceof RPSMemberTableEntry) {
                                LOGGER.finest("Date Cell clicked");
                                LocalDate day = LocalDate.from(((DateTableColumn) param).date.getTime().toInstant().atZone(ZoneId.systemDefault()));

                                /**
                                 * control is pressed, set (or clear if the rps entry is already selected)
                                 * selected rps entry and refresh the table
                                 */
                                if (event.isControlDown()) {
                                    if (cell.thisRPSEntryObject.equals(selectedRPSEntry)) {
                                        selectedRPSEntry = null;
                                        selectedRPSMemberTableEntry = null;
                                    } else {
                                        selectedRPSEntry = cell.thisRPSEntryObject;
                                        selectedRPSMemberTableEntry = (RPSMemberTableEntry) entry;
                                    }
                                    memberTableView.refresh();

                                    /**
                                     * double click: set filter of entry table tab to for this entry
                                     * and switch to tab
                                     */
                                } else if(event.getClickCount() > 1) {
                                    try {
                                        parent.entryTableGridPaneController.resetFilterGui();
                                        parent.entryTableGridPaneController.startFilterStartDatePicker.setValue(day);
                                        parent.entryTableGridPaneController.startFilterEndDatePicker.setValue(day);
                                        parent.entryTableGridPaneController.memberFilterComboBox.setValue(((RPSMemberTableEntry) entry).member);
                                        parent.entryTableGridPaneController.doCalculation();
                                        parent.entryTableGridPaneController.doGuiUpdate();
                                    } catch(IOException ioe) {
                                        LOGGER.log(Level.SEVERE, null, ioe);
                                    }

                                    parent.switchToTab(MainWindowController.MainWindowTab.ENTRY_LIST);
                                }
                            }
                        }
                    });
                    cell.setAlignment(Pos.CENTER);
                    return cell;
                }
            };

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        /**
         * initiate DatePicker
         */
        rangeStart = Calendar.getInstance();
        rangeEnd = Calendar.getInstance();
        rangeEnd.add(Calendar.DAY_OF_MONTH, 13);
        LocalDate startDate = rangeStart.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        startRangeDatePicker.setValue(startDate);
        LocalDate endDate = rangeEnd.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        endRangeDatePicker.setValue(endDate);

        /*
         * initiate add and remove comboBoxes (items are not filled here)
         * MyComboBoxKeyEventHandler is added for typing characters on the combo box
         * And a event Handler for typing enter is added whitch calles button press of the coresponding one
         */
        addMemberComboBox.addEventHandler(KeyEvent.KEY_RELEASED, new MyComboBoxKeyEventHandler());
        removeMemberComboBox.addEventHandler(KeyEvent.KEY_RELEASED, new MyComboBoxKeyEventHandler());
        addMemberComboBox.addEventHandler(KeyEvent.KEY_RELEASED, (event) -> {
            if(event.getCode().equals(KeyCode.ENTER)) {
                addMemberButtonAction(null);
            }
        });
        removeMemberComboBox.addEventHandler(KeyEvent.KEY_RELEASED, (event) -> {
            if (event.getCode().equals(KeyCode.ENTER)) {
                removeMemberButtonAction(null);
            }
        });

        //set up messageStringProperty with empty string
        mainMessageStringProperty = new SimpleStringProperty("");

        //initiate default date range
        setTableViewColumns();

        tableEntryList = FXCollections.observableArrayList();
    }

    @Override
    public void setParentOfTab(MainWindowController mainWindowController) {
        parent = mainWindowController;
    }

    @Override
    public void initDataAccessModule(DataAccessModule dataAccessModule) throws IOException {
        if(dataAccess != null)
            return;
        dataAccess = dataAccessModule;

        //calculate rows for table and display these
        doCalculation();
        doGuiUpdate();
    }

    @Override
    public void doCalculation() throws IOException {

        /**
         * load data from dataAccess
         */
        removeMemberList = FXCollections.observableArrayList(dataAccess.getDataAccessProperty().get().getSelectedMemberSet());
        FXCollections.sort(removeMemberList);

        addMemberList = FXCollections.observableArrayList(dataAccess.getDataAccessProperty().get().getRPSMemberSet());
        FXCollections.sort(addMemberList);

        calculateTableEntryList();
    }

    /**
     * calculating table entry list from selected rps member set and the RPSEntry set <br>
     * <b>only use in doCalculation()</b>
     */
    private void calculateTableEntryList() throws IOException {
        tableEntryList.clear();


        for(RPSMember m : removeMemberList) {
            RPSMemberTableEntry tableEntry = new RPSMemberTableEntry(m);
            tableEntry.entryList.addAll(dataAccess.getDataAccessProperty().get().getRPSEntrySet().stream().filter(e -> e.hasMember(m)).collect(Collectors.toList()));
            tableEntryList.add(tableEntry);
        }

        if(!tableEntryList.contains(selectedRPSMemberTableEntry)) {
            selectedRPSEntry = null;
            selectedRPSMemberTableEntry = null;
        }
    }

    @Override
    public void doGuiUpdate() {
        // update combobox for removing and adding members to table
        removeMemberComboBox.setItems(removeMemberList);

        addMemberList.removeAll(removeMemberList);
        addMemberComboBox.setItems(addMemberList);

        addMemberButton.setDisable(addMemberList.isEmpty());
        removeMemberButton.setDisable(removeMemberList.isEmpty());

        memberTableView.setItems(tableEntryList);

        mainMessageStringProperty.set("gelistete Personen: "+removeMemberList.size());
    }

    /**
     * the table columns will be created new from the given date ranges of the
     * date pickers
     */
    private void dateRangeChanged() {
        LocalDate startDate = startRangeDatePicker.getValue();
        LocalDate endDate = endRangeDatePicker.getValue();

        rangeStart.setTime(Date.from(startDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
        rangeEnd.setTime(Date.from(endDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));

        setTableViewColumns();
    }

    /**
     * the method sets the columns for the table view<br>
     * a column represents a date, so call this methods if the date range 
     * to display has changed 
     */
    private void setTableViewColumns() {
        //remove all columns first
        memberTableView.getColumns().clear();

        /*
         * create first column, the name of the member
         */
        TableColumn<RPSMemberTableEntry, RPSMember> nameColumn = new TableColumn<>("Name");
        nameColumn.setPrefWidth(200);
        nameColumn.setCellValueFactory(new PropertyValueFactory("member"));

        nameColumn.setCellFactory(new Callback<TableColumn<RPSMemberTableEntry, RPSMember>, TableCell<RPSMemberTableEntry, RPSMember>>() {
            @Override
            public TableCell<RPSMemberTableEntry, RPSMember> call(TableColumn param) {
                TableCell<RPSMemberTableEntry, RPSMember> cell = new TableCell<RPSMemberTableEntry, RPSMember>() {

                    @Override
                    protected void updateItem(RPSMember item, boolean empty) {
                        Object entry = getTableRow().getItem();
                        if (!empty && entry instanceof RPSMemberTableEntry) {
                            if(showPrefixCheckBox.isSelected())
                                setText(item.getFullName());
                            else
                                setText(item.getName());
                        } else {
                            setText("");
                            setCursor(Cursor.DEFAULT);
                        }
                    }

                };

                /**
                 * on double click change filter for entry table and switch to entry table tab
                 */
                cell.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
                    if(event.getClickCount() >= 2 && event.getSource() instanceof TableCell) {
                        try {
                            parent.entryTableGridPaneController.resetFilterGui();
                            parent.entryTableGridPaneController.memberFilterComboBox.setValue(((RPSMemberTableEntry)((TableCell<RPSMemberTableEntry, String>) event.getSource()).getTableRow().getItem()).member);
                            parent.entryTableGridPaneController.doCalculation();
                            parent.entryTableGridPaneController.doGuiUpdate();
                        } catch (IOException ioe) {
                            LOGGER.log(Level.SEVERE, null, ioe);
                        }

                        parent.switchToTab(MainWindowController.MainWindowTab.ENTRY_LIST);
                    }
                });
                cell.setWrapText(true);
                cell.setAlignment(Pos.CENTER_LEFT);
                return cell;
            }
        });
        memberTableView.getColumns().add(nameColumn);

        /**
         * create columns for every date
         */
        Calendar iteratorDate = (Calendar) rangeStart.clone();
        while(iteratorDate.compareTo(rangeEnd) <= 0) {

            DateTableColumn<RPSMemberTableEntry, List<RPSEntry>> dateColumn = new DateTableColumn<>((Calendar) iteratorDate.clone());
            dateColumn.setSortable(false); // do not allow sorting for a date column
            dateColumn.setPrefWidth(60);
            dateColumn.setCellValueFactory(new PropertyValueFactory<>("entryList"));
            dateColumn.setCellFactory(timeCellFactory);
            memberTableView.getColumns().add(dateColumn);

            iteratorDate.add(Calendar.DAY_OF_MONTH, 1);
        }
    }

    public void close() {}

    //<editor-fold defaultstate="collapsed" desc="FXML methods">

    /**
     * fxml controller method for add member button action
     * @param actionEvent
     */
    @FXML private void addMemberButtonAction(ActionEvent actionEvent) {
        RPSMember member = addMemberComboBox.getValue();
        if(member == null) {
            return;
        }

        try {
            dataAccess.getDataAccessProperty().get().addSelectedMember(member);
            LOGGER.log(Level.FINE, "{0} added to selected members", member.getFullName());

            doCalculation();
            doGuiUpdate();
        } catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, "errorin adding selected member", ioe);
        }
    }

    /**
     * fxml controller method for remove member button action
     * @param actionEvent
     */
    @FXML private void removeMemberButtonAction(ActionEvent actionEvent) {
        RPSMember member = removeMemberComboBox.getValue();
        if(member == null) {
            return;
        }
        try {
            dataAccess.getDataAccessProperty().get().removeSelectedMember(member);
            LOGGER.log(Level.FINE, "{0} removed from selected members", member.getFullName());

            doCalculation();
            doGuiUpdate();
        } catch(IOException ioe) {
            LOGGER.log(Level.SEVERE, "exception in removing selected member", ioe);
        }
    }

    /**
     * fxml controller method for changing data range
     * @param actionEvent
     */
    @FXML private void dateRangeChangedAction(ActionEvent actionEvent) {
        if(startRangeDatePicker.getValue().compareTo(endRangeDatePicker.getValue()) > 0)
            endRangeDatePicker.setValue(startRangeDatePicker.getValue());

        dateRangeChanged();
    }

    /**
     * fxml controller method for change of prefix check box
     * @param actionEvent
     */
    @FXML private void prefixCheckBoxAction(ActionEvent actionEvent) {
        memberTableView.refresh();
    }

    /**
     * fxml controller method for change of show type check box
     * @param actionEvent
     */
    @FXML private void showTypeCheckBoxAction(ActionEvent actionEvent) {
        memberTableView.refresh();
    }

    //</editor-fold>

    /**
     * wrapper class for TableColumn to store corresponding date in the column
     * @param <S>
     * @param <T> 
     */
    private class DateTableColumn<S,T> extends TableColumn<S,T> {

        private Calendar date;

        DateTableColumn(Calendar date) {
            super(FORMAT_FOR_COLUMN.format(date.getTime()));
            this.date = date;
        }
    }

    /**
     * wrapper class for TableCell to store corresponding RPSEntry in the cell
     * @param <T>
     * @param <S> 
     */
    private class EntryTableCell<T, S> extends TableCell<T, S> {
        public RPSEntry thisRPSEntryObject = null;
    }
}