package at.foorada.rk.rps.gui.mainTabs;

import at.foorada.rk.rps.MyComboBoxKeyEventHandler;
import at.foorada.rk.rps.data.DataAccessModule;
import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.data.RPSMember;
import at.foorada.rk.rps.gui.MainWindowController;
import java.io.IOException;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class MemberToMemberTableController implements MainTabController {

	private static final Logger LOGGER = Logger.getLogger(MemberToMemberTableController.class.getName());

	public static final SimpleDateFormat FORMAT_FOR_COLUMN = new SimpleDateFormat("EEE dd.MM");
	public static final SimpleDateFormat FORMAT_TIME_NO_SEC = new SimpleDateFormat("HH:mm");

	public ComboBox<RPSMember> removeMemberComboBox;
	public ComboBox<RPSMember> addMemberComboBox;
	public TableView memberTableView;
	public Button removeMemberButton;
	public Button addMemberButton;

	private MainWindowController parent;
	private DataAccessModule dataAccess;

	private ObservableList<RPSMemberTableEntry> tableEntryList;
	private ObservableList<RPSMember> removeSelectedMemberList;
	private ObservableList<RPSMember> addSelectedMemberList;

	public StringProperty mainMessageStringProperty;

	/**
	 *
	 *	    TODO do calculation for the display in the doCalculation method
	 *
	 * callback object for cell factory of memberColumn
	 */
	private Callback<TableColumn<RPSMemberTableEntry, List<RPSEntry>>, TableCell<RPSMemberTableEntry, List<RPSEntry>>> timeCellFactory =
			new Callback<TableColumn<RPSMemberTableEntry, List<RPSEntry>>, TableCell<RPSMemberTableEntry, List<RPSEntry>>>() {
				@Override
				public TableCell call(TableColumn param) {
					TableCell cell = new TableCell<RPSMemberTableEntry, List<RPSEntry>>(){

						@Override
						protected void updateItem(List<RPSEntry> item, boolean empty) {
							super.updateItem(item, empty);
							getStyleClass().removeAll("sameMemberTableCell");

							//check if cell is in the MemberTableColumn (should be everytime)
							if(param instanceof MemberTableColumn) {
								int entryCounter = 0;
								MemberTableColumn column = (MemberTableColumn)param;
								RPSMember memberColumn = column.member;


								if(item != null && !empty) {
									//count entries for displaying
									for(RPSEntry currentEntry :item) {
										if(currentEntry.hasMember(memberColumn)) entryCounter++;
									}
									if(entryCounter > 0)
										setText(Integer.toString(entryCounter));
									else
										setText("");

									/**
									 * if its the cross cell (same member in column and in row)
									 * add a css class for coloring the cell
									 */
									if(getTableRow().getItem() instanceof RPSMemberTableEntry) {
										RPSMemberTableEntry row = (RPSMemberTableEntry) getTableRow().getItem();
										if(memberColumn.equals(row.getMember())) {
											getStyleClass().add("sameMemberTableCell");
										}
									}
								}  else {
									setText("");
								}
								return;
							}
							setText("");
						}
					};

					cell.setAlignment(Pos.CENTER);
					return cell;
				}
			};

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		addMemberComboBox.addEventHandler(KeyEvent.KEY_RELEASED, new MyComboBoxKeyEventHandler());
		//typing enter has same effect than press button
		addMemberComboBox.addEventHandler(KeyEvent.KEY_RELEASED, (event) -> {
			if(event.getCode().equals(KeyCode.ENTER)) {
				addMemberButtonAction(null);
			}
		});

		removeMemberComboBox.addEventHandler(KeyEvent.KEY_RELEASED, new MyComboBoxKeyEventHandler());
		//typing enter has same effect than press button
		removeMemberComboBox.addEventHandler(KeyEvent.KEY_RELEASED, (event) -> {
			if (event.getCode().equals(KeyCode.ENTER)) {
				removeMemberButtonAction(null);
			}
		});

		mainMessageStringProperty = new SimpleStringProperty("");
	}

	@Override
	public void setParentOfTab(MainWindowController mainWindowController) {
		this.parent = mainWindowController;
	}

	@Override
	public void initDataAccessModule(DataAccessModule dataAccessModule) throws IOException {
		if(dataAccess != null)
			return;
		dataAccess = dataAccessModule;

		doCalculation();
		doGuiUpdate();
	}

	@Override
	public void doCalculation() throws IOException {

		removeSelectedMemberList = FXCollections.observableArrayList(dataAccess.getDataAccessProperty().get().getM2MSet());
		FXCollections.sort(removeSelectedMemberList);

		addSelectedMemberList = FXCollections.observableArrayList(dataAccess.getDataAccessProperty().get().getRPSMemberSet());
		addSelectedMemberList.removeAll(removeSelectedMemberList);
		FXCollections.sort(addSelectedMemberList);

		calculateTableEntryList();
	}

	/**
	 * calculating table entry list from selected rps member set and the RPSEntry set <br>
	 * <b>only use in doCalculation()</b>
	 */
	private void calculateTableEntryList() throws IOException {
		tableEntryList = FXCollections.observableArrayList();

		List<RPSMember> newM2M = new ArrayList<>(dataAccess.getDataAccessProperty().get().getM2MSet());
		Collections.sort(newM2M);
		for(RPSMember m:newM2M) {
			RPSMemberTableEntry tableEntry = new RPSMemberTableEntry(m);
			tableEntry.entryList.addAll(dataAccess.getDataAccessProperty().get().getRPSEntrySet().stream().filter(e -> e.hasMember(m)).collect(Collectors.toList()));
			tableEntryList.add(tableEntry);
		}
	}

	@Override
	public void doGuiUpdate() {

		removeMemberComboBox.setItems(removeSelectedMemberList);
		removeMemberButton.setDisable(removeSelectedMemberList.isEmpty());

		addMemberComboBox.setItems(addSelectedMemberList);
		addMemberButton.setDisable(addSelectedMemberList.isEmpty());

		setTableViewColumns();

		mainMessageStringProperty.set("gelistete Personen: "+removeSelectedMemberList.size());

		memberTableView.setItems(tableEntryList);
	}

	/**
	 * the method sets the columns for the table view<br>
	 * a column represents a date, so call this methods if the date range
	 * to display has changed
	 */
	private void setTableViewColumns() {
		memberTableView.getColumns().clear();

		/**
		 * create tableColumn for first the first column
		 * simply name column width mouse event handler (filter in
		 * entryTableTab on double click)
		 */
		TableColumn<RPSMemberTableEntry, RPSMember> nameColumn = new TableColumn<>("");
		nameColumn.setPrefWidth(200);
		nameColumn.setCellValueFactory(new PropertyValueFactory("member"));
		nameColumn.setCellFactory(new Callback<TableColumn<RPSMemberTableEntry, RPSMember>, TableCell<RPSMemberTableEntry, RPSMember>>() {
			@Override
			public TableCell call(TableColumn param) {
				TableCell cell = new TableCell<RPSMemberTableEntry, RPSMember>() {

					@Override
					protected void updateItem(RPSMember item, boolean empty) {
						Object entry = getTableRow().getItem();
						if (!empty && entry instanceof RPSMemberTableEntry) {
							setText(item.getName());
						} else {
							setText("");
						}
					}

				};

				/**
				 * add a event filter for double click mouse events
				 * change filter on entryTableTab and switch to it
				 */
				cell.addEventFilter(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
					if(event.getClickCount() >= 2 && event.getSource() instanceof TableCell) {
						try {
							parent.entryTableGridPaneController.resetFilterGui();
							parent.entryTableGridPaneController.memberFilterComboBox.setValue(((RPSMemberTableEntry)((TableCell<RPSMemberTableEntry, String>) event.getSource()).getTableRow().getItem()).member);
							parent.entryTableGridPaneController.doCalculation();
							parent.entryTableGridPaneController.doGuiUpdate();
						} catch (IOException ioe) {
							LOGGER.log(Level.SEVERE, null, ioe);
						}

						parent.switchToTab(MainWindowController.MainWindowTab.ENTRY_LIST);
					}
				});
				cell.setWrapText(true);
				cell.setAlignment(Pos.CENTER_LEFT);
				return cell;
			}
		});
		memberTableView.getColumns().add(nameColumn);

		/**
		 * add MemberTableColumns for each Column in the list
		 */
		for (RPSMember mem : removeSelectedMemberList) {

			MemberTableColumn<RPSMemberTableEntry, List<RPSEntry>> memberCol = new MemberTableColumn<>(mem);
			memberCol.setSortable(false);
			memberCol.setPrefWidth(30);
			memberCol.setCellValueFactory(new PropertyValueFactory("entryList"));
			memberCol.setCellFactory(timeCellFactory);
			memberTableView.getColumns().add(memberCol);
		}
	}

	public void close() {
	}

	//<editor-fold defaultstate="collapsed" desc="FXML methods">

	/**
	 * fxml controller method for add member button action
	 * @param actionEvent
	 */
	@FXML private void addMemberButtonAction(ActionEvent actionEvent) {
		RPSMember member = addMemberComboBox.getValue();
		if(member == null) {
			return;
		}

		try {
			dataAccess.getDataAccessProperty().get().addM2M(member);
			LOGGER.log(Level.FINE, "{0} added to selected members", member.getFullName());

			doCalculation();
			doGuiUpdate();
		} catch (IOException ioe) {
			LOGGER.log(Level.SEVERE, "error in adding member for m2m", ioe);
		}
	}

	/**
	 * fxml controller method for remove member button action
	 * @param actionEvent
	 */
	@FXML private void removeMemberButtonAction(ActionEvent actionEvent) {
		RPSMember member = removeMemberComboBox.getValue();
		if(member == null) {
			return;
		}

		try {
			dataAccess.getDataAccessProperty().get().removeM2M(member);
			LOGGER.log(Level.SEVERE, " removed from selected members {0}", member.getFullName());

			doCalculation();
			doGuiUpdate();
		} catch (IOException ioe) {
			LOGGER.log(Level.SEVERE, "error in removing member from m2m", ioe);
		}
	}

	//</editor-fold>


	/**
	 * wrapper for TableColumn for Members.
	 * Set the header for the member columns.
	 * @param <S>
	 * @param <T>
	 */
	class MemberTableColumn<S,T> extends TableColumn<S,T> {

		public RPSMember member;

		MemberTableColumn(RPSMember member) {
			super("");
			this.member = member;

			Label l = new Label(member.getName());
			l.setRotate(-90);
			l.setPrefHeight(200);
			l.setStyle("-fx-font-weight: normal");
			this.setGraphic(new Group(l));
		}
	}
}