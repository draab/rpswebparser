package at.foorada.rk.rps.gui.mainTabs;

import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.data.RPSMember;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


public class RPSEntryTableEntry {
    
//    private static final Logger LOGGER = Logger.getLogger(RPSEntryTableEntry.class.getName());

    public static final SimpleDateFormat DATE_TIME_FORMAT_NO_SEC = new SimpleDateFormat("dd.MM.yyyy HH:mm");
    public static final SimpleDateFormat DATE_TIME_FORMAT_NO_SEC_WITH_NAME = new SimpleDateFormat("EEE, dd.MM.yyyy HH:mm");

    public RPSEntry entry;

    public RPSEntryTableEntry(RPSEntry entry) {
        this.entry = entry;
    }

    public String getDepartment() {
        return entry.getDepartment().toString();
    }

    public String getType() {
        return entry.getType().toString();
    }

    public String getStart() {
        return DATE_TIME_FORMAT_NO_SEC.format(entry.getStart().getTime());
    }

    public Date getStartTime() {
        return entry.getStart().getTime();
    }

    public String getEnd() {
        return DATE_TIME_FORMAT_NO_SEC.format(entry.getEnd().getTime());
    }

    public Date getEndTime() {
        return entry.getEnd().getTime();
    }

    public String getAnker() {
        return entry.getAnker();
    }

    public RPSMember getMember1() {
        List<RPSMember> member = entry.getMemberList();
        if(member != null && member.size() >= 1)
            return member.get(0);
        return null;
    }

    public RPSMember getMember2() {
        List<RPSMember> member = entry.getMemberList();
        if(member != null && member.size() >= 2)
            return member.get(1);
        return null;
    }

    public RPSMember getMember3() {
        List<RPSMember> member = entry.getMemberList();
        if(member != null && member.size() >= 3)
            return member.get(2);
        return null;
    }

    public String getFurtherMember() {
        List<RPSMember> member = entry.getMemberList();
        if(member != null && member.size() >= 4) {
            StringBuilder bldr = new StringBuilder();
            for(int i=3; i < member.size(); i++) {
                if(i != 3)
                    bldr.append(" ; ");
                bldr.append(EntryTableController.showPrefix ? member.get(i).getFullName() : member.get(i).name);
            }
            return bldr.toString();
        }
        return "";
    }

    public String getComment() {
        return entry.getComment();
    }
}
