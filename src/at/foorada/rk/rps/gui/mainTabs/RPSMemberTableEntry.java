package at.foorada.rk.rps.gui.mainTabs;

import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.data.RPSMember;

import java.util.ArrayList;
import java.util.List;

public class RPSMemberTableEntry {

    public RPSMember member;
    public List<RPSEntry> entryList;

    public RPSMemberTableEntry(RPSMember member) {
        this.member = member;
        entryList = new ArrayList<>();
    }

    public void addEntry(RPSEntry entry) {
        entryList.add(entry);
    }

    public RPSMember getMember() {
        return member;
    }

    public String getName() {
        return member.name;
    }

    public String getFullName() {
        return member.getFullName();
    }

    public List<RPSEntry> getEntryList() {
        return entryList;
    }
}
