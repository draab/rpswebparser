package at.foorada.rk.rps.parser;

import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class RPSWebParser {
    
    private static final Logger LOGGER = Logger.getLogger(RPSWebParser.class.getName());

    private RPSDepartment department;
    private String originalHtml;
    private Calendar startDate;
    private Document htmlDoc;

    public RPSWebParser() {
    }

    public RPSWebParser(String htmlString, Calendar startDate, RPSDepartment department) {
        this();
        loadHtmlString(htmlString, startDate, department);
    }

    public void loadHtmlString(String string, Calendar startDate, RPSDepartment department) {
        originalHtml = string;
        this.startDate = startDate;
        try {
            loadHtmlDocument();
            this.department = department;
        } catch(RPSWebParserException pe) {

        }
    }

    private void loadHtmlDocument() throws RPSWebParserException {

        htmlDoc = Jsoup.parse(originalHtml);
    }

    public void loadFile(File file, Calendar startDate, RPSDepartment department) throws IOException {

        try(BufferedReader bfrr = new BufferedReader(new FileReader(file))) {
            StringBuilder strbfr = new StringBuilder();
            String temp;
            while((temp = bfrr.readLine()) != null) {
                strbfr.append(temp);
            }
            loadHtmlString(strbfr.toString(), startDate, department);
            bfrr.close();

        } catch(IOException ioe) {
            throw ioe;
        }
    }

    public String getOriginalHtml() {
        return originalHtml;
    }

    public List<RPSEntry> parseRPSPoolTable() {

        List<RPSEntry> parseList = new ArrayList<>();
        Calendar iteratorDate = (Calendar) startDate.clone();

        Elements poolTableList = htmlDoc.getElementsByClass("RPS_PoolTable");
        for(Element day:poolTableList) {

            Elements poolEntryList = day.getElementsByTag("tr");
            poolEntryList.remove(0);    //remove headline
            for(Element poolEntry:poolEntryList) {
                try {
                    RPSEntry entry = parseFromPoolRowElement(poolEntry, iteratorDate);
                    entry.setDepartment(department);
                    parseList.add(entry);
                } catch(ParseException | RPSWebParserException pe) {
		    LOGGER.log(Level.WARNING, "failed to parse entry: dep;"+department.getDescription()+
			    "; "+poolEntry.text(), pe);
                }
            }
            iteratorDate.add(Calendar.DATE, 1);
        }

        return parseList;
    }

    public List<RPSEntry> parseRPSPlanTable() {

        List<RPSEntry> parseList = new ArrayList<>();
        Calendar iteratorDate = (Calendar) startDate.clone();

        Elements planTableList = htmlDoc.getElementsByClass("RPS_PlanTable");
        for(Element day:planTableList) {

            Elements planEntryList = day.getElementsByTag("tr");
            for(Element planEntry:planEntryList) {
                try {
                    RPSEntry entry = parseFromPlanRowElement(planEntry, iteratorDate);
                    entry.setDepartment(department);
                    parseList.add(entry);
                } catch(ParseException | RPSWebParserException pe) {
		    LOGGER.log(Level.WARNING, "failed to parse entry: dep:"+department.getDescription()+
			    "; "+planEntry.text() +"; on date: "+iteratorDate.getTime(), pe);
                }
            }
            iteratorDate.add(Calendar.DATE, 1);
        }

        return parseList;
    }

    public static boolean parseLoginResponse(String response) {

        Document loginHtml = Jsoup.parse(response);

        return loginHtml != null && loginHtml.getElementById("loginMsgContent") == null && loginHtml.getElementById("register-user") != null;
    }

    public static List<RPSDepartment> parseDepartmentFromJobContainer(String html) {

        Document jobHtml = Jsoup.parse(html);
        List<RPSDepartment> parseList = new ArrayList<>();

        Elements poolTableList = jobHtml.select("select[name=abteilung] > option");

        for(Element dep:poolTableList) {
            parseList.add(new RPSDepartment(Integer.parseInt(dep.attr("value")), dep.text()));
        }

        return parseList;

    }

    private RPSEntry parseFromPoolRowElement(Element tr, Calendar date) throws RPSWebParserException, ParseException {

        int parameterCnt = 0;

        Elements parameters = tr.getElementsByTag("td");
        RPSEntry newEntry = new RPSEntry();

        //entry type
        newEntry.setType(RPSEntry.Type.POOL);

        //entry members
        newEntry.addMember(parameters.get(parameterCnt++).text().trim());

        //entry start time
        Calendar start = (Calendar) date.clone();
        newEntry.setStart(parseTime(parameters.get(parameterCnt++).text().trim(), start));

        //entry end time
        Calendar end = (Calendar) date.clone();
        try {
            newEntry.setEnd(parseTime(parameters.get(parameterCnt++).text().trim(), end));
        } catch (RPSEntry.RPSEntryException rpsEE) {
            throw new RPSWebParserException(rpsEE.getMessage());
        }

        //entry comment
        newEntry.setComment(parameters.get(parameterCnt).text().trim());

        return newEntry;
    }

    private RPSEntry parseFromPlanRowElement(Element tr, Calendar date) throws RPSWebParserException, ParseException {

        int parameterCnt = 0;

        Elements parameters = tr.getElementsByTag("td");
        RPSEntry newEntry = new RPSEntry();

        //entry type
        RPSEntry.Type entryType= RPSEntry.Type.getFromPlanString(parameters.get(parameterCnt++).text().trim());
        if(entryType == RPSEntry.Type.UNKNOWN)
            throw new RPSWebParserException("rps entry type is not known");
        newEntry.setType(entryType);

        //entry comment
        newEntry.setComment(parameters.get(parameterCnt++).text().trim());

        //entry start time
        Calendar start = (Calendar) date.clone();
        newEntry.setStart(parseTime(parameters.get(parameterCnt++).text().trim(), start));

        //entry end time
        Calendar end = (Calendar) date.clone();
        try {
            newEntry.setEnd(parseTime(parameters.get(parameterCnt++).text().trim(), end));
        } catch (RPSEntry.RPSEntryException rpsEE) {
            throw new RPSWebParserException(rpsEE.getMessage());
        }

        if(newEntry.getType().hasAnker()) {
            newEntry.setAnker(parameters.get(parameterCnt++).text().trim());
        }

        //entry members
        for(; parameterCnt<parameters.size(); parameterCnt++) {
            String member = parameters.get(parameterCnt).text().trim();
            if(RPSEntry.isValidMember(member))
                newEntry.addMember(member);
        }

        return newEntry;
    }

    private Calendar parseTime(String timeString, Calendar date) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        sdf.setLenient(false);

        Calendar t = Calendar.getInstance();
        t.setTime(sdf.parse(timeString));

        date.set(Calendar.HOUR_OF_DAY, t.get(Calendar.HOUR_OF_DAY));
        date.set(Calendar.MINUTE, t.get(Calendar.MINUTE));
        date.set(Calendar.SECOND, 0);

        return date;
    }

}