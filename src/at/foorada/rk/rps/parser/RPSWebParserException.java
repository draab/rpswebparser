package at.foorada.rk.rps.parser;

/**
 * Created by danie on 26.04.2016.
 */
public class RPSWebParserException extends Exception {
    public RPSWebParserException(String message) {
        super(message);
    }
}