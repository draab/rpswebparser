package at.foorada.rk.rps.parser;

import at.foorada.rk.rps.data.web.BrowserSimulator;
import at.foorada.rk.rps.data.RPSDepartment;
import at.foorada.rk.rps.data.RPSEntry;
import at.foorada.rk.rps.data.storage.main.dataAccess.DataAccessInterface;
import at.foorada.rk.rps.gui.dialog.overview.OverviewController;

import javax.net.ssl.*;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class RPSWebParserManager {
    
    private static final Logger LOGGER = Logger.getLogger(RPSWebParserManager.class.getName());

    //if plan is set, the program will store the downloaded plan to a file
    public static boolean WRITE_HTML_PLAN_TO_FILE = false;

    private static final String PROTOCOL = "https://";
    private static final String RK_INTERN_HOST = "dienstplan";
    private static final String RK_EXTERN_HOST = "dienstplan.o.roteskreuz.at";
    private static final String LOGIN_PATH = "/login.php";
    private static final String PLAN_PATH = "/maisPlan/showPlan.content.content.zuteilungsplan.php";
    private static final String JOB_CONTAINER_PATH = "/mais/newJobContainer.content.content_2.php";
    private static final int DEFAULT_TIMEOUT = 3000;

    private static final String USER_AGENT_INTERN = "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET4.0C; .NET4.0E; .NET CLR 3.5.30729; .NET CLR 3.0.30729; InfoPath.3)";

    private BrowserSimulator browser;
    private boolean intern;
    private Map<String, String> loginProperties;

    public RPSWebParserManager(String loginname, String password) throws RPSWebParserException, IOException {

        checkIfIntern();

        browser = new BrowserSimulator();

        loginProperties = new HashMap<>(3);
        loginProperties.put("loginname", loginname);
        loginProperties.put("password", password);
        loginProperties.put("login", "Login");

        try {
            String loginResponse;
            if(intern) {
                overrideTrustCheck();
                loginResponse = browser.sendPOSTRequest(PROTOCOL + RK_INTERN_HOST + LOGIN_PATH, RK_INTERN_HOST, loginProperties, USER_AGENT_INTERN);
            } else
                loginResponse = browser.sendPOSTRequest(PROTOCOL + RK_EXTERN_HOST + LOGIN_PATH, RK_EXTERN_HOST, loginProperties);

            if(!RPSWebParser.parseLoginResponse(loginResponse))
                throw new RPSWebParserException("login was not successful. check loginparameters.");
        } catch(IOException ioe) {
            throw ioe;
        }
    }

    /**
     *  fix for
     *    Exception in thread "main" javax.net.ssl.SSLHandshakeException:
     *       sun.security.validator.ValidatorException:
     *           PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException:
     *               unable to find valid certification path to requested target
     */
    private void overrideTrustCheck() {
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        }

                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	    
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            LOGGER.log(Level.WARNING, "error while workaround for ssl certification", e);
        }
    }

    private void checkIfIntern() {

        try {
            intern = InetAddress.getByName(RK_INTERN_HOST).isReachable(DEFAULT_TIMEOUT);
        } catch (IOException | IllegalArgumentException | SecurityException e) {
            intern = false;
        }
	LOGGER.log(Level.INFO, "app started {0}", intern ? "RK intern" : "RK external");
    }

    public String getJobContainerFromWebsite() throws IOException, RPSWebParserException {

        try {
            String jobContainerHtml;
            if(intern) {
                jobContainerHtml = browser.sendPOSTRequest(PROTOCOL + RK_INTERN_HOST + JOB_CONTAINER_PATH, RK_INTERN_HOST);
            } else
                jobContainerHtml = browser.sendPOSTRequest(PROTOCOL + RK_EXTERN_HOST + JOB_CONTAINER_PATH, RK_EXTERN_HOST);


            if(WRITE_HTML_PLAN_TO_FILE)
                BrowserSimulator.writeInFile(new File("jobContainer.html"), jobContainerHtml);

            return jobContainerHtml;

        } catch(IOException ioe) {

            throw ioe;
        }
    }

    private String getPlanFromWebsite(Calendar date, RPSDepartment department, int days) throws IOException, RPSWebParserException {

        //region check parameters
        if(date == null)
            throw new RPSWebParserException("given date is null");
        if(department == null)
            throw new RPSWebParserException("given department is null");
        if(days > 7 || days < 0)
            throw new RPSWebParserException("given days parameter is not valid");
        //endregion

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

	Map<String, String> planProperties = new HashMap<>(10);
	planProperties.put("mergeList", "");
	planProperties.put("date", dateFormat.format(date.getTime()));
	planProperties.put("abteilung", String.valueOf(department.getId()));
	planProperties.put("days", String.valueOf(days));
	planProperties.put("nTag", "0");
	planProperties.put("showPool", "true");

	String planHtml;
	if(intern) {
	    planHtml = browser.sendPOSTRequest(PROTOCOL + RK_INTERN_HOST + PLAN_PATH, RK_INTERN_HOST, planProperties);
	} else
	    planHtml = browser.sendPOSTRequest(PROTOCOL + RK_EXTERN_HOST + PLAN_PATH, RK_EXTERN_HOST, planProperties);


	try {
	    if(WRITE_HTML_PLAN_TO_FILE)
		BrowserSimulator.writeInFile(new File("plan_"+RPSEntry.TIMESTAMP_DATE_ONLY.format(date.getTime())+"_"+days+".html"), planHtml);
	} catch(IOException ioe) {
	    LOGGER.log(Level.WARNING, "error in writing plan html to file", ioe);
	}
	return planHtml;
    }

    public List<RPSEntry> parseDays(Calendar startDate, int days, RPSDepartment department) throws IOException, RPSWebParserException {

        if(days <= 0)
            throw new RPSWebParserException("parameter days is not valid");

        RPSWebParser parser = new RPSWebParser();
        List<RPSEntry> parsedEntryList = new ArrayList<>();
        Calendar date = (Calendar) startDate.clone();
        int allDays2Parse = days;

        for(;allDays2Parse > 0; allDays2Parse -= 7) {
	    
	    String html = getPlanFromWebsite((Calendar) date.clone(), department, (allDays2Parse < 7 ? allDays2Parse : 7));
	    parser.loadHtmlString(html, (Calendar) date.clone(), department);
	    parsedEntryList.addAll(parser.parseRPSPlanTable());
	    parsedEntryList.addAll(parser.parseRPSPoolTable());

            date.add(Calendar.DAY_OF_MONTH, 7);
        }

	LOGGER.log(Level.INFO, "{0} entries parsed from rps website.", parsedEntryList.size());

        return parsedEntryList;
    }

    public List<RPSEntry> parseDays(RPSParseProperty properties) throws IOException, RPSWebParserException {
        return parseDays(properties.date, properties.days, properties.department);
    }

    public List<RPSEntry> parseDays(List<RPSParseProperty> properties) throws IOException, RPSWebParserException {

        List<RPSEntry> list = new ArrayList<>();

        for(RPSParseProperty property : properties)
            list.addAll(parseDays(property));

        return list;
    }

    public static List<RPSParseProperty> filterOutParsed (
	    Collection<RPSDepartment> departmentList, Collection<RPSEntry> entryList, RPSParseProperty property) {
        return filterOutParsed(departmentList, entryList, property.date, property.days, property.department);
    }

    public static List<RPSParseProperty> filterOutParsed (
	    Collection<RPSDepartment> departmentList, Collection<RPSEntry> entryList,
	    Calendar startDate, int days, RPSDepartment department) {

        List<RPSParseProperty> propertyList = new ArrayList<>();

	/* intermezzo for dpeartment == RPSDepartment.ALL
	 * --- loop all available departments
	 */
	if(department == RPSDepartment.ALL) {
	    departmentList.forEach(depLooper -> {
		propertyList.addAll(filterOutParsed(departmentList, entryList, startDate, days, depLooper));
	    });
	    return propertyList;
	}


	Calendar date = (Calendar) startDate.clone();
	RPSParseProperty currentParseProperty = null;


	for (int i = 0; i < days; i++) {
	    List<RPSEntry> list = DataAccessInterface.filterRPSEntryListByDateDepartment(entryList, date, department);
	    if (list.isEmpty()) {
		if (currentParseProperty == null)
		    currentParseProperty = new RPSParseProperty((Calendar) date.clone(), 1, department);
		else
		    currentParseProperty.days++;
	    } else {
		if (currentParseProperty != null) {
		    propertyList.add(currentParseProperty);
		    currentParseProperty = null;
		}
	    }

	    date.add(Calendar.DAY_OF_MONTH, 1);
	}
	if (currentParseProperty != null)
	    propertyList.add(currentParseProperty);

        return propertyList;
    }

    public static class RPSParseProperty {

        public Calendar date = null;
        public int days = 0;
        public RPSDepartment department = null;

        public RPSParseProperty(Calendar date, int days, RPSDepartment department) {
            this.date = date;
            this.days = days;
            this.department = department;
        }

        @Override
        public String toString() {
            if(date != null && department != null) {
                Calendar end = (Calendar) date.clone();
                end.add(Calendar.DAY_OF_MONTH, days-1);
                return department.getDescription() + ": " + OverviewController.DATE_FORMAT.format(date.getTime()) + " - " + OverviewController.DATE_FORMAT.format(end.getTime());
            }
            return super.toString();
        }
    }
}
