package at.foorada.system;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Logger;

/**
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 24.03.2017.
 */
public class DesktopHelper {

    private static final Logger LOGGER = Logger.getLogger(DesktopHelper.class.getName());

    public static void openMailClient(String sendTo, String subject) throws URISyntaxException, IOException {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;

        if (desktop != null && desktop.isSupported(Desktop.Action.MAIL)) {
                desktop.mail(new URI("mailto:"+sendTo+"?subject="+subject));
        } else
            throw new IOException("desktop is not supported");
    }

    public static void openWebpage(String url) throws IOException, URISyntaxException {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                desktop.browse(new URL(url).toURI());
        } else
            throw new IOException("desktop is not supported");
    }
}
