package at.foorada.timing;

import java.util.Calendar;
/**
 *
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 20.03.2017.
 */
public class ConsoleTimer {

    private static long firstTime = -1;
    private static long lastTime = -1;

    public static void Time() {
        Time("");
    }
    public static void Time(String message) {
        if(firstTime < 0) {
            firstTime = Calendar.getInstance().getTimeInMillis();
            System.out.format("timing: %s\n", message);
            return;
        }

        System.out.format("timing: %d - %s\n", Calendar.getInstance().getTimeInMillis() - firstTime, message);
    }


    public static void TimeRelative() {
        TimeRelative("");
    }
    public static void TimeRelative(String message) {
        if(lastTime < 0) {
            lastTime = Calendar.getInstance().getTimeInMillis();
            System.out.format("relative timing: %s\n", message);
            return;
        }

        long now = Calendar.getInstance().getTimeInMillis();
        System.out.format("relative timing: %d - %s\n", now - lastTime, message);
        lastTime = now;
    }
}
