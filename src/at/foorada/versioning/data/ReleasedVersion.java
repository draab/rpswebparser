package at.foorada.versioning.data;

import java.util.Calendar;

/**
 *
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 14.03.2017.
 */
public class ReleasedVersion extends Version  implements Comparable<Version> {

    private int pushNumber;
    private String message;
    private String link;
    private Calendar releaseDate;

    public ReleasedVersion(String appName, String appVersion) {
        super(appName, appVersion);
    }

    public ReleasedVersion(String appName, String appVersion, int pushNumber, String message, String link, Calendar releaseDate) {
        super(appName, appVersion);
        this.pushNumber = pushNumber;
        this.message = message;
        this.link = link;
        this.releaseDate = releaseDate;
    }

    public int getPushNumber() {
        return pushNumber;
    }

    public String getMessage() {
        return message;
    }

    public String getLink() {
        return link;
    }

    public Calendar getReleaseDate() {
        return releaseDate;
    }

    @Override
    public int compareTo(Version o) {

        int cmp = this.getAppName().compareTo(o.getAppName());
        if(cmp == 0) {
            if (o instanceof ReleasedVersion) {
                cmp = this.getPushNumber() - ((ReleasedVersion) o).getPushNumber();
            } else {
                super.compareTo(o);
            }
        }
        return cmp;
    }
}
