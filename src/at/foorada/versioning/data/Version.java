package at.foorada.versioning.data;

import java.util.Calendar;

/**
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 14.03.2017.
 */
public class Version implements Comparable<Version> {
    private String appName;
    private String appVersion;

    public Version(String appName, String appVersion) {
        this.appName = appName;
        this.appVersion = appVersion;
    }

    public String getAppName() {
        return appName;
    }

    public String getAppVersion() {
        return appVersion;
    }


    @Override
    public int compareTo(Version o) {
        int cmp = this.appName.compareTo(o.appName);
        if(cmp == 0) {
            String thisV = this.appVersion.substring(1);
            String oV = o.appVersion.substring(1);
            cmp = thisV.compareTo(oV);
        }

        return cmp;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || !(o instanceof Version)) return false;

        Version version = (Version) o;

        if (!appName.equals(version.appName)) return false;
        return appVersion.equals(version.appVersion);
    }

    @Override
    public int hashCode() {
        int result = appName.hashCode();
        result = 31 * result + appVersion.hashCode();
        return result;
    }
}