package at.foorada.versioning.net;

import at.foorada.rk.rps.Main;
import at.foorada.versioning.data.ReleasedVersion;
import at.foorada.versioning.data.Version;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Created by Daniel Raab <daniel.raab.at@gmail.com> on 14.03.2017.
 */
public class VersionUtils {

    private static final Logger LOGGER = Logger.getLogger(VersionUtils.class.getName());

    private static final String DEFAULT_URL = "http://versioning.draab.at/check.php";
    /**
     * format for database timestamp (string sorting possible)<br>
     * format:yyyy-MM-dd HH:mm:ss
     */
    private static final SimpleDateFormat TIMESTAMP = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static List<ReleasedVersion> getVersionsFromServer(String url, String appName) throws IOException, org.json.simple.parser.ParseException {

        String req = requestVersionString(new URL(url), appName, null);
        return parseRequestedString(req);
    }

    public static String getWebURL() {
        return VersionUtils.getWebURL(Main.APP_VERSION);
    }

    public static String getWebURL(Version version) {
        return VersionUtils.getWebURL(DEFAULT_URL, version);
    }

    public static String getWebURL(String url, Version version) {

        url += "?print=true&appName=" + version.getAppName();
        url += "&appVersion=" + version.getAppVersion();

        return url;
    }

    private static String requestVersionString(URL url, String appName, String appVersion) throws IOException {

        String message = "appName="+appName;
        if(appVersion != null) {
            message += "&appVersion="+appVersion;
        }

        return doRequest(url, message);
    }

    private static String doRequest(URL url, String message) throws IOException {

        LOGGER.log(Level.FINE, "doRequest stated with message: "+message);

        byte[] postData = message.getBytes( StandardCharsets.UTF_8 );
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setDoOutput(true);
        connection.setInstanceFollowRedirects(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty( "charset", "utf-8");
        connection.setUseCaches( false );
        connection.setRequestProperty( "Content-Length", Integer.toString( postData.length ));

        DataOutputStream wr = new DataOutputStream( connection.getOutputStream());
        wr.write(postData);
        wr.close();

        String response = IOUtils.toString(connection.getInputStream(), "UTF-8");
        LOGGER.log(Level.FINEST, "result of request: "+response);
        return response;
    }

    private static List<ReleasedVersion> parseRequestedString(String requestedString) throws org.json.simple.parser.ParseException {

        List<ReleasedVersion> list = new ArrayList<>();
        JSONArray arr = (JSONArray) new JSONParser().parse(requestedString);

        for(Object o : arr) {
            if(o instanceof JSONObject) {
                list.add(jsonObjectToVersionObj((JSONObject) o));
            }
        }

        return list;
    }

    private static ReleasedVersion jsonObjectToVersionObj(JSONObject obj) {
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(TIMESTAMP.parse((String) obj.get("releaseDate")));
        } catch (ParseException ignored) {}

        return new ReleasedVersion(
                (String)obj.get("appName"),
                (String)obj.get("appVersion"),
                ((Long) obj.get("pushNumber")).intValue(),
                (String) obj.get("message"),
                (String) obj.get("link"),
                cal
        );
    }

    public static List<ReleasedVersion> checkForNewVersionInList(List<ReleasedVersion> list, Version version) {
        Collections.sort(list);

        List<ReleasedVersion> newVersionList = new ArrayList<>();
        ReleasedVersion curReleasedVersion = null;

        for(ReleasedVersion rv : list) {
            if(rv.equals(version)) {
                curReleasedVersion = rv;
            } else if(curReleasedVersion != null && rv.getPushNumber() > curReleasedVersion.getPushNumber()) {
                newVersionList.add(rv);
            }
        }

        return newVersionList;
    }
}
